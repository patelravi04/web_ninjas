INSERT INTO users(first_name, last_name, phone, email, password, role, created_by) 
			VALUES ('Ravi', 'Patel', '204 333-3823', 'patel.ravi04@gmail.com', 'mypass', 'admin', 1);

INSERT INTO users(first_name, last_name, phone, email, password, role, created_by) 
			VALUES ('John', 'Doe', '204 123-4567', 'john@doe.com', 'mypass', 'customer', 1);

INSERT INTO users(first_name, last_name, phone, email, password, role, created_by) 
			VALUES ('Steve', 'George', '431 223-4567', 'steve@george.com', 'mypass', 'admin', 1);

INSERT INTO users(first_name, last_name, phone, email, password, role, created_by) 
			VALUES ('Leslie', 'William', '204 867-4567', 'leslie@gmail.com', 'mypass', 'customer', 1);

INSERT INTO users(first_name, last_name, phone, email, password, role, created_by) 
			VALUES ('Deep', 'Bhalotia', '431 227-4587', 'bandar@gmail.com', 'mypass', 'customer', 1);

INSERT INTO users(first_name, last_name, phone, email, password, role, created_by) 
			VALUES ('Sonia', 'Verma', '204 190-4567', 'sonia@verma.com', 'mypass', 'customer', 1);

INSERT INTO users(first_name, last_name, phone, email, password, role, created_by) 
			VALUES ('Lloyd', 'Gates', '204 456-4567', 'lloyd@gates.com', 'mypass', 'admin', 1);

INSERT INTO users(first_name, last_name, phone, email, password, role, created_by) 
			VALUES ('Sean', 'James', '204 453-4567', 'sean@james.com', 'mypass', 'customer', 1);

INSERT INTO users(first_name, last_name, phone, email, password, role, created_by) 
			VALUES ('Jeffrey', 'Way', '204 768-4996', 'jeffrey@way.com', 'mypass', 'customer', 1);

INSERT INTO users(first_name, last_name, phone, email, password, role, created_by) 
			VALUES ('Kelly', 'Carpick', '431 123-8789', 'kelly@carpick.com', 'mypass', 'customer', 1);															