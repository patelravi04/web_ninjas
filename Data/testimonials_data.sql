INSERT INTO testimonials(user_id, description, created_by) 
			VALUES (2, 'Words can’t express how grateful I am that you were our florist for our big 
				day. Working with you was so enjoyable and straightforward because you understood 
				what I wanted straight away and I trusted you completely!
				So many people commented on how gorgeous the flowers were (even boys!) so I 
				have sung your praises to anyone who will listen.', 1);
INSERT INTO testimonials(user_id, description, created_by) 
			VALUES (3, 'I wanted to drop you a line and thank you SOOO much for the beautiful 
				flowers. You took my brief and made it EVEN better. They were simply STUNNING.', 1);

INSERT INTO testimonials(user_id, description, created_by) 
			VALUES (4, 'I just wanted to say a HUGE HUGE thank you for doing my beautiful flowers 
				for the wedding. The arch was absolutely gorgeous and I can’t believe it was all 
				for my wedding. I was so overwhelmed when the flowers arrived at Chilly Powder too, 
				they matched the girls dresses perfectly, the colours were subtle and the blushes 
				just made the flowers all that much sweeter.', 1);

INSERT INTO testimonials(user_id, description, created_by) 
			VALUES (5, 'Florists at your company are the loveliest persons to deal with and 
				could not have been more helpful; even going so far as to find us a replacement organist 
				at the last minute, what service!', 1);

INSERT INTO testimonials(user_id, description, created_by) 
			VALUES (6, 'Thank you SO SO much for such beautiful flowers at our wedding. Our big 
				day was all we could have dreamed of and more. We had such a wonderful day, and 
				the style/ colours of the flowers in the Chapel and Cabane looked fantastic and 
				perfect for us', 1);

INSERT INTO testimonials(user_id, description, created_by) 
			VALUES (8, 'The bouquet and the large arrangement in the Farmhouse entrance especially. I 
				just loved them all. The bouquet is here centre stage in our apartment and it 
				makes me drift into a bubble of memories of our special day each time I see it.', 1);

INSERT INTO testimonials(user_id, description, created_by) 
			VALUES (1, 'Thank you so much for making our wedding day so amazing with your beautiful 
				flowers! We were so impressed with the arrangements. It was everything we had imagined. 
				ou were so wonderful to work with. Thank you again.', 1);

INSERT INTO testimonials(user_id, description, created_by) 
			VALUES (9, 'It has been a pleasure ordering flowers from you for the past couple of years. 
				I will never forget the help you gave me in arranging flowers for each of my staff members 
				for my first day of school way back when.', 1);

INSERT INTO testimonials(user_id, description, created_by) 
			VALUES (10, 'Thank you very much for the beautiful arrangement and your good wishes. It was much appreciated at this busy but exciting time for us.
', 1);

INSERT INTO testimonials(user_id, description, created_by) 
			VALUES (7, 'Thank you very much for the flowers that you gave to us to share with 
				out residents. They enjoyed receiving them as well as brightened their day. 
				We are grateful for your thoughtfulness and kindness. Thank you again.', 1);																		