<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Routes for front pages 
Route::get('/', 'PagesController@home'); 

Route::get('/shop', 'PagesController@shop');

Route::get('/shop/category/{id}', 'PagesController@shopByCategory');

Route::get('/shop/{id}', 'PagesController@shopDetails');

Route::get('/about', 'PagesController@about');

Route::get('/contact', 'PagesController@contact');

Route::get('/privacy', 'PagesController@privacy');

Route::get('/terms', 'PagesController@terms');

Route::get('/login', 'PagesController@login');

Route::get('/register', 'PagesController@register');

Route::get('/cart', 'PagesController@cart');

Route::get('/addToCart/{flag}/{id}', 'CartController@updateItem');

Route::get('/removeFromCart/{flag}/{id}', 'CartController@updateItem');

Route::get('/incrementInCart/{flag}/{id}', 'CartController@updateItem');

Route::get('/clearCart', 'CartController@clearCart');

Route::get('/faq', 'PagesController@faq');

Route::get('/sendMailFromContact', 'PagesController@sendMailFromContact');

Route::middleware(['auth'])->group(function(){
	Route::get('/checkout', 'PagesController@checkout');

	Route::post('/payment', 'PagesController@paymentOrder');

	Route::get('/orders', 'PagesController@orders');

	// Route for the Categories page in admin panel
	Route::get('/addresses/', 'AddressesController@index');
	Route::get('/addresses/create', 'AddressesController@create');
	Route::post('/addresses', 'AddressesController@store');
	Route::get('/addresses/{id}', 'AddressesController@edit');
	Route::put('/addresses/{id}', 'AddressesController@update');
	Route::delete('/addresses/{id}', 'AddressesController@destroy');

	Route::get('/profile', 'PagesController@profile');

	Route::get('/thankyou','PagesController@thankyou');
});

// ************************ Admin routes ******************************

// Admin group - requires authentication
Route::middleware(['auth', 'admin'])->group(function(){

	//Dashboard
	Route::get('/admin', 'DashboardController@index');

	// Route for the Categories page in admin panel 
	Route::get('/admin/categories', 'Admin\CategoriesController@index');
	Route::get('/admin/categories/create', 'Admin\CategoriesController@create');
	Route::post('/admin/categories', 'Admin\CategoriesController@store');
	Route::get('/admin/categories/{id}', 'Admin\CategoriesController@edit');
	Route::put('/admin/categories/{id}', 'Admin\CategoriesController@update');
	Route::delete('/admin/categories/{id}', 'Admin\CategoriesController@destroy');

	// Route for the Products page in admin panel 
	Route::get('/admin/products', 'Admin\ProductsController@index');
	Route::get('/admin/products/create', 'Admin\ProductsController@create');
	Route::post('/admin/products', 'Admin\ProductsController@store');
	Route::get('/admin/products/{id}', 'Admin\ProductsController@edit');
	Route::put('/admin/products/{id}', 'Admin\ProductsController@update');
	Route::delete('/admin/products/{id}', 'Admin\ProductsController@destroy');

	// tax
	Route::get('/admin/tax', 'Admin\TaxController@index');
	Route::get('/admin/tax/create', 'Admin\TaxController@create');
	Route::post('/admin/tax/store', 'Admin\TaxController@store');
	Route::get('/admin/tax/edit/{id}', 'Admin\TaxController@edit');
	Route::put('/admin/tax/{id}', 'Admin\TaxController@update');
	Route::delete('/admin/tax/delete/{id}', 'Admin\TaxController@destroy');

	// Route for the testimonials page in admin panel 
	Route::get('/admin/testimonials', 'Admin\TestimonialsController@index');
	Route::get('/admin/testimonials/{testimonial_id}', 'Admin\TestimonialsController@show');

	// Route for delivery charges page in admin panel
	Route::get('/admin/delivery_charges','Admin\DeliveryChargesController@index');
	Route::get('/admin/delivery_charges/create', 'Admin\DeliveryChargesController@create');
	Route::post('/admin/delivery_charges','Admin\DeliveryChargesController@store');
	Route::get('/admin/delivery_charges/{delivery_charge_id}','Admin\DeliveryChargesController@edit');
	Route::put('/admin/delivery_charges/{delivery_charge_id}','Admin\DeliveryChargesController@update');
	Route::delete('/admin/delivery_charges/{delivery_charge_id}','Admin\DeliveryChargesController@destroy');

	// Route for users page in admin panel
	Route::get('/admin/users', 'Admin\UsersController@index');
	Route::get('/admin/users/create', 'Admin\UsersController@create');
	Route::post('/admin/users', 'Admin\UsersController@store');
	Route::get('/admin/users/{user_id}', 'Admin\UsersController@edit');
	Route::put('/admin/users/{user_id}', 'Admin\UsersController@update');
	Route::delete('/admin/users/{user_id}', 'Admin\UsersController@destroy');

	// discounts
	Route::get('/admin/discounts', 'Admin\DiscountsController@index');
	Route::get('/admin/discounts/create', 'Admin\DiscountsController@create');
	Route::post('/admin/discounts', 'Admin\DiscountsController@store');
	Route::get('/admin/discounts/{id}', 'Admin\DiscountsController@edit');
	Route::put('/admin/discounts/{id}', 'Admin\DiscountsController@update');
	Route::delete('/admin/discounts/{id}', 'Admin\DiscountsController@destroy');

	//Transactions
	Route::get('/admin/transactions', 'Admin\TransactionsController@index');
	Route::get('/admin/transactions/{transaction_id}', 'Admin\TransactionsController@show');	

	//Orders
	Route::get('/admin/orders', 'Admin\OrdersController@index');
	Route::get('/admin/orders/{order_id}', 'Admin\OrdersController@show');		

	//Route for Admin Page in Admin Panel
	Route::get('admin/faq','Admin\FaqsController@index');
	Route::get('admin/faq/create','Admin\FaqsController@create');
	Route::post('admin/faq','Admin\FaqsController@store');
	Route::get('admin/faq/{id}','Admin\FaqsController@edit');
	Route::put('admin/faq/{id}','Admin\FaqsController@update');
	Route::delete('admin/faq/{id}','Admin\FaqsController@destroy');

	//Route for Admin Page in Admin Panel -- About
	Route::get('admin/about','Admin\AboutController@index');
	Route::get('admin/about/create','Admin\AboutController@create');
	Route::post('admin/about','Admin\AboutController@store');
	Route::get('admin/about/{id}','Admin\AboutController@edit');
	Route::put('admin/about/{id}','Admin\AboutController@update');
	Route::delete('admin/about/{id}','Admin\AboutController@destroy');
});

Auth::routes(['verify' => true]);
