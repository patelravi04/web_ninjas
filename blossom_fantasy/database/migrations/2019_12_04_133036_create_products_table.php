<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->string('description');
            $table->decimal('price',5,2);
            $table->integer('quantity');
            $table->integer('max_days_for_delivery');
            $table->integer('reorder_level');
            $table->string('image');
            $table->integer('has_discount');
            $table->decimal('discounted_price',5,2);
            $table->integer('discount_id');
            $table->integer('is_deleted');
            $table->int('created_by');
            $table->int('updated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
