<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
        	'name' => 'Pineland Daisy',
            'description' => 'ut suscipit a feugiat et eros vestibulum ac est lacinia nisi 
                                venenatis tristique fusce congue diam id ornare',
            'price' => 49.65,
            'quantity' => 253,
            'max_days_for_delivery' => 4,
            'reorder_level' => 5,
            'image' => 'images\flowers\beeBalm.jpg',
            'has_discount' => 1,
            'discounted_price' => 1.76,
            'discount_id' => 2,
            'is_deleted' => 5,
            'created_by' => 7,
            'updated_by' => 7,
        	'created_at' => Carbon::now(),
        	'updated_at' => Carbon::now()
        ]);
        DB::table('products')->insert([
        	'name' => 'Maguire\'s Penstemon',
            'description' => 'nulla pede ullamcorper augue a suscipit nulla elit 
                                ac nulla sed vel enim sit amet nunc viverra',
            'price' => 12.13,
            'quantity' => 370,
            'max_days_for_delivery' => 1,
            'reorder_level' => 5,
            'image' => 'images\flowers\bittersweet.jpe',
            'has_discount' => 1,
            'discounted_price' => 1.76,
            'discount_id' => 2,
            'is_deleted' => 1,
            'created_by' => 7,
            'updated_by' => 5,
        	'created_at' => Carbon::now(),
        	'updated_at' => Carbon::now()
        ]);

        DB::table('products')->insert([
        	'name' => 'False Goldeneye',
            'description' => 'vivamus in felis eu sapien cursus vestibulum proin eu mi nulla ac',
            'price' => 12.13,
            'quantity' => 370,
            'max_days_for_delivery' => 1,
            'reorder_level' => 5,
            'image' => 'images\flowers\bittersweett.jpe',
            'has_discount' => 0,
            'discounted_price' => 4.76,
            'discount_id' => 9,
            'is_deleted' => 0,
            'created_by' => 3,
            'updated_by' => 5,
        	'created_at' => Carbon::now(),
        	'updated_at' => Carbon::now()
        ]);

        DB::table('products')->insert([
        	'name' => 'Taxiphyllum Moss',
            'description' => 'magnis dis parturient montes nascetur 
                                ridiculus mus vivamus vestibulum sagittis sapien 
                                cum sociis natoque penatibus et magnis dis',
            'price' => 6.65,
            'quantity' => 167,
            'max_days_for_delivery' => 5,
            'reorder_level' => 9,
            'image' => 'images\flowers\blackEyedSusanFlower.jpe',
            'has_discount' => 1,
            'discounted_price' => 4.76,
            'discount_id' => 9,
            'is_deleted' => 0,
            'created_by' => 3,
            'updated_by' => 4,
        	'created_at' => Carbon::now(),
        	'updated_at' => Carbon::now()
        ]);

        DB::table('products')->insert([
        	'name' => 'Thrift Seapink',
            'description' => 'consequat lectus in est risus auctor sed tristique in tempus sit amet',
            'price' => 82.26,
            'quantity' => 837,
            'max_days_for_delivery' => 5,
            'reorder_level' => 9,
            'image' => 'images\flowers\blueFlagIris.jpg',
            'has_discount' => 0,
            'discounted_price' => 4.76,
            'discount_id' => 9,
            'is_deleted' => 0,
            'created_by' => 7,
            'updated_by' => 1,
        	'created_at' => Carbon::now(),
        	'updated_at' => Carbon::now()
        ]);
        DB::table('products')->insert([
        	'name' => 'Szatala\'s Crabseye Lichen',
            'description' => 'gravida sem praesent id massa id nisl venenatis lacinia 
                                aenean sit amet justo morbi ut odio cras',
            'price' => 84.31,
            'quantity' => 445,
            'max_days_for_delivery' => 5,
            'reorder_level' => 7,
            'image' => 'images\flowers\butterflyWeed.jpe',
            'has_discount' => 0,
            'discounted_price' => 4.76,
            'discount_id' => 9,
            'is_deleted' => 0,
            'created_by' => 6,
            'updated_by' => 8,
        	'created_at' => Carbon::now(),
        	'updated_at' => Carbon::now()
        ]);

        DB::table('products')->insert([
        	'name' => 'Foothill Rush',
            'description' => 'nulla facilisi cras non velit nec nisi vulputate nonummy maecenas',
            'price' => 59.60,
            'quantity' => 445,
            'max_days_for_delivery' => 5,
            'reorder_level' => 7,
            'image' => 'images\flowers\camprisRadicum.jpe',
            'has_discount' => 1,
            'discounted_price' => 4.57,
            'discount_id' => 9,
            'is_deleted' => 1,
            'created_by' => 8,
            'updated_by' => 3,
        	'created_at' => Carbon::now(),
        	'updated_at' => Carbon::now()
        ]);

        
        


    }
}
