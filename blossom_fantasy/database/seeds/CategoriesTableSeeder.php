<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('categories')->insert([
        	'name' => 'Birthday',
            'description' => 'tortor duis mattis egestas metus aenean fermentum donec 
                                ut mauris eget massa tempor convallis',
            'is_deleted' => 0,
            'created_by' => 6,
            'updated_by' => 2,
        	'created_at' => Carbon::now(),
        	'updated_at' => Carbon::now()
        ]);
        DB::table('categories')->insert([
        	'name' => 'Anniversary',
            'description' => 'tempus semper est quam pharetra magna ac consequat metus sapien ut',
            'is_deleted' => 0,
            'created_by' => 4,
            'updated_by' => 6,
        	'created_at' => Carbon::now(),
        	'updated_at' => Carbon::now()
        ]);
        DB::table('categories')->insert([
        	'name' => 'Wedding',
            'description' => 'molestie nibh in lectus pellentesque at nulla suspendisse potenti 
                                cras in purus eu magna',
            'is_deleted' => 0,
            'created_by' => 2,
            'updated_by' => 3,
        	'created_at' => Carbon::now(),
        	'updated_at' => Carbon::now()
        ]);
        DB::table('categories')->insert([
        	'name' => 'Debut',
            'description' => 'sed interdum venenatis turpis enim blandit mi in porttitor pede 
                                justo eu massa donec dapibus duis at velit eu',
            'is_deleted' => 0,
            'created_by' => 2,
            'updated_by' => 7,
        	'created_at' => Carbon::now(),
        	'updated_at' => Carbon::now()
        ]);
        DB::table('categories')->insert([
        	'name' => 'Prom',
            'description' => 'sed interdum venenatis turpis enim blandit mi in porttitor pede 
                                justo eu massa donec dapibus duis at velit eu',
            'is_deleted' => 0,
            'created_by' => 4,
            'updated_by' => 1,
        	'created_at' => Carbon::now(),
        	'updated_at' => Carbon::now()
        ]);
        DB::table('categories')->insert([
        	'name' => 'Grand Opening',
            'description' => 'interdum in ante vestibulum ante ipsum primis in faucibus orci',
            'is_deleted' => 0,
            'created_by' => 6,
            'updated_by' => 10,
        	'created_at' => Carbon::now(),
        	'updated_at' => Carbon::now()
        ]);
        DB::table('categories')->insert([
        	'name' => 'Funeral',
            'description' => 'interdum in ante vestibulum ante ipsum primis in faucibus orci',
            'is_deleted' => 0,
            'created_by' => 3,
            'updated_by' => 10,
        	'created_at' => Carbon::now(),
        	'updated_at' => Carbon::now()
        ]);
        DB::table('categories')->insert([
        	'name' => 'Festival',
            'description' => 'interdum in ante vestibulum ante ipsum primis in faucibus orci',
            'is_deleted' => 0,
            'created_by' => 4,
            'updated_by' => 9,
        	'created_at' => Carbon::now(),
        	'updated_at' => Carbon::now()
        ]);
        DB::table('categories')->insert([
        	'name' => 'Christmas',
            'description' => 'interdum in ante vestibulum ante ipsum primis in faucibus orci',
            'is_deleted' => 0,
            'created_by' => 8,
            'updated_by' => 9,
        	'created_at' => Carbon::now(),
        	'updated_at' => Carbon::now()
        ]);
        DB::table('categories')->insert([
        	'name' => 'Thanksgiving',
            'description' => 'interdum in ante vestibulum ante ipsum primis in faucibus orci',
            'is_deleted' => 0,
            'created_by' => 3,
            'updated_by' => 2,
        	'created_at' => Carbon::now(),
        	'updated_at' => Carbon::now()
        ]);
    }
}
