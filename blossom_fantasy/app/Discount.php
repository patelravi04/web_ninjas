<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Discount extends Model
{
    /**
	 * Eloquent's "soft delete" model
	 */
    use SoftDeletes;

    /**
     * Mass Assignment to utilize laravel's Eloquent ORM (Object-Relational Mapping)
     * @var [type]
     */
    protected $fillable = ['description', 'percentage', 'deleted_at', 'created_by', 'updated_by', 'updated_at'];

    /**
     * Get the products that the category belongs to
     * @return Array
     */
    
    protected $primaryKey = 'id';
    
}
