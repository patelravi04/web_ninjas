<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Tax extends Model
{
    /**
	 * Eloquent's "soft delete" model
	 */
    use SoftDeletes;

    /**
     * Mass Assignment to utilize laravel's Eloquent ORM (Object-Relational Mapping)
     * @var [type]
     */
    protected $fillable = ['name', 'description','percentage', 'created_by','updated_by', 'updated_at'];

    /**
     * Get the products that the category belongs to
     * @return Array
     */
    public function products()
    {
    	return $this->belongsToMany('App\Product');
    }
}
