<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Testimonial extends Model
{	

    /**
     * function for many relationship with testimonials in user model
     * @return [type] data
     */
    public function users()
    {
        return $this->belongsTo('App\User','id');
    }
}
