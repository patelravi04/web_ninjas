<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Address extends Model
{
    /**
	 * Eloquent's "soft delete" model
	 */
    use SoftDeletes;

    /**
     * Mass Assignment to utilize laravel's Eloquent ORM (Object-Relational Mapping)
     * @var [type]
     */
    protected $fillable = ['user_id', 'address_type', 'address', 'city', 'province', 'postal_code', 'created_by', 'updated_by', 'updated_at'];
}
