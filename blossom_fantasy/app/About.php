<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class About extends Model
{
	/**
     * Eloquent's "soft delete" model
     */
    use SoftDeletes;
	
/**
     * Mass Assignment to utilize laravel's Eloquent ORM (Object-Relational Mapping)
     * @var [type]
     */
    protected $fillable = ['image', 'timeline', 'deleted_at', 'created_by', 'updated_by', 'updated_at'];
}
