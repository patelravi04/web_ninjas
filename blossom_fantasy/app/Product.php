<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;



class Product extends Model
{
    /**
     * Eloquent's "soft delete" model
     */
    use SoftDeletes;

	/**
     * Mass Assignment to utilize laravel's Eloquent ORM (Object-Relational Mapping)
     * @var [type]
     */
    protected $fillable = ['title', 'description', 'price', 'quantity',
    						 'max_days_for_delivery', 'reorder_level',
    						 'discount_applicable', 'discount_in_price', 'created_by',
                             'updated_by', 'updated_at' ];

    /**
     * Get the caategories that the product belongs to
     * @return Array
     */
    public function categories()
    {
    	return $this->belongsToMany('App\Category');
    }
}
