<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Address;
use Carbon\Carbon;
use Auth;

class AddressesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Addresses';
        $subtitle = 'BF - Addresses';
        $slug = 'addresses';
        $addresses = Address::where('user_id', '=', Auth::user()->id)->get();
        //dd($addresses->toArray());
        return view('addresses.index', compact('title', 'subtitle', 'slug', 'addresses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Add New Address';
        $subtitle = 'BF Admin - Create Address';
        $slug = 'create-address';
        $addressTypes = ['billing', 'shipping'];
        return view('addresses.create', compact('title', 'subtitle', 'slug', 'addressTypes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $this->validateRequestData($request);
        $validatedData['user_id'] = Auth::user()->id;
        $validatedData['created_by'] = Auth::user()->id;

        $status = "";
        $message = "";
        if(Address::create($validatedData)){
            $status = "success";
            $message = "Address was successfully created!";
        } else {
            $status = "failure";
            $message = "Address was not successfully created!";
        }

        return redirect('/addresses')->with($status, $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title = 'Edit an Address';
        $subtitle = 'BF - Addresses';      
        $address = Address::findOrFail($id);
        $addressTypes = ['billing', 'shipping'];
        return view('addresses.edit', compact('title','subtitle', 'address', 'addressTypes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $this->validateRequestData($request, $id);
        $validatedData['updated_by'] = Auth::user()->id;
        $validatedData['updated_at'] = Carbon::now();

        $status = "";
        $message = "";
        if(Address::find($id)->update($validatedData)){
            $status = "success";
            $message = "Address was successfully updated!";
        } else {
            $status = "failure";
            $message = "Address was not successfully updated!";
        }
        
        return redirect('/addresses')->with($status, $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $address = Address::find($id);

        $status = "";
        $message = "";
        if($address->delete()){
            $status = "success";
            $message = "Address was successfully deleted!";
        } else {
            $status = "failure";
            $message = "Address was not successfully deleted!";
        }
        
        return redirect('/addresses')->with($status, $message);
    }

    /**
     * Validates the data within a request
     * @param  Request $request
     * @param  INT $id      
     * @return Mixed          validated data
     */
    private function validateRequestData($request, $id = null)
    {

        $validatedData = request()->validate([
            'address_type' => 'required',
            'address' => 'required|max:100',
            'city' => 'required|max:50',
            'province' => 'required|max:20',
            'postal_code' => 'required|max:7'
        ]);

        return $validatedData;
    }
}
