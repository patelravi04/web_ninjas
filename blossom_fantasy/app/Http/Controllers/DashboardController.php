<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Discount;
use App\Order;
use App\Product;
use App\Tax;
use App\Testimonial;
use App\Transaction;
use App\User;

class DashboardController extends Controller
{
    public function index() 
    {
    	$title = "Dashboard";
    	$subtitle = "Dashboard";
    	$slug = "dashboard";

    	$category_sum = Category::get()->count();
    	$discount_sum = Discount::get()->count();
    	$order_sum = Order::get()->count();
    	$product_sum = Product::get()->count();
    	$tax_sum = Tax::get()->count();
    	$testimonial_sum = Testimonial::get()->count();
    	$transaction_sum = Transaction::get()->count();
    	$user_sum = User::get()->count();

    	$max_product = Product::max('price');
    	$min_product = Product::min('price');
        $avg_product = Product::avg('price');

    	$max_order = Order::max('total');
    	$min_order = Order::min('total');
        $avg_order = Order::avg('total');

    	return view('admin.index', compact('title', 'subtitle', 'slug', 'category_sum', 'discount_sum', 'order_sum', 'product_sum', 'tax_sum', 'testimonial_sum', 'transaction_sum', 'user_sum','max_product','min_product', 'max_order','min_order', 'avg_product', 'avg_order'));
    }
}
