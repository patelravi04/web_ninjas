<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Faq;
use Carbon\Carbon;
use Auth;

class FaqsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'FAQ';
        $subtitle = 'BF Admin - FAQ';
        $slug = 'faq';
        $faqs = Faq::latest()->get();
        return view('admin.faq.index', compact('title', 'subtitle', 'slug', 'faqs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Create new FAQs';
        $subtitle = 'BF Admin - Create FAQ';
        $slug = 'create-faq';
        return view('admin.faq.create', compact('title', 'subtitle', 'slug'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $this->validateRequestData($request);
        $validatedData['created_by'] = Auth::user()->id;

        $status = "";
        $message = "";
        // if conditin to check the error status
        if(Faq::create($validatedData)){
            $status = "success";
            $message = "FAQ was successfully created!";
        } else {
            $status = "failure";
            $message = "FAQ was not successfully created!";
        }

        return redirect('admin/faq')->with($status, $message); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title = 'Edit a FAQ';
        $subtitle = 'BF Admin - FAQ';      
        $faq = Faq::findOrFail($id);
        return view('admin.faq.edit', compact('title','subtitle', 'faq'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $this->validateRequestData($request, $id);
        $validatedData['updated_by'] = Auth::user()->id;
        $validatedData['updated_at'] = Carbon::now();

        $status = "";
        $message = "";
        if(Faq::find($id)->update($validatedData)){
            $status = "success";
            $message = "FAQ was successfully updated!";
        } else {
            $status = "failure";
            $message = "FAQ was not successfully updated!";
        }
        
        return redirect('/admin/faq')->with($status, $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $faq = Faq::find($id);

        $status = "";
        $message = "";
        if($faq->delete()){
            $status = "success";
            $message = "FAQ was successfully deleted!";
        } else {
            $status = "failure";
            $message = "FAQ was not successfully deleted!";
        }
        
        return redirect('/admin/faq')->with($status, $message);
    }

    /**
     * Validates the data within a request
     * @param  Request $request
     * @param  INT $id      
     * @return Mixed          validated data
     */
    private function validateRequestData($request, $id = null)
    {

        $validatedData = request()->validate([
          "questions" => 'required|string',
          "answers" => 'required|string'
        ]);

        return $validatedData;
    }
}
