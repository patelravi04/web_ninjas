<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Tax;
use Carbon\Carbon;
use Auth;

class TaxController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Taxes';
        $subtitle = 'BF Admin - Taxes';
        $slug = 'tax';
        $taxes = Tax::latest()->get();
        return view('admin.tax.index', compact('title', 'subtitle', 'slug', 'taxes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Create A Tax';
        $subtitle = 'BF Admin - Create Tax';
        $slug = 'create-tax';
        return view('admin.tax.create', compact('title', 'subtitle', 'slug'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validatedData = $this->validateRequestData($request);
        $validatedData['created_by'] = Auth::User()->id;

        $status = "";
        $message = "";

        if(Tax::create($validatedData)){
            $status = "success";
            $message = "Tax was successfully created!";
        } else {
            $status = "failure";
            $message = "Tax was not successfully created!";
        }

        return redirect('admin/tax')->with($status, $message);

    }



    /**
     * Display the specified resource.
     *
     * @param  \App\Tax  $tax
     * @return \Illuminate\Http\Response
     */
    public function show(Tax $tax)
    {
        return view('admin.tax.show', $tax);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Tax  $tax
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title = 'Edit Tax';

        $subtitle = 'BF Admin - Taxes';      

        $tax = Tax::findOrFail($id);
        return view('admin.tax.edit', compact('title','subtitle', 'tax'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Tax  $tax
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $this->validateRequestData($request, $id);
        $validatedData['updated_by'] = Auth::User()->id;
        $validatedData['updated_at'] = Carbon::now();

        $status = "";
        $message = "";
        if(Tax::whereId($id)->update($validatedData)){
            $status = "success";
            $message = "Tax was successfully updated!";
        } else {
            $status = "failure";
            $message = "Tax was not successfully updated!";
        }
        
        return redirect('/admin/tax')->with($status, $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tax  $tax
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tax = Tax::find($id);

        $status = "";
        $message = "";
        if($tax->delete()){
            $status = "success";
            $message = "Tax was successfully deleted!";
        } else {
            $status = "failure";
            $message = "Tax was not successfully deleted!";
        }
        
        return redirect('/admin/tax')->with($status, $message);
    }

    /**
     * Validates the data within a request
     * @param  Request $request
     * @param  INT $id      
     * @return Mixed          validated data
     */
    private function validateRequestData($request, $id = null)
    {

        $validatedData = request()->validate([
          "name" => 'required|alpha',
          'description' => 'required',
          'percentage' => 'required|numeric'
        ]);

        return $validatedData;
    }
}
