<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\User;
use Carbon\Carbon;
use Auth;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Users';
        $subtitle = 'BF Admin - Users';
        $slug = 'users';
        $users = User::latest()->get();
        return view('admin.users.index', compact('title', 'subtitle', 'slug', 'users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Add a User';
        $subtitle = 'BF Admin - Add User';
        $slug = 'add-user';
        $roles = ['admin', 'blogger', 'customer', 'support'];
        return view('admin.users.create', compact('title', 'subtitle', 'slug', 'roles'));
    }    


/**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $this->validateRequestData($request);
        $validatedData['created_by'] = Auth::user()->id;        
        $validatedData = $request->validate(['email'=>'required']);
        $status = "";
        $message = "";
        if(User::create($validatedData)){
            $status = "success";
            $message = "User was successfully created!";
        } else {
            $status = "failure";
            $message = "User was not successfully created!";
        }

        return redirect('admin/users')->with($status, $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title = 'Edit a User';
        $subtitle = 'BF Admin - Users';      
        $user = User::findOrFail($id);
        $roles = ['admin', 'blogger', 'customer', 'support'];
        return view('admin.users.edit', compact('title','subtitle', 'user', 'roles'));
    }

/**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $this->validateRequestData($request, $id);
        $validatedData = $request->validate(['email'=>'required']);
        $validatedData['updated_by'] = Auth::user()->id;
        $validatedData['updated_at'] = Carbon::now();

        $status = "";
        $message = "";
        if(User::whereId($id)->update($validatedData)){
            $status = "success";
            $message = "User was successfully updated!";
        } else {
            $status = "failure";
            $message = "User was not successfully updated!";
        }
        
        return redirect('/admin/users')->with($status, $message);
    }

        /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);

        $status = "";
        $message = "";
        if($user->delete()){
            $status = "success";
            $message = "User was successfully deleted!";
        } else {
            $status = "failure";
            $message = "User was not successfully deleted!";
        }
        
        return redirect('/admin/users')->with($status, $message);
    }

    /**
     * Validates the data within a request
     * @param  Request $request
     * @param  INT $id      
     * @return Mixed          validated data
     */
    private function validateRequestData($request, $id = null)
    {

        $validatedData = request()->validate([
          "first_name" => 'required|string|max:16',
          "last_name" => 'required|string|max:16',
          "phone" => 'required|min:10|unique:users|regex:/^([0-9]{3})-?([0-9]{3})-?([0-9]{4})$/',
          "password" => 'required|min:6|regex:/(?=.*[A-Z]+)(?=.*[a-z]+)(?=.*[0-9]+)(?=.*[\!\@\#\$\%\^\&\*\(\)]+).{6,}/',
          "role" => 'required'
        ]);

        return $validatedData;
    }    
}
