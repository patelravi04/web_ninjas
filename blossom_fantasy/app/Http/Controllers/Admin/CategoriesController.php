<?php
// namespace for controller
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Category;
use Carbon\Carbon;
use Auth;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Categories';
        $subtitle = 'BF Admin - Categories';
        $slug = 'categories';
        $categories = Category::latest()->get();
        return view('admin.categories.index', compact('title', 'subtitle', 'slug', 'categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Create A Category';
        $subtitle = 'BF Admin - Create Category';
        $slug = 'create-category';
        return view('admin.categories.create', compact('title', 'subtitle', 'slug'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $this->validateRequestData($request);
        $validatedData['created_by'] = Auth::user()->id;

        $status = "";
        $message = "";
        if(Category::create($validatedData)){
            $status = "success";
            $message = "Category was successfully created!";
        } else {
            $status = "failure";
            $message = "Category was not successfully created!";
        }

        return redirect('admin/categories')->with($status, $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title = 'Edit a Category';
        $subtitle = 'BF Admin - Categories';      
        $category = Category::findOrFail($id);
        return view('admin.categories.edit', compact('title','subtitle', 'category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $this->validateRequestData($request, $id);
        $validatedData['updated_by'] = Auth::user()->id;
        $validatedData['updated_at'] = Carbon::now();

        $status = "";
        $message = "";
        if(Category::find($id)->update($validatedData)){
            $status = "success";
            $message = "Category was successfully updated!";
        } else {
            $status = "failure";
            $message = "Category was not successfully updated!";
        }
        
        return redirect('/admin/categories')->with($status, $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::find($id);

        $status = "";
        $message = "";
        if($category->delete()){
            $status = "success";
            $message = "Category was successfully deleted!";
        } else {
            $status = "failure";
            $message = "Category was not successfully deleted!";
        }
        
        return redirect('/admin/categories')->with($status, $message);
    }

    /**
     * Validates the data within a request
     * @param  Request $request
     * @param  INT $id      
     * @return Mixed          validated data
     */
    private function validateRequestData($request, $id = null)
    {

        $validatedData = request()->validate([
          "name" => 'required|alpha|max:50|unique:categories,name,'.$id,
          "description" => 'required'
        ]);

        return $validatedData;
    }
}
