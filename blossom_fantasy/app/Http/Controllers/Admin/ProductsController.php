<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Product;
use App\Category;
use Carbon\Carbon;
use Arr;
use Intervention\Image\Facades\Image;
use Auth;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Products';
        $subtitle = 'BF Admin - Products';
        $slug = 'products';
        $products = Product::with('categories')->latest()->paginate(10);
        return view('admin.products.index', compact('title', 'subtitle', 'slug', 'products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Create A Product';
        $subtitle = 'BF Admin - Create Product';
        $slug = 'create-product';
        $categories = Category::latest()->get();
        return view('admin.products.create', compact('title', 'subtitle', 'slug', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $this->validateRequestData($request);
        $validatedData['created_by'] = Auth::user()->id;

        $status = "";
        $message = "";
        if($product = Product::create($validatedData)){
            if(request()->hasFile('image')) {
                $this->storeImage($product, 'image');
                $filename = $request->file('image')->hashName();
                $product->image = $filename;
                $this->makeImageThumbnail($filename);
            }

            if(request()->hasFile('image2')) {
                $this->storeImage($product, 'image2');
                $filename = $request->file('image2')->hashName();
                $product->image2 = $filename;
                $this->makeImageThumbnail($filename);
            }

            if(request()->hasFile('image3')) {
                $this->storeImage($product, 'image3');
                $filename = $request->file('image3')->hashName();
                $product->image3 = $filename;
                $this->makeImageThumbnail($filename);
            }
            
            $product->categories()->sync(request('checkbox_for_categories'));
            $product->save();

            $status = "success";
            $message = "Product was successfully created!";
        } else {
            $status = "failure";
            $message = "Product was not successfully created!";
        }

        return redirect('admin/products')->with($status, $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title = 'Edit a Product';
        $subtitle = 'BF Admin - Products';
        $product = Product::with('categories')->findOrFail($id);
        $product_categories = Arr::pluck($product->categories->toArray(), 'id');
        $categories = Category::latest()->get();
        
        return view('admin.products.edit', compact('title','subtitle', 'product', 'product_categories', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $this->validateRequestData($request, $id);
        $validatedData['discount_applicable'] = request('discount_applicable') ? true : false;
        $validatedData['updated_by'] = Auth::user()->id;
        $validatedData['updated_at'] = Carbon::now();

        $status = "";
        $message = "";
        if(Product::find($id)->update($validatedData)){
            $product = Product::find($id);

            if(request()->hasFile('image')) {
                $this->storeImage($product, 'image');
                $filename = $request->file('image')->hashName();
                $product->image = $filename;
                $this->makeImageThumbnail($filename);
            }

            if(request()->hasFile('image2')) {
                $this->storeImage($product, 'image2');
                $filename = $request->file('image2')->hashName();
                $product->image2 = $filename;
                $this->makeImageThumbnail($filename);
            }

            if(request()->hasFile('image3')) {
                $this->storeImage($product, 'image3');
                $filename = $request->file('image3')->hashName();
                $product->image3 = $filename;
                $this->makeImageThumbnail($filename);
            }

            $product->categories()->sync(request('checkbox_for_categories'));
            $product->save();

            $status = "success";
            $message = "Product was successfully updated!";
        } else {
            $status = "failure";
            $message = "Product was not successfully updated!";
        }
        
        return redirect('/admin/products')->with($status, $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);

        $status = "";
        $message = "";
        if($product->delete()){
            $status = "success";
            $message = "Product was successfully deleted!";
        } else {
            $status = "failure";
            $message = "Product was not successfully deleted!";
        }
        
        return redirect('/admin/products')->with($status, $message);
    }

    /**
     * Validates the data within a request
     * @param  Request $request
     * @param  INT $id      
     * @return Mixed          validated data
     */
    private function validateRequestData($request, $id = null)
    {
        $validatedData = request()->validate([
            "title" => 'required|max:100|unique:products,title,'.$id,
            "description" => 'required',
            "price" => 'required|numeric|between:0,999.99',
            "quantity" => 'required|numeric',
            "max_days_for_delivery" => 'required|numeric',
            "reorder_level" => 'required|numeric',
            "discount_in_price" => 'nullable',
            "discount_in_price" => 'nullable|numeric|between:0,999.99',
            "checkbox_for_categories" => "required",
            "image" => 'nullable|file|image|max:4000'
        ]);

        return $validatedData;
    }

    private function storeImage($product, $fieldName)
    {
        if(request()->hasFile($fieldName)) {
            $product->update([
                $fieldName => request()->$fieldName->store('images/flowers', 'public'),
                $fieldName => request()->$fieldName->store('images/flowers/thumbs', 'public')
            ]);
        }
    }

    private function makeImageThumbnail($fileName)
    {
        $image = Image::make(public_path('storage/images/flowers/thumbs/' . $fileName))->fit(100, 100);
        $image->save();
    }
}