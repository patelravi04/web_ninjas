<?php
// namespave for admin
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
// use of DeliveryCharge model
use App\DeliveryCharge;
use Carbon\Carbon;
use Auth;

class DeliveryChargesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Delivery Charges';
        $subtitle = 'BF Admin - Delivery Charges';
        $slug = 'delivery_charges';
        // getiign data through model
        $delivery_charges = DeliveryCharge::latest()->get();
        return view('admin.delivery_charges.index', compact('title', 'subtitle', 'slug', 'delivery_charges'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Create new Charges for Delivery';
        $subtitle = 'BF Admin - Create delivery Charges';
        $slug = 'create-delivery_charges';
        return view('admin.delivery_charges.create', compact('title', 'subtitle', 'slug'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $this->validateRequestData($request);
        $validatedData['created_by'] = Auth::user()->id;

        $status = "";
        $message = "";
        // if conditin to check the error status
        if(DeliveryCharge::create($validatedData)){
            $status = "success";
            $message = "Delivery Charges was successfully created!";
        } else {
            $status = "failure";
            $message = "Delivery Charges was not successfully created!";
        }

        return redirect('admin/delivery_charges')->with($status, $message);   
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title = 'Edit a Charges for Delivery';
        $subtitle = 'BF Admin - Delivery Charges';      
        $delivery_charge = DeliveryCharge::findOrFail($id);
        return view('admin.delivery_charges.edit', compact('title','subtitle', 'delivery_charge'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $this->validateRequestData($request, $id);
        $validatedData['updated_by'] = Auth::user()->id;
        $validatedData['updated_at'] = Carbon::now();

        $status = "";
        $message = "";
        if(DeliveryCharge::whereId($id)->update($validatedData)){
            $status = "success";
            $message = "Delivery Charges was successfully updated!";
        } else {
            $status = "failure";
            $message = "Delivery Charges was not successfully updated!";
        }

        return redirect('admin/delivery_charges')->with($status, $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delivery_charge = DeliveryCharge::find($id);
        $status = "";
        $message = "";
        if($delivery_charge->delete()){
            $status = "success";
            $message = "Delivery Charges was successfully deleted!";
        } else {
            $status = "failure";
            $message = "Delivery Charges was not successfully deleted!";
        }

        return redirect('/admin/delivery_charges')->with($status, $message);
    }

    /**
     * Validates the data within a request
     * @param  Request $request
     * @param  INT $id      
     * @return Mixed          validated data
     */
    private function validateRequestData($request, $id = null)
    {
        /**
         * $validatedData -- check for validation
         * @var array
         */
        $validatedData = request()->validate([
          "cost" => 'required|numeric|max:999.99',
          "days_for_delivery" => 'required|max:3'
        ]);

        return $validatedData;
    }
}
