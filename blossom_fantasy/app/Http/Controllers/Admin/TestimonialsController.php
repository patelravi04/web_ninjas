<?php
// namespave for admin
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
// use of testimonials model
use App\Testimonial;
use Carbon\Carbon;
class TestimonialsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Testimonials';
        $subtitle = 'BF Admin - Testimonials';
        $slug = 'testimonials';
        // getting data through model
        $testimonials = Testimonial::with('users')->latest()->get();

        return view('admin.testimonials.index', compact('title', 'subtitle', 'slug', 'testimonials'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $title = 'Testimonials';
        $subtitle = 'BF Admin - Testimonials';
        $slug = 'testimonials';
        // getting data through model
        $testimonial = Testimonial::with('users')->find($id);

        //dd($testimonials->toArray());

        return view('admin.testimonials.show', compact('title', 'subtitle', 'slug', 'testimonial'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
