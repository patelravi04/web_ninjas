<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\About;
use Carbon\Carbon;
use Intervention\Image\Facades\Image;
use Auth;

class AboutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'about';
        $subtitle = 'BF Admin - about';
        $slug = 'about';
        $abouts = About::latest()->get();
        return view('admin.about.index', compact('title', 'subtitle', 'slug', 'abouts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Create new About';
        $subtitle = 'BF Admin - Create about';
        $slug = 'create-about';
        return view('admin.about.create', compact('title', 'subtitle', 'slug'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $this->validateRequestData($request);
        $validatedData['created_by'] = Auth::user()->id;

        $status = "";
        $message = "";
        if($about = About::create($validatedData)){
            if(request()->hasFile('image')) {
                $this->storeImage($about, 'image');
                $filename = $request->file('image')->hashName();
                $about->image = $filename;
                $this->makeImageThumbnail($filename);
            }

            $about->save();

            $status = "success";
            $message = "Timeline was successfully created!";
        } else {
            $status = "failure";
            $message = "Timeline was not successfully created!";
        }

        return redirect('admin/about')->with($status, $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title = 'Edit a about';
        $subtitle = 'BF Admin - about';      
        $about = About::findOrFail($id);
        return view('admin.about.edit', compact('title','subtitle', 'about'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $this->validateRequestData($request, $id);
        $validatedData['updated_by'] = Auth::user()->id;
        $validatedData['updated_at'] = Carbon::now();

        $status = "";
        $message = "";
        if(About::find($id)->update($validatedData)){
            $about = About::find($id);

            if(request()->hasFile('image')) {
                $this->storeImage($about, 'image');
                $filename = $request->file('image')->hashName();
                $about->image = $filename;
                $this->makeImageThumbnail($filename);
            }

            $about->save();

            $status = "success";
            $message = "Timeline was successfully updated!";
        } else {
            $status = "failure";
            $message = "Timeline was not successfully updated!";
        }
        
        return redirect('/admin/about')->with($status, $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $about = About::find($id);

        $status = "";
        $message = "";
        if($about->delete()){
            $status = "success";
            $message = "about was successfully deleted!";
        } else {
            $status = "failure";
            $message = "about was not successfully deleted!";
        }
        
        return redirect('/admin/about')->with($status, $message);
    }


    /**
     * Validates the data within a request
     * @param  Request $request
     * @param  INT $id      
     * @return Mixed          validated data
     */
    private function validateRequestData($request, $id = null)
    {

        $validatedData = request()->validate([
          "image" => 'nullable|file|image|max:4000',
          "timeline" => 'required'
        ]);

        return $validatedData;
    }

    private function storeImage($about, $fieldName)
    {
        if(request()->hasFile($fieldName)) {
            $about->update([
                $fieldName => request()->$fieldName->store('images/about', 'public'),
                $fieldName => request()->$fieldName->store('images/about/thumbs', 'public')
            ]);
        }
    }

    private function makeImageThumbnail($fileName)
    {
        $image = Image::make(public_path('storage/images/about/thumbs/' . $fileName))->fit(100, 100);
        $image->save();
    }


}
