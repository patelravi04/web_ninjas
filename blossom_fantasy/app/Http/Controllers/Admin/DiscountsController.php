<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Discount;
use Carbon\Carbon;
use Auth;

class DiscountsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Discounts';
        $subtitle = 'BF Admin - Discounts';
        $slug = 'discounts';
        /*$discounts = Discount::where('deleted_at', '=', FALSE)->get();*/
        $discounts = Discount::latest()->get();
        return view('admin.discounts.index', compact('title', 'subtitle', 'slug', 'discounts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Create a Discount';
        $subtitle = 'BF Admin - Create Discount';
        $slug = 'create-discount';
        return view('admin.discounts.create', compact('title', 'subtitle', 'slug'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $this->validateRequestData($request);
        $validatedData['created_by'] = Auth::user()->id;
  
        $status = "";
        $message = "";
        if(Discount::create($validatedData)){
            $status = "success";
            $message = "Discount was successfully created!";
        } else {
            $status = "failure";
            $message = "Discount was not successfully created!";
        }
  
        return redirect('admin/discounts')->with($status, $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title = 'Edit a Discount';
        $subtitle = 'BF Admin - Discounts';      
        $discount = Discount::findOrFail($id);
        return view('admin.discounts.edit', compact('title','subtitle', 'discount'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $this->validateRequestData($request, $id);
        $validatedData['updated_by'] = Auth::user()->id;
        $validatedData['updated_at'] = Carbon::now();

        $status = "";
        $message = "";
        if(Discount::whereId($id)->update($validatedData)){
            $status = "success";
            $message = "Discount was successfully updated!";
        } else {
            $status = "failure";
            $message = "Discount was not successfully updated!";
        }
        
        return redirect('/admin/discounts')->with($status, $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $discount = Discount::find($id);

        $status = "";
        $message = "";
        if($discount->delete()){
            $status = "success";
            $message = "Discount was successfully deleted!";
        } else {
            $status = "failure";
            $message = "Discount was not successfully deleted!";
        }
        
        return redirect('/admin/discounts')->with($status, $message);
    }

    /**
     * Validates the data within a request
     * @param  Request $request
     * @param  INT $id      
     * @return Mixed          validated data
     */
    private function validateRequestData($request, $id = null)
    {

        $validatedData = request()->validate([
          /*"percentage" => 'required|string|max:50|unique:categories,name,'.$id,*/
          "percentage" => 'required|min:2|regex:/^([0-9]{2})$/',
          "description" => 'required'
        ]);

        return $validatedData;
    }


}
