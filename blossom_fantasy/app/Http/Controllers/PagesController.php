<?php

namespace App\Http\Controllers;
use App\Product;
use App\Category;

use App\User;

use App\About;

use Illuminate\Http\Request;

use App\Faq;
use Auth;
use App\Address;
use Pagerange\Bx\_5bx;
use Carbon\Carbon;
use DB;
use Mail;

class PagesController extends Controller
{
    /**
     * home page path
     * @return string and view page
     */
    public function home()
    {
        $title = 'Blossom Fantasy - Home';
        $slug = 'home';
        return view('home', compact('title', 'slug'));
    }
    
    /**
     * shop page path
     * @return string and view page
     */
    public function shop()
    {
        $title = 'Blossom Fantasy - Shop';
        $slug = 'shop';
        $products = Product::with('categories')->latest()->paginate(10);
        $categories = Category::latest()->get();
        $categorySlug = '';
        return view('shop', compact('title', 'slug','products','categories', 'categorySlug'));
    }

    /**
     * shop page path
     * @return string and view page
     */
    public function shopByCategory($id){
        $title = 'Blossom Fantasy - Shop';
        $slug = 'shop';

        if($id == 'all'){
            $products = Product::with('categories')->latest()->paginate(10);
            $categorySlug = 'all';
        } else {
            $products = Product::with('categories')->whereHas('categories', function ($query) use ($id) {
                $query->where('category_id', '=', $id);
            })->latest()->paginate(10);
            $categorySlug = $id;
        }

        $categories = Category::latest()->get();
        return view('shop', compact('title', 'slug','products','categories','categorySlug'));
    }

    public function shopDetails($id){
        $title = 'Blossom Fantasy - Product Details';
        $slug = 'shop';
        $product = Product::with('categories')->findOrFail($id);
        
        $arrCategory = [];
        foreach($product->categories as $category) {
            $arrCategory[] = $category->id;
        }
        $strCategories = implode(",", $arrCategory);

        $similarProducts = Product::whereHas('categories', function ($query) use ($arrCategory, $product) {
                       $query->where('Product_id', '!=', $product->id)->whereIn('category_id', $arrCategory);
                    })->latest()->take(4)->get();
        
        return view('shop_detail', compact('title', 'slug', 'product', 'similarProducts'));
    }
    
    /**
     * about page path
     * @return string and view page
     */
    public function about()
    {
        $title = 'Blossom Fantasy - About';
        $slug = 'about';
        $abouts = About::get();
        return view('about', compact('title', 'slug', 'abouts'));
    } 
    
    /**
     * contact page path
     * @return string and view page
     */
    public function contact()
    {
        $title = 'Blossom Fantasy - Contact';
        $slug = 'contact';
        return view('contact', compact('title', 'slug'));
    } 
    
    /**
     * privacy page path
     * @return string and view page
     */
    public function privacy()
    {
        $title = 'Blossom Fantasy - Privacy';
        $slug = 'privacy';
        return view('privacy', compact('title', 'slug'));
    }
    
    /**
     * terms page path
     * @return string and view page
     */
    public function terms()
    {
        $title = 'Blossom Fantasy - Terms';
        $slug = 'terms';
        return view('terms', compact('title', 'slug'));
    }
    
    /**
     * login page path
     * @return string and view page
     */
    public function login()
    {
        $title = 'Blossom Fantasy - Login';
        $slug = 'login';
        dd($slug);
        return view('login', compact('title', 'slug'));
    } 
    
    /**
     * Register page path
     * @return string and view page
     */
    public function register()
    {
        $title = 'Blossom Fantasy - Register';
        $slug = 'register';
        return view('register', compact('title', 'slug'));
    }
    
    /**
     * cart page path
     * @return string and view page
     */
    public function cart()
    {
        $title = 'Blossom Fantasy - Cart';
        $slug = 'cart';
        return view('cart', compact('title', 'slug'));
    }

    /**
     * checkout page path
     * @return string and view page
     */
    public function checkout()
    {
        $title = 'Blossom Fantasy - Checkout';
        $slug = 'checkout';
        $addresses = Address::where('user_id', '=', Auth::user()->id)->latest()->get();
        return view('checkout',compact('title', 'slug', 'addresses'));
    }

    public function paymentOrder(Request $request)
    {
        $title = 'Blossom Fantasy - Payment';
        $slug = 'payment';

        request()->validate([
            'billing_address' => 'required',
            'shipping_address' => 'required',
            'card_num' => 'required|numeric|digits_between:15,16',
            'card_type' => 'required',
            'expiry' => 'required|numeric|digits:4|max:1299',
            'cvv' => 'required|numeric|digits:3'
        ]);

        //dd($request->toArray());

        $order_id = DB::table('orders')->insertGetId([
                    'user_id' => Auth::user()->id,
                    'ordered_date' => Carbon::now(),
                    'billing_address' => $request->billing_address,
                    'shipping_address' => $request->shipping_address,
                    'sub_total' => $request->sub_total,
                    'gst' => $request->gst,
                    'pst' => $request->pst,
                    'total' => $request->total,
                    'status' => 'ordered',
                    'created_by' => Auth::user()->id
                ]);
        //dd($order_id);
        

        if(session()->has('cart')){
            $cartItems = session('cart');
            
            foreach ($cartItems as $key => $singleItem){
                $product_id = $singleItem[key($singleItem)]['id'];
                $price = $singleItem[key($singleItem)]['price'];
                $discount_in_price = $singleItem[key($singleItem)]['discount_in_price'];
                $qty = $singleItem[key($singleItem)]['ordered_qty'];

                DB::table('order_line_items')->insert([
                        'order_id' => $order_id,
                        'product_id' => $product_id,
                        'unit_price' => $price,
                        'discount' => $discount_in_price,
                        'quantity' => $qty
                    ]);
            }
            session()->forget('cart');
        }

        $refNo = "bf" . (1000 + $order_id);

        DB::table('orders')
            ->where('id', $order_id)
            ->update(['refNo' => $refNo]);

            
        // You need your login and API key to begin
        $transaction = new _5bx("5664401", "5ae85773240c184b594e05c975bcc682");

        $response = $this->processTransaction($transaction, $request, $refNo);

        // Figure out what's in the response and how to use it
        //dd($response);

        if($response->result_code == "ok"){
            $transaction_reference = $response->transaction_response->trans_id;
            //dd($transaction_reference);

            $transaction_id = DB::table('transactions')->insertGetId([
                    'user_id' => Auth::user()->id,
                    'reference_number' => $transaction_reference,
                    'created_by' => Auth::user()->id
                ]);

            DB::table('orders')
                ->where('id', $order_id)
                ->update(['transaction_id' => $transaction_id, 'status' => 'paid']);

            session()->forget('cart');

            $data['order_reference'] = $refNo;
            $data['transaction_reference'] = $transaction_reference;

            Mail::send('emails.thankyou', $data, function ($message) {
                $message->from('postmaster@webninjas.com', 'Blossom Fantasy');
                $message->subject('Thank you for your purchase');
                $message->to(Auth::user()->email);
            });

            return view('/thankyou', compact('title','subtitle', 'refNo', 'transaction_reference'));
            
        } else {
            //dd($response->toArray());
            return redirect('/payment')->with('success', 'Something went wrong. Please try again.');
        }
        
    }

    public function sendMailFromContact(Request $request)
    {
        $title = 'Blossom Fantasy - Payment';
        $slug = 'payment';

        request()->validate([
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required|numeric',
            'message' => 'required'
        ]);

        $messageBody = "Name: " . $request->name;
        $messageBody .= "\nEmail: " . $request->email;
        $messageBody .= "\nPhone: " . $request->phone;
        $messageBody .= "\nMessage: " . $request->message;

        Mail::raw($messageBody, function ($message) {
            $message->to('patel.ravi04@gmail.com')
            ->subject('Blossom Fantasy - Contact Page');
        });

        return redirect('/contact')->with('success', 'Email has been sent successfully.');
    }

    public function processTransaction(_5bx $transaction, $request, $refNo)
    {
        // Replace hard coded values with your own variables
        $transaction->amount($request->total); // total sale
        $transaction->card_num($request->card_num); // credit card number
        $transaction->exp_date ($request->expiry); // expiry date month and year (august 2022)
        $transaction->cvv($request->cvv); // card cvv number
        $transaction->ref_num($refNo); // your reference or invoice number
        $transaction->card_type($request->card_type); // card type (visa, mastercard, amex)
        return $transaction->authorize_and_capture(); // returns JSON object
    }

    /**
     * faq page path
     * @return string and view page
     */
    public function faq()
    {
        $title = 'Blossom Fantasy - FAQ';
        $slug = 'faq';
        $faqs = Faq::latest()->get();
        return view('faq',compact('title','slug','faqs'));
    }

    /**
     * orders page path
     * @return string and view page
     */
    public function orders()
    {
        $title = 'Blossom Fantasy - Orders';
        $slug = 'Orders';
        return view('orders', compact('title','slug'));      
    }

    /**
     * profile page path
     * @return string and view page
     */
    public function profile()
    {
        $title = 'Blossom Fantasy - Profile';
        $slug = 'profile';
        $users = User::latest()->get();
        return view('profile', compact('title', 'slug','users'));
    }

    /**
     * thankyou page path
     * @return string and view page
     */
    public function thankyou()
    {
        $title = 'Blossom Fantasy - Thank You';
        $slug = 'thank you';
        return view('thankyou', compact('title', 'slug'));
    }

}
