<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class CartController extends Controller
{

    public function updateItem(Request $request, $flag, $id)
    {
    	//dd($request->toArray());
    	$product = Product::with('categories')->findOrFail($id);

    	$item = array(
			'product_id' => $id,
			'quantity' => $request->qty
		);

		$message = "";

    	if(session()->has('cart')){
    		$cartItems = session('cart');
    		//dd($cartItems);
    		foreach ($cartItems as $key => $singleItem){
    			//dd($key);
    			//dd($singleItem[key($singleItem)]);
    			if(key($singleItem) == $id){
    				if($flag == 'remove') {
    					$item['quantity'] = $singleItem[key($singleItem)]['ordered_qty'] - $item['quantity'];

    					$message = "item has been removed successfully";
    				} elseif ($flag == 'add') {
    					$item['quantity'] = $singleItem[key($singleItem)]['ordered_qty'] + $item['quantity'];

    					$message = "item has been added successfully";
    				} else {
    					$item['quantity'] += $singleItem[key($singleItem)]['ordered_qty'];

    					$message = "item has been added successfully";
    				}
    				
    				//dd($cartItems[$key][$id]);
    				//dd($item['quantity']);
	                unset($cartItems[$key]);
	            }

	        }
	        session()->put('cart',$cartItems);
	        //dd(session('cart'));
    	}
    	//dd($item['quantity']);

    	if($item['quantity'] != 0){
    		$cartItem[$item['product_id']] = $product->toArray();
			$cartItem[$item['product_id']]['ordered_qty'] = $item['quantity'];
			//dd($cartItem[$id]);
			session()->push('cart', $cartItem);	
    	}
		//dd(session('cart'));
		
		if($message == ""){
			$message = "item has been added successfully";
		}
		
		return redirect('/cart/')->with('success', $message);
    }

    public function clearCart()
    {
    	session()->forget('cart');
    	return redirect('/cart/')->with('success', 'Cart has been cleared');
    }
}
