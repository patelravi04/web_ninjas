<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Notifications\Notifiable;

class User extends Authenticatable implements MustVerifyEmail
{

    /**
     * Eloquent's "soft delete" model
     */
    use SoftDeletes;

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $primaryKey = 'id';

    protected $fillable = [
        'first_name','last_name','phone', 'email', 'password', 'role','created_by', 'updated_by', 'updated_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * function for many relationship with testimonials in user model
     * @return [type] data
     */
    public function testimonials()
    {
        return $this->hasMany('App\Testimonial');
    }

    public function transactions()
    {
        return $this->hasMany('App\Transaction');
    } 

    public function orders()
    {
        return $this->hasMany('App\Order');
    }           
}
