<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DeliveryCharge extends Model
{	
    /**
     * Eloquent's "soft delete" model
     */
    use SoftDeletes;

    /**
     * fillable for create table
     * @var array
     */
    protected $fillable = [
        'cost', 'days_for_delivery','created_by','updated_by','updated_at'
    ];
}
