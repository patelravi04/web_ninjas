@extends('layout')

@section('style')
<style type="text/css">
	/*order details at right side of page */
	div.price_list{
		border: 2px solid #c53337;
		border-radius: 20px;
		padding:20px;
	}

	div.price_detail{
		margin-bottom: 50px;
	}

	.price{
		color: #c53337;
		font-weight: bold;
	}

	.save{
		text-align: center;
		padding: 12px;
		color: #c53337;
	}

	/*form Css*/

	div#cart1{
		padding: 30px 45px;
	}

	div.item{
		padding-left: 40px;
	}

	.btn-primary{
		background-color: #EC3939;
		border: 2px solid #EC3939;
		width: 100%;
		margin-top: 20px;
	}

	div.item label{
		font-size: 20px;
		padding: 10px 0;
	}

	.field_imp{
		color: #c53337;
	}

	fieldset,fieldset.fieldset{
		border: 2px solid #c53337;
		padding:20px 40px ;
		margin-bottom: 50px;
		border-radius: 20px;
	}

	h3{
		text-align: center;
	}

	div.empty{
		margin-top: 40px;
	    text-align: center;
	}

	table.cart thead tr th{
		background-color: #c53337 !important;
	}

	table.cart tr td img{
		max-width: 100px;
	}

	td.update_qty{
		min-width: 150px;
	}

	td.update_qty span{
		float: left;
		margin-top: 30px;
	}

	td.update_qty form{
		display: inline;
		margin: 0 10px;
		float: left;
	}

	form legend{
		width: auto;
	    padding: 10px 20px;
	    background-color: #C53337;
	    color: #FFF;
	}

</style>
@endsection('style')
@section('content')
	<div class="row col-md-12">
		<div id="cart1" class="col-md-12">
			<h1>Checkout</h1>
		</div> <!-- cart -->
		</div> <!-- row -->
		<div id="primary">
			<div class="row col-md-12">
				<div class="item col-md-8">
					<div class="row product_detail col-md-12">
						<div class="col">
							<h3>All Item(s)</h3>
						</div>
					</div>

					@if(session()->has('cart') && count(session('cart')) > 0)
							<table class="table cart">
							  	<thead class="thead-dark">
							    	<tr>
							      		<th scope="col">Product</th>
								      	<th scope="col">Details</th>
								      	<th scope="col">Quantity</th>
							    	</tr>
							  	</thead>
							  	<tbody>

								@php
									$subtotal = 0;
									$gst = 0;
									$pst = 0;
									$total = 0;
									$totalDiscount = 0;
								@endphp
								  
								@foreach (session('cart') as $key => $singleItem)
									@php
										$id = $singleItem[key($singleItem)]['id'];
										$image = $singleItem[key($singleItem)]['image'];
										$title = $singleItem[key($singleItem)]['title'];
										$price = $singleItem[key($singleItem)]['price'];
										$discount_in_price = $singleItem[key($singleItem)]['discount_in_price'];
										$final_price = $price - $discount_in_price;
										$qty = $singleItem[key($singleItem)]['ordered_qty'];
										$ordered_price = $final_price * $qty;
									@endphp
									<tr>
							      		<td scope="row">
							      			<img src="{{ asset('storage') . '/images/flowers/thumbs/' . $image }}" class="card-img-top" alt="product_1">		
							      		</td>
							      		<td>
							      			<strong title>Title: </strong>
							      			{{ $title }}
							      			<br>
							      			<strong>Price: </strong>
							      			${{ $final_price }}
							      			@if($discount_in_price > 0)
							      			<span style="text-decoration: line-through; color: #cfcfcf;padding-left: 5px;">
							      				${{ $price }}
							      			</span>
							      			@endif
							      			<br>
							      			<strong>Total: </strong>
							      			${{ $ordered_price }}
							      		</td>
							      		<td class="update_qty">
							      			<span>
							      				{{ $qty }}
							      			</span>
							      		</td>
							    	</tr>
							
									@php
										$subtotal += $ordered_price;
										$totalDiscount += $discount_in_price * $qty;
									@endphp
								@endforeach

								@php
									$gst = (7 * $subtotal) / 100;
									$pst = (5 * $subtotal) / 100;
									$total = $subtotal + $gst + $pst;
								@endphp
								</tbody>
							</table>
						@else
							<div class="empty">
								<p>Your cart is empty</p>
								<a href="/shop" class="btn btn-danger">
									Continue Shopping
								</a>
							</div>
						@endif
				
				</div> <!-- item -->

				<!-- order detaisl at right hand side of page -->
				@if(session()->has('cart') && count(session('cart')) > 0)
					<div class="price_detail col-md-4">
						<div class="price_list">
							<div class="row order_total col-md-12">
								<div class="col-8">
									<p>Order Subtotal</p>
									<p>Tax (GST)</p>
									<p>Tax (PST)</p>
									<p>Total</p>
								</div>
								<div class="col-4">
									<p>${{ $subtotal }}</p>
									<p>${{ $gst }}</p>
									<p>${{ $pst }}</p>
									<p>${{ $total }}</p>
								</div>
							</div>
							@if($totalDiscount > 0)
							<div class="save">
								<big>You Save ${{ $totalDiscount }} </big>
							</div>
							@endif
						</div> <!-- price_list -->
					</div> <!-- price detail -->
				@endif

				<div class="col-md-12">
					<!-- form for registration -->
					<form method="post" action="/payment">
						@csrf
						<fieldset>
							<legend>Payment Details</legend>
							<p>(<span class="field_imp"><strong>*</strong></span>) Fields are mendatory</p>
							
							<input type="hidden" name="sub_total" value="{{$subtotal}}">
							<input type="hidden" name="gst" value="{{$gst}}">
							<input type="hidden" name="pst" value="{{$pst}}">
							<input type="hidden" name="total" value="{{$total}}">

					    	<!-- fiest name field -->
						  	<div class="form-group">
						  		<label for="first_name">First Name <span class="field_imp">*</span></label>
						    	<input type="text" class="first_name form-control form-control-lg" name="first_name" id="first_name" placeholder="First Name" value="{{Auth::user()->first_name}}" disabled>
						  	</div>

						  	<!-- last name field -->
						  	<div class="form-group">
						  		<label for="last_name">Last Name</label>
						    	<input type="text" class="last_name form-control form-control-lg" name="last_name" id="last_name" placeholder="Last Name" value="{{Auth::user()->last_name}}" disabled>
						  	</div>
						    
						  	<!-- phone number field -->
						  	<div class="form-group">
						  		<label for="phone">Phone Number <span class="field_imp">*</span>
						  		</label>
						    	<input type="tel" class="phone form-control form-control-lg" name="phone" id="phone" placeholder="Delivery Phone Number" value="{{Auth::user()->phone}}" disabled>
						  	</div>

						  	<div class="form-row">
								<div class="form-group col-md-6">
									<h4>Billing Address</h4>
									<a href="/addresses" class="btn btn-danger">Manage Addresses</a>
									@foreach($addresses as $address)
									@if($address->address_type == "billing")
									<div class="form-check">
										<input class="form-check-input" type="radio" name="billing_address" id="addresses{{$address->id}}" 
										value="{{$address->address}} {{$address->city}} {{$address->province}} {{$address->postal_code}}">
									  	<label class="form-check-label" for="addresses{{$address->id}}">
									    	{{$address->address}}
											{{$address->city}}
											{{$address->province}}
											{{$address->postal_code}}
									  	</label>
									</div>
									@endif
									@endforeach
									@if($errors->has('billing_address'))
	                                    <div class="alert alert-danger" role="alert">
	                                        {{ $errors->first('billing_address') }}
	                                    </div>
	                                @endif
								</div>
								<div class="form-group col-md-6">
									<h4>Shipping Address</h4>
									<a href="/addresses" class="btn btn-danger">Manage Addresses</a>
									@foreach($addresses as $address)
									@if($address->address_type == "shipping")
									<div class="form-check">
										<input class="form-check-input" type="radio" name="shipping_address" id="addresses{{$address->id}}" 
										value="{{$address->address}} {{$address->city}} {{$address->province}} {{$address->postal_code}}
											">
									  	<label class="form-check-label" for="addresses{{$address->id}}">
									    	{{$address->address}}
											{{$address->city}}
											{{$address->province}}
											{{$address->postal_code}}
									  	</label>
									</div>
									@endif
									@endforeach
									@if($errors->has('shipping_address'))
	                                    <div class="alert alert-danger" role="alert">
	                                        {{ $errors->first('shipping_address') }}
	                                    </div>
	                                @endif
								</div>
							</div>

						  	<div class="form-group">
						  	  	<label for="card_num">Enter the Card Number <span class="field_imp">*</span></label>
							    <input type="text" class="form-control form-control-lg" name="card_num" id="card_num" placeholder="Card Number">
							    @if($errors->has('card_num'))
	                                <div class="alert alert-danger" role="alert">
	                                    {{ $errors->first('card_num') }}
	                                </div>
	                            @endif
							</div>

							<div class="form-group">
							    <label for="card_type">Card Type<span class="field_imp">*</span></label>
							    <select name="card_type" id="card_type" class="form-control form-control-lg">
							        <option value="" selected>Choose...</option>
							        <option value="visa">Visa</option>
							        <option value="mastercard">Mastercard</option>
							        <option value="amex">Amex</option>
							    </select>
							    @if($errors->has('card_type'))
	                                <div class="alert alert-danger" role="alert">
	                                    {{ $errors->first('card_type') }}
	                                </div>
	                            @endif
							</div>

							<!-- expiry data field -->
							<div class="form-row">
							    <div class="form-group col-md-6">
						  	  	  	<label for="expiry">Enter the Expiry Date <span class="field_imp">*</span></label>
							      	<input type="text" class="form-control form-control-lg" name="expiry" id="expiry" placeholder="MMYY">
							      	@if($errors->has('expiry'))
		                                <div class="alert alert-danger" role="alert">
		                                    {{ $errors->first('expiry') }}
		                                </div>
		                            @endif
							    </div>
							    <!-- cvv field -->
							    <div class="form-group col-md-6">
						  	  	  	<label for="cvv">Enter the last three digits of card<span class="field_imp">*</span></label>
							      	<input type="text" class="form-control form-control-lg" name="cvv" id="cvv" placeholder="CVV">
							      	@if($errors->has('cvv'))
		                                <div class="alert alert-danger" role="alert">
		                                    {{ $errors->first('cvv') }}
		                                </div>
		                            @endif
							    </div>
							</div>

						  	<!-- button for payment -->
						  	<button type="submit" class="btn btn-danger btn-block">Pay Now</button>
						</fieldset>
					</form>							
				</div> <!-- row -->
			
		  </div><!-- div class row-->
		</div> <!-- primary -->
@endsection('content')