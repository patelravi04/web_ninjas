@extends('admin.layout')

@section('content')
		<h1 class="dash-title">{{ $title }}</h1>

        <div class="row">
            <div class="col">
                <div class="card spur-card">
                    <div class="card-header bg-secondary text-white">
                        <div class="spur-card-icon">
                            <i class="fas fa-chart-bar"></i>
                        </div>
                        <div class="spur-card-title"> Address Form </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="/addresses" id="address" novalidate>
                            @csrf

                            <div class="form-group">
                                <label for="address_type">Address Type</label>
                                <select name="address_type" class="form-control">
                                    @foreach($addressTypes as $type)
                                        <option value={{$type}}
                                        @if(old('address_type') == $type)
                                            selected 
                                        @endif
                                        >
                                            {{ ucfirst($type) }}
                                        </option>
                                    @endforeach
                                </select>
                                @if($errors->has('address_type'))
                                    <div class="alert alert-danger" role="alert">
                                        {{ $errors->first('address_type') }}
                                    </div>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="title">Address</label>
                                <input type="text" class="form-control" 
                                    id="address" name="address" placeholder="Address" 
                                    value="{{ old('address') }}">
                                @if($errors->has('address'))
                                    <div class="alert alert-danger" role="alert">
                                        {{ $errors->first('address') }}
                                    </div>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="title">City</label>
                                <input type="text" class="form-control" 
                                    id="city" name="city" placeholder="City" 
                                    value="{{ old('city') }}">
                                @if($errors->has('city'))
                                    <div class="alert alert-danger" role="alert">
                                        {{ $errors->first('city') }}
                                    </div>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="title">Province</label>
                                <input type="text" class="form-control" 
                                    id="province" name="province" placeholder="Province" 
                                    value="{{ old('province') }}">
                                @if($errors->has('province'))
                                    <div class="alert alert-danger" role="alert">
                                        {{ $errors->first('province') }}
                                    </div>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="title">Postal Code</label>
                                <input type="text" class="form-control" 
                                    id="postal_code" name="postal_code" placeholder="Postal Code" 
                                    value="{{ old('postal_code') }}">
                                @if($errors->has('postal_code'))
                                    <div class="alert alert-danger" role="alert">
                                        {{ $errors->first('postal_code') }}
                                    </div>
                                @endif
                            </div>

                            <button type="submit" class="btn btn-primary">Save</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
@endsection