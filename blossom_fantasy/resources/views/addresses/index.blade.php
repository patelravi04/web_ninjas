@extends('layout')

@section('style')
<style type="text/css">
    h1 {
        text-align: center;
        margin: 15px 0;
        color: #C53337;
    }
    a.btn-add{
        margin-left: 20px;
    }
</style>
@endsection('style')

<!-- secition content from layout added here -->
@section('content')
		<h1 class="dash-title">{{ $title }}</h1>

        @include('partials.flash')

        <div class="row">
            <div class="col">
                <a href="/addresses/create" class="btn btn-danger btn-add">
                    Add New Address
                </a>
                <br><br>

                <div class="card spur-card">
                    <div class="card-header bg-secondary text-white">
                        <div class="spur-card-icon">
                            <i class="fas fa-table"></i>
                        </div>
                        <div class="spur-card-title">List of Addresses</div>
                    </div>
                    <div class="card-body card-body-with-dark-table">
                        <table class="table table-dark table-in-card" 
                            id="table_categories">
                            <thead>
                                <tr>
                                    <th scope="col">Type</th>
                                    <th scope="col">Address</th>
                                    <th scope="col">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($addresses)>0)
                                    @foreach($addresses as $address)
                                        <tr>
                                            <td class="title">{{ ucfirst($address->address_type) }}</td>
                                            <td>
                                                {{ $address->address }}
                                                {{ $address->city }}
                                                {{ $address->province }}
                                                {{ $address->postal_code }}
                                            </td>
                                            <td>
                                                <a href="/addresses/{{$address->id}}" class="btn btn-primary btn-sm mb-1">
                                                    Edit
                                                </a>
                                                <form class="form d-inline form-inline" 
                                                action="/addresses/{{$address->id}}"
                                                method="post">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button class="btn btn-danger btn-sm mb-1">Delete</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="3">There is no address available</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
@endsection