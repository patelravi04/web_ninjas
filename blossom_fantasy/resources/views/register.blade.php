@extends('layout')

@section('style')
<style type="text/css">
	/*order details at right side of page */
div.price_list{
	border: 2px solid #c53337;
	border-radius: 20px;
	padding:20px;
}

div.price_detail{
	margin-bottom: 50px;
}

.price{
	color: #c53337;
	font-weight: bold;
}

.order_total,.price_disc{
	border-bottom: 2px solid #cfcfcf;
}

.save{
	text-align: center;
	padding: 12px;
	color: #c53337;
}

/*form Css*/

div#cart1{
	padding: 50px 60px;
}

div.item{
	padding-left: 40px;
}

.btn-primary{
	background-color: #EC3939;
	border: 2px solid #EC3939;
	width: 100%;
	margin-top: 20px;
}

div.item label{
	font-size: 20px;
	padding: 10px 0;
}

.field_imp{
	color: #c53337;
}

fieldset,fieldset.fieldset{
	border: 2px solid #c53337;
	padding:20px 40px ;
	margin-bottom: 50px;
	border-radius: 20px;
}

/* CSS for Login Page */
.item{
	margin-right: 50px;
}

.not_account{
	margin-bottom: 30px;
	margin-left: 10px;
}

/* CSS for Register Page */
fieldset.fieldset{
	padding-top: 30px;
	padding-bottom: 30px;
}

.register{
	margin-right: 50px;
	margin-left: 50px;
}
</style>
@endsection('style')
@section('content')
<div class="row col-md-12">
	<div id="cart1" class="col-md-6">
		<h1>Register Now</h1>
	</div> <!-- cart -->
	</div> <!-- row -->
	<div id="primary" class="col-md-12">
		<div class="register">
			<!-- form starts here -->
			<form>
			  <fieldset class="fieldset">
				  <div class="form-row">
				  	@csrf
				  	
				  	<!-- first name field -->
				    <div class="form-group col-md-6">
				      <label for="first_name">First Name<span class="field_imp">*</span></label>
				      <input type="text" class="form-control" id="first_name" placeholder="First Name">
				    </div>
				    <!-- last name field -->
				    <div class="form-group col-md-6">
				      <label for="last_name">Last Name</label>
				      <input type="text" class="form-control" id="last_name" placeholder="Last Name">
				    </div>
				  </div>
				  <!-- adress 1 field -->
				  <div class="form-group">
				    <label for="inputAddress">Address 1<span class="field_imp">*</span></label>
				    <input type="text" class="form-control" id="inputAddress" placeholder="1234 Main St">
				  </div>
				  <!-- address 2 field -->
				  <div class="form-group">
				    <label for="inputAddress2">Address 2</label>
				    <input type="text" class="form-control" id="inputAddress2" placeholder="Apartment, studio, or floor">
				  </div>
				  <!-- city field -->
				  <div class="form-row">
				    <div class="form-group col-md-6">
				      <label for="inputCity">City<span class="field_imp">*</span></label>
				      <input type="text" class="form-control" id="inputCity">
				    </div>
				    <!-- state field -->
				    <div class="form-group col-md-4">
				      <label for="inputState">State<span class="field_imp">*</span></label>
				      <select id="inputState" class="form-control">
				        <option selected>Choose...</option>
				        <option>Alberta</option>
				        <option>British Columbia</option>
				        <option>Manitoba</option>
				        <option>New Brunswick</option>
				        <option>Newfoundland and Labrador</option>
				        <option>Nova Scotia</option>
				        <option>Ontario</option>
				        <option>Prince Edward Island</option>
				        <option>Quebec</option>
				        <option>Saskatchewan</option>
				      	<option>Northwest Territories</option>
				        <option>Nunavut</option>
				        <option>Yukon</option>
				      </select>
				    </div>
				    <!-- zip code field -->
				    <div class="form-group col-md-2">
				      <label for="inputZip">Zip Code<span class="field_imp">*</span></label>
				      <input type="text" class="form-control" id="inputZip">
				    </div>
				  </div>
				  <!-- phone field -->
				  <div class="form-group">
				    <label for="phone">Phone</label>
				    <input type="tele" class="form-control" id="phone" placeholder="contact number">
				  </div>
				  <!-- email field -->
				  <div class="form-group">
				    <label for="email">Email<span class="field_imp">*</span></label>
				    <input type="email" class="form-control" id="email" placeholder="email id">
				  </div>
				  <div class="form-row">
				  	<!-- password field -->
				    <div class="form-group col-md-6">
				      <label for="inputPassword4">Password<span class="field_imp">*</span></label>
				      <input type="password" class="form-control" id="inputPassword4" placeholder="Password">
				    </div>
				    <!-- password confirm field -->
				    <div class="form-group col-md-6">
				      <label for="confirm_password">Confirm Password<span class="field_imp">*</span></label>
				      <input type="password" class="form-control" id="confirm_password" placeholder="Confirm Password">
				    </div>
				  </div>
				  <!-- signin button -->
				  <button type="submit" class="btn btn-primary btn-block">Sign in</button>
				</fieldset>
			</form>
		</div> 					
	</div> <!-- primary -->
@endsection('content')