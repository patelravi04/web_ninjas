@extends('layout')

@section('style')

<style>
	div.callout {
	display: relative;
	padding: 0;
	padding-bottom: 30px;
}

div.callout div img {
	width: 100%;
	height: auto;
}

div.similar div.caption {
	position: absolute;
	bottom: 0;
	height: 50px;
	line-height: 50px;
	background-color: rgba(178,31,45,.7);
	color: #fff;
	padding-left: 10px;
	display: block;
}

div.callout > div {
	position: relative;
	overflow: hidden;
}

div.callout .caption {
	background: transparent;
}

div.callout .caption a {
	text-shadow: 1px 1px 1px #000;
}

div.caption a {
	color: #fff;
	line-height: 50px;
	margin-left: 10px;
}

div#primary {
	padding: 0;
	background-color: #FFF;
}


.alignleft {
	float: left;
	max-width: 100%;
}


h1 {
	text-align: center;
	margin: 20px 0 15px;
}
h3.price{
	color: #c53337;
}


div.right{
	/*border: 1px solid #cfcfcf;*/
	box-sizing: border-box;
    max-height: 100%
}
div.row{
	padding: 14px;
}

div.illust{
	/*border: 1px solid #cfcfcf;*/
	box-sizing: border-box;	
	position: relative;
	bottom: 11px;
	border-top: none;
	padding-right: 5px; 		
}

.bt1{
   position: relative;
   left: 180px
   
}

.bt2{
	position: relative;
	left: 190px;

}
div.bt{
	/*border-bottom:1px solid #cfcfcf;*/
	box-sizing: border-box;
}

#secondary{
	padding-top: 10px;
}

#myCarousel .list-inline {
    white-space:nowrap;
    overflow-x:auto;
}

#myCarousel .carousel-indicators {
    position: static;
    left: initial;
    width: initial;
    margin-left: initial;
}

#myCarousel .carousel-indicators > li {
    width: initial;
    height: initial;
    text-indent: initial;
}

#myCarousel .carousel-indicators > li.active img {
    opacity: 0.7;
}

div.similar a{
	position: relative;
	min-height: 200px;
    max-height: 200px;
    display: block;
    background-color: #efefef;
    border: 1px solid #C53337;
    overflow: hidden;
}

</style>

@endsection('style')

@section('content')		
		<div id="primary">
			<div class="container">
			    <div class="row">
			        <div class="col-lg-6" id="slider">
			            <div id="myCarousel" class="carousel slide shadow">
			                <!-- main slider carousel items -->
			                <div class="carousel-inner">
			                	@if($product->image)
		                		<div class="active carousel-item" data-slide-number="0">
			                        <img src="{{ asset('storage') . '/images/flowers/' . $product->image}}" class="img-fluid" alt={{ $product->image }}>
			                    </div>
			                	@endif

			                	@if($product->image2)
		                		<div class="carousel-item" data-slide-number="1">
			                        <img src="{{ asset('storage') . '/images/flowers/' . $product->image2}}" class="img-fluid" alt={{ $product->image2 }}>
			                    </div>
			                	@endif

			                	@if($product->image3)
		                		<div class="carousel-item" data-slide-number="2">
			                        <img src="{{ asset('storage') . '/images/flowers/' . $product->image3}}" class="img-fluid" alt={{ $product->image3 }}>
			                    </div>
			                	@endif

			                    <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
			                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
			                        <span class="sr-only">Previous</span>
			                    </a>
			                    <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
			                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
			                        <span class="sr-only">Next</span>
			                    </a>

			                </div>
			                <!-- main slider carousel nav controls -->

			                <ul class="carousel-indicators list-inline mx-auto border px-2">
			                	@if($product->image)
			                	<li class="list-inline-item active">
			                        <a id="carousel-selector-0" class="selected" data-slide-to="0" data-target="#myCarousel">
			                            <img src="{{ asset('storage') . '/images/flowers/thumbs/' . $product->image}}" class="img-fluid" alt={{ $product->image }}>
			                        </a>
			                    </li>
			                	@endif

			                	@if($product->image2)
			                	<li class="list-inline-item">
			                        <a id="carousel-selector-1" class="selected" data-slide-to="1" data-target="#myCarousel">
			                            <img src="{{ asset('storage') . '/images/flowers/thumbs/' . $product->image2}}" class="img-fluid" alt={{ $product->image2 }}>
			                        </a>
			                    </li>
			                	@endif

			                	@if($product->image3)
			                	<li class="list-inline-item">
			                        <a id="carousel-selector-2" class="selected" data-slide-to="2" data-target="#myCarousel">
			                            <img src="{{ asset('storage') . '/images/flowers/thumbs/' . $product->image3}}" class="img-fluid" alt={{ $product->image3 }}>
			                        </a>
			                    </li>
			                	@endif
			                </ul>
			            </div>
			        </div><!--/main slider carousel-->

			    	<div class="col-lg-6 right">
			    		<h1>{{ $product->title }}</h1>
						<h2>Category</h2>
						<ul>
							@foreach($product->categories as $category)
							<li>{{ $category->name }}</li>
							@endforeach
						</ul>
						<h3 class="price">${{$product->price}}</h3>
						<br>
						<form method="get" action="/addToCart/new/{{$product->id}}">
							<label for="qty">Quantity</label>
							<input type="number" class="form-control" id="qty" name="qty" value="1" min="1" max="{{$product->reorder_level - 20}}">
							<br>
							<input type="submit" name="add_to_cart" class="btn btn-danger btn-block" value="Add to Cart" />
						</form>
					</div>    

			    </div>

			    <div class="row">
			    	<div class="col">
			    		{{$product->description}}
			    	</div>
			    </div>
				
			</div>
		</div><!-- end primary -->

		<div id="secondary" class="col-md-12">
		<h2>Similar Products</h2>
			<div class="row col-md-12">
				@foreach($similarProducts as $similarProduct)
					<div class="callout col-md-6 col-lg-3">
						
						<div class="col-md-12 similar">
							<a href="/shop/{{$similarProduct->id}}"><img src="{{ asset('storage') . '/images/flowers/' . $similarProduct->image}}" alt="{{ $similarProduct->image }}" />
								<div class="caption col-md-12">
									{{ $similarProduct->title }}
								</div><!-- /caption -->
							</a>
						</div>
						
					</div><!-- /callout -->
				@endforeach
			</div>
		</div><!-- Secondary div ends here -->

		@endsection('content')
