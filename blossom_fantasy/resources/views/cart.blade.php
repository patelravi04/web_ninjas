@extends('layout')

@section('style')
<style type="text/css">
/* CSS of Cart Page */
div#cart2{
	text-align: right;
}

.cart_info{
	padding: 30px;
	border-top: 2px solid #000;

}

a.sign_in{
	color: #c53337;
	font-size: 20px;
	font-weight: bold;
}

a:hover .signin{
	columns: #c53337;
	text-decoration: underline;
}

div#primary{
	margin-top: 20px;
	margin-bottom: 60px;
}

.item{	
	padding-left: 20px;
	/*margin-right: 0;*/
}

.info_div{
	margin-top: 40px;
}

/*order deatis and checkout button*/
.btn-primary{
	background-color: #EC3939;
	border: 2px solid #EC3939;
	width: 100%;
}

div.price_detail .price_list{
	border: 2px solid #c53337;
	border-radius: 20px;
	padding:20px;
}

.price{
	color: #c53337;
	font-weight: bold;
}

.order_total,.price_disc{
	border-bottom: 2px solid #cfcfcf;
}

.save{
	text-align: center;
	padding: 12px;
	color: #c53337;
}
.delivery{
	padding: 20px;
}

.remove{
	margin-left: 10px;
}

h3{
	text-align: center;
}

div.empty{
	margin-top: 40px;
    text-align: center;
}

table.cart thead tr th{
	background-color: #c53337 !important;
}

table.cart tr td img{
	max-width: 100px;
}

td.update_qty{
	min-width: 150px;
}

td.update_qty span{
	float: left;
	margin-top: 30px;
}

td.update_qty form{
	display: inline;
	margin: 0 10px;
	float: left;
}

</style>
@endsection('style')
@section('content')
<div class="row col-xs-12 cart_info">
				<div id="cart1" class="col">
					<h1>
						Your Cart(
						@if(session()->has('cart'))
							{{ count(session('cart')) }}
						@else
							0
						@endif
						)
					</h1>
				</div> <!-- cart -->
				@guest
				<div id="cart2" class="col-6">
					<p><a href="/login" class="sign_in">Sign In</a> for faster checkout.</p>
				</div> <!-- cart -->
				@endguest
			</div> <!-- row -->
			<div id="primary">
				<div class="row col-md-12">
					
					<div class="item col-md-8">
						<div class="row product_detail col-md-12">
							<div class="col">
								@include('partials.flash')
								<h3>All Item(s)</h3>
							</div>
						</div>

						@if(session()->has('cart') && count(session('cart')) > 0)
							<table class="table cart">
							  	<thead class="thead-dark">
							    	<tr>
							      		<th scope="col">Product</th>
								      	<th scope="col">Details</th>
								      	<th scope="col">Quantity</th>
							    	</tr>
							  	</thead>
							  	<tbody>

								@php
									$subtotal = 0;
									$gst = 0;
									$pst = 0;
									$total = 0;
									$totalDiscount = 0;
								@endphp
								  
								@foreach (session('cart') as $key => $singleItem)
									@php
										$id = $singleItem[key($singleItem)]['id'];
										$image = $singleItem[key($singleItem)]['image'];
										$title = $singleItem[key($singleItem)]['title'];
										$price = $singleItem[key($singleItem)]['price'];
										$discount_in_price = $singleItem[key($singleItem)]['discount_in_price'];
										$final_price = $price - $discount_in_price;
										$qty = $singleItem[key($singleItem)]['ordered_qty'];
										$ordered_price = $final_price * $qty;
									@endphp
									<tr>
							      		<td scope="row">
							      			<img src="{{ asset('storage') . '/images/flowers/thumbs/' . $image }}" class="card-img-top" alt="product_1">		
							      		</td>
							      		<td>
							      			<strong title>Title: </strong>
							      			{{ $title }}
							      			<br>
							      			<strong>Price: </strong>
							      			${{ $final_price }}
							      			@if($discount_in_price > 0)
							      			<span style="text-decoration: line-through; color: #cfcfcf;padding-left: 5px;">
							      				${{ $price }}
							      			</span>
							      			@endif
							      			<br>
							      			<strong>Total: </strong>
							      			${{ $ordered_price }}
							      		</td>
							      		<td class="update_qty">
							      			<form method="get" action="/removeFromCart/remove/{{$id}}">
												<input type="hidden" class="form-control" name="qty" value="1">
												<br>
												<input type="submit" name="remove_from_cart" class="btn btn-danger" value="-" />
											</form>
							      			<span>
							      				{{ $qty }}
							      			</span>
							      			<form method="get" action="/incrementInCart/add/{{$id}}">
												<input type="hidden" class="form-control" name="qty" value="1" />
												<br>
												<input type="submit" name="increment_in_cart" class="btn btn-danger" value="+" />
											</form>
							      		</td>
							    	</tr>
							
									@php
										$subtotal += $ordered_price;
										$totalDiscount += $discount_in_price * $qty;
									@endphp
								@endforeach

								@php
									$gst = (7 * $subtotal) / 100;
									$pst = (5 * $subtotal) / 100;
									$total = $subtotal + $gst + $pst;
								@endphp
								</tbody>
							</table>
						@else
							<div class="empty">
								<p>Your cart is empty</p>
								<a href="/shop" class="btn btn-danger">
									Continue Shopping
								</a>
							</div>
						@endif
					
					</div><!-- item -->

					@if(session()->has('cart') && count(session('cart')) > 0)
					<div class="price_detail col-md-4">
						<div class="price_list">
							<div class="row order_total col-md-12">
								<div class="col-8">
									<p>Order Subtotal</p>
									<p>Tax (GST)</p>
									<p>Tax (PST)</p>
									<p>Total</p>
								</div>
								<div class="col-4">
									<p>${{ $subtotal }}</p>
									<p>${{ $gst }}</p>
									<p>${{ $pst }}</p>
									<p>${{ $total }}</p>
								</div>
							</div>
							@if($totalDiscount > 0)
							<div class="save">
								<big>You Save ${{ $totalDiscount }} </big>
							</div>
							@endif
							<a href="/checkout" class="btn btn-danger btn-block">Checkout</a>
							<br>
							<a href="/clearCart" class="btn btn-danger btn-block">
								Clear Cart
							</a>
							@guest
							<div style="padding: 10px;">
								<p>Already a member? <a href="/login">Sign In</a></p>
							</div>
							@endguest
						</div> <!-- price_list -->
					</div> <!-- price detail -->
					@endif
				
			</div><!-- div class row-->
			</div> <!-- primary -->

@endsection('content')