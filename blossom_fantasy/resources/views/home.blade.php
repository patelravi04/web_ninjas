@extends('layout')

@section('style')

<style>
	/* Home Page Custom CSS*/

/*SLider CSS*/
img.d-block{
	opacity: 1;
}

div.carousel-caption{
	position: absolute;
	top: 200px;
}

div.carousel-caption h2{
	font-size: 36px;
	margin-bottom: 30px;
}
div.carousel-caption a{
	border: 2px solid #c53337;
	background-color: #c53337;
	color: #fff;
	padding: 8px 20px;
	font-size: 24px;
}

/*best sellers product*/

.best_sellers h2{
	margin:50px;
	text-align: center;
	color: #c53337;
}
.card-deck{
	margin-left: 12px;
	margin-right: 12px;
}

p.price{
	position: absolute;
	right:0;
	background-color: rgba(236, 57, 57, 0.6);
	padding: 8px 40px;
	font-size: 20px;
	color: #fff;
}

p.img_caption{
	background-color: rgba(25, 25, 25, 0.6);
	padding: 8px 40px;
	font-size: 20px;
	color: #fff;
	text-align: center;
}

p.card-text1{
	color: #c53337;
	border: 2px solid;
	border-radius: 5px;
	display: inline-block;
	padding: 5px 20px;
}

div.more{
	text-align: center;
}


/*Second mini slider*/
.testimonials{
	margin-top: 50px;
	margin-bottom: 50px;
	background-color: #c53337;
	color: #fff;
	text-align: center;
	padding: 30px;
}

.testimonials p{
	padding: 30px;
}

.testimonials h5{
	padding-bottom: 15px;
}

/*stats div*/
.container{
	margin-bottom: 50px;
}
.row{
	text-align: center;
	color: #c53337;
	padding-bottom: 30px;
}
</style>

@endsection('style')

@include('partials.flash')

@section('content')
<div class="bd-example">
	<!-- slider seciton -->
	  <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
	    <ol class="carousel-indicators">
	      <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
	      <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
	      <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
	    </ol>
	    <!-- first image for slider -->
	    <div class="carousel-inner">
	      <div class="carousel-item active">
	        <img src="/images/slider1.jpg" class="d-block w-100" alt="slider_image1">
	        <div class="carousel-caption d-none d-md-block">
	          <h2>Blooms Desgins For Happiness</h2>
	          <p><a href="/shop">Shop Now</a></p>
	        </div>
	      </div>
	      <!-- second image for slider -->
	      <div class="carousel-item">
	        <img src="/images/slider1.jpg" class="d-block w-100" alt="slider_image2">
	        <div class="carousel-caption d-none d-md-block">
	          <h2>Blooms Desgins For Happiness</h2>
	          <p><a href="/shop">Shop Now</a></p>
	        </div>
	      </div>
	      <!-- third image for slider -->
	      <div class="carousel-item">
	        <img src="/images/slider1.jpg" class="d-block w-100" alt="slider_image3">
	        <div class="carousel-caption d-none d-md-block">
	          <h2>Blooms Desgins For Happiness</h2>
	          <p><a href="/shop">Shop Now</a></p>
	        </div>
	      </div>
	    </div>
	    <!-- previous button link -->
	    <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
	      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
	      <span class="sr-only">Previous</span>
	    </a>
	    <!-- next button link -->
	    <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
	      <span class="carousel-control-next-icon" aria-hidden="true"></span>
	      <span class="sr-only">Next</span>
	    </a>
	  </div>
	</div>

	<!-- div for products -->
	<div class="col-xs-12 products_three">
		<div class="best_sellers"><h2>Best Sellers</h2></div>
		<div class="card-deck">
			<!-- card 1 div -->
		  <div class="card">
		  	<p class="price">$70</p>
		    <img class="card-img-top" src="/images/best1.png" alt="Card image cap" />
		    <p class="img_caption">You're Special Bouquet</p>
		    <div class="card-body">
		      <p class="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.This card has supporting text below as a natural lead-in to additional content. This is a wider card with supporting text below as a natural lead-in to additional content. </p>
		      <div class="more">
			    <p class="card-text1">More</p>
			  </div>
		    </div>
		  </div>
		  <!-- card 2 div -->
		  <div class="card">
		  	<p class="price">$70</p>
		    <img class="card-img-top" src="/images/best2.png" alt="Card image cap" />
		    <p class="img_caption">You're Special Bouquet</p>
		    <div class="card-body">
		      <p class="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.This card has supporting text below as a natural lead-in to additional content. This is a wider card with supporting text below as a natural lead-in to additional content. </p>
		      <div class="more">
			      <p class="card-text1">More</p>
			    </div>
		    </div>
		  </div>
		  <!-- card 3 div -->
		  <div class="card">
		  	<p class="price">$70</p>
		    <img class="card-img-top" src="/images/best3.png" alt="Card image cap" />
		    <p class="img_caption">You're Special Bouquet</p>
		    <div class="card-body">
		      <p class="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.This card has supporting text below as a natural lead-in to additional content. This is a wider card with supporting text below as a natural lead-in to additional content. </p>
		      <div class="more">
			      <p class="card-text1">More</p>
			    </div>
		    </div>
		  </div>
		</div>
	</div>

	<!-- small carasoul -->

	<div id="carouselExampleSlidesOnly" class="carousel slide testimonials" data-ride="carousel">
		<ol class="carousel-indicators">
	    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
	    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
	    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
	  </ol>
	  <!-- carasol div 1 -->
	  <div class="carousel-inner">
	    <div class="carousel-item active">
	      <h2>Things They Say About Us!</h2>
	      <p>It was a wonderful experience. It was my first wedding anniversary and my husband loved it. Thanks to you guys. I look forward to connect with you on all my special occasions.</p>
	      <h5>Emma Rossum</h5>
	    </div>
	    <!-- carousel div 2 -->
	    <div class="carousel-item">
	      <h2>Things They Say About Us!</h2>
	      <p>It was a wonderful experience. It was my first wedding anniversary and my husband loved it. Thanks to you guys. I look forward to connect with you on all my special occasions.</p>
	      <h5>Emma Rossum</h5>
	    </div>
	    <!-- carousel div 3 -->
	    <div class="carousel-item">
	      <h2>Things They Say About Us!</h2>
	      <p>It was a wonderful experience. It was my first wedding anniversary and my husband loved it. Thanks to you guys. I look forward to connect with you on all my special occasions.</p>
	      <h5>Emma Rossum</h5>
	    </div>
	  </div>
	</div>

	<!-- Stats div -->
	<div class="col-xs-12">
		<div class="row">
			<div class="col-6">
				<h3>2,45,654</h3>
				<p>Fresh Blossom Delivered</p>
			</div>
			<div class="col-6">
				<h3>70,456</h3>
				<p>Happy Customers and Growing More</p>
			</div>
		</div>
	</div>

@endsection('content')