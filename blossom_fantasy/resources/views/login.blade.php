@extends('layout')

@section('style')
<style type="text/css">
		/*order details at right side of page */
	div.price_list{
		border: 2px solid #c53337;
		border-radius: 20px;
		padding:20px;
	}

	div.price_detail{
		margin-bottom: 50px;
	}

	.price{
		color: #c53337;
		font-weight: bold;
	}

	.order_total,.price_disc{
		border-bottom: 2px solid #cfcfcf;
	}

	.save{
		text-align: center;
		padding: 12px;
		color: #c53337;
	}

	/*form Css*/

	div#cart1{
		padding: 50px 60px;
	}

	div.item{
		padding-left: 40px;
	}

	button.login_button{
		background-color: #EC3939;
	}

	div.item label{
		font-size: 20px;
		padding: 10px 0;
	}

	.field_imp{
		color: #c53337;
	}

	fieldset,fieldset.fieldset{
		border: 2px solid #c53337;
		padding:20px 40px ;
		margin-bottom: 50px;
		border-radius: 20px;
	}

	/* CSS for Login Page */
	.item{
		margin-right: 50px;
	}

	.not_account{
		margin-bottom: 30px;
		margin-left: 10px;
	}

	/* CSS for Register Page */
	fieldset.fieldset{
		padding-top: 30px;
		padding-bottom: 30px;
	}

	.register{
		margin-right: 50px;
		margin-left: 50px;
	}
	.btn{
		max-width: 20%;
	}
</style>
@endsection('style')
<!-- section content starts here -->
@section('content')
<div class="row col-md-12">
	<div id="cart1" class="col-md-6">
		<h1>Login Now</h1>
	</div> <!-- cart -->
	</div> <!-- row -->
	<div class="login" class="col-md-12">
		<div class="item">
			<form>
				<fieldset>
				  @csrf

				  <!-- email field -->
				  <div class="form-group row">
				    <label for="inputEmail3" class="col-sm-2 col-form-label">Email<span class="field_imp">*</span></label>
				    <div class="col-sm-12">
				      <input type="email" class="form-control" id="inputEmail3" placeholder="Email">
				    </div>
				  </div>
				  <!-- password field -->
				  <div class="form-group row">
				    <label for="inputPassword3" class="col-sm-2 col-form-label">Password<span class="field_imp">*</span></label>
				    <div class="col-sm-12">
				      <input type="password" class="form-control" id="inputPassword3" placeholder="Password">
				    </div>
				  </div>
				  <!-- login button -->
				  <div class="form-group row">
				    <div class="col-sm-6 login">
				      <button type="submit" class="btn btn-danger btn-block">Login</button>
				    </div>
				  </div>
			  </fieldset>
			</form>
			<div class="not_account">
				<p>If You do not have account, then <a href="/register" class="now">Register now<a>?</p>
			</div>	
		</div>	
	</div> <!-- primary -->

@endsection('content')