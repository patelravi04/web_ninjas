@extends('layout')
@section('style')
<style type="text/css">

    #content{
      padding: 50px 80px 50px 80px;
    }

    div#content h1{
      color: #c53337;
      text-align: center;
      margin-bottom: 50px;
    }

    .field_imp{
        color: #c53337;
    }

    fieldset,fieldset.fieldset{
        border: 2px solid #c53337;
        padding:20px 40px ;
        margin-bottom: 50px;
        border-radius: 20px;
    }

    .btn-primary{
        background-color: #EC3939;
        border: 2px solid #EC3939;
        width: 100%;
        margin-top: 20px;
    }
    .para{
        padding: 0px 20px 30px 20px;
    }
    .form_row{
        padding: 20px;
    }

    @media screen and (max-width: 440px){

        #content{
            padding: 22px;
        }
    }

</style>
@endsection('style')

@section('content')
    <h1>Contact us</h1>
    <div>
        <p class="para">
            Email us with any questions or inquiries or call at 204-510-2396. We would be happy to answer all your questions.
            We're passionate about our products as well as our customers and it shows in the level of service that we provide
            Please complete the form below, so we can provide quick and efficient service. 
            </p>
    </div>
    <!-- row div starts here -->
    <div class="row form_row" >
        <div class="col-md-12">
            @include('partials.flash')
            <form method="get" action="/sendMailFromContact">
                <fieldset class="fieldset">
                <!-- name field  -->
                <div class="form-group">
                    <label for="Name">Name<span class="field_imp">*</span></label>
                    <input type="text" class="form-control" id="name" name="name" aria-describedby="nameHelp"
                        placeholder="Name">
                    <small id="name" class="form-text text-muted">We'll never share your information with anyone
                        else.</small>
                    @if($errors->has('name'))
                        <div class="alert alert-danger" role="alert">
                            {{ $errors->first('name') }}
                        </div>
                    @endif
                </div>
                <!-- email field  -->
                <div class="form-group">
                    <label for="exampleInputEmail1">Email address<span class="field_imp">*</span></label>
                    <input type="email" class="form-control" id="exampleInputEmail1" name="email"
                        aria-describedby="emailHelp" placeholder="Enter email">
                    @if($errors->has('email'))
                        <div class="alert alert-danger" role="alert">
                            {{ $errors->first('email') }}
                        </div>
                    @endif
                </div>
                <!-- phone field -->
                <div class="form-group">
                    <label for="Phonenumber">Phone number</label>
                    <input type="text" class="form-control" id="Phonenumber" name="phone" aria-describedby="phoneHelp"
                        placeholder="Phone Number">
                    @if($errors->has('phone'))
                        <div class="alert alert-danger" role="alert">
                            {{ $errors->first('phone') }}
                        </div>
                    @endif
                </div>
                <!-- message field -->
                <div class="form-group">
                    <label for="exampleFormControlTextarea1">Message<span class="field_imp">*</span></label>
                    <textarea class="form-control" id="exampleFormControlTextarea1" name="message" rows="3"></textarea>
                    @if($errors->has('message'))
                        <div class="alert alert-danger" role="alert">
                            {{ $errors->first('message') }}
                        </div>
                    @endif
                </div>
                
                <!-- submit button -->
                <button type="submit" class="btn btn-danger btn-block">Submit</button>
                </fieldset>
            </form>
        </div>
    </div>

@endsection('content')