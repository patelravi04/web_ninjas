<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width,initial-scale=1.0" />
  <meta name="description" content="" />
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	
  <link type="text/css" rel="stylesheet" href="/styles/style.css" />
  <link rel="icon" href="/images/favicon/favicon.png" />
  <link rel="apple-touch-icon" sizes="16x16" href="/images/favicon/favicon-16x16.png">
  <link rel="apple-touch-icon" sizes="32x32" href="/images/favicon/favicon-32x32.png">  
  <link rel="apple-touch-icon" sizes="192x192" href="images/android-chrome-192x192.png">
  <link rel="apple-touch-icon" sizes="512x512" href="images/android-chrome-512x512.png">


 

  @yield('style')
  <title>{{$title ?? 'Blossom Fantasy'}}</title>
</head>
<body>
	<div class="container">
		<header class="main col-xs-12">
			<div class="image"><a href="/"><img src="/images/logo.jpg" alt="logo blosson_fantasy" /></a></div>
			<!-- utility navigation -->
			<nav class="util">
				<ul class="menu">
					@auth
						<li class="nav-item dropdown dp">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>Welcome, 
                                {{ Auth::user()->first_name }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            	<a class="dropdown-item" href="/profile" title="{{ $slug ?? '' }}">Profile</a>
                                <a class="dropdown-item" href="{{ route('logout') }}" 
                                   onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();" title="{{ $slug ?? '' }}">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
					@else
						<li class="log"><a href="/login" title="{{ $slug ?? '' }}">Login</a></li>
						<li class="log"><a href="/register" title="{{ $slug ?? '' }}">Register</a></li>
					@endauth
					<li class="cart">
						<a href="/cart">Cart <img src="/images/cart.png" alt="cart_image" /></a>
						<span>
							@if(session()->has('cart'))
								{{ count(session('cart')) }}
							@else
								0
							@endif
						</span>
					</li>
				</ul>
		  </nav><!-- utility nav ends here -->
		  <!-- Main Navigation -->
		  <nav class="navbar navbar-expand-lg navbar-light bg-light">
		  	  	<div class="div-toggle">
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					    <span class="navbar-toggler-icon"></span>
					</button>
				</div>

			  	<div class="collapse navbar-collapse" id="navbarSupportedContent">
			    	<ul class="navbar-nav mr-auto">
			      		<li class="nav-item @isset($slug) @if($slug=='home') active @endif @endisset">
				        	<a class="nav-link" href="/" title="{{ $slug ?? '' }}">Home <span class="sr-only">(current)</span></a>
				      	</li>
				      	<li class="nav-item @isset($slug) @if($slug=='about') active @endif @endisset">
				        	<a class="nav-link" href="/about" title="{{ $slug ?? '' }}">About</a>
				      	</li>
				      	<li class="nav-item @isset($slug) @if($slug=='shop') active @endif @endisset">
				        	<a class="nav-link" href="/shop" title="{{ $slug ?? '' }}">Shop</a>
				      	</li>
				      	<li class="nav-item @isset($slug) @if($slug=='faq') active @endif @endisset">
				        	<a class="nav-link" href="/faq" title="{{ $slug ?? '' }}">Faq</a>
				      	</li>
				      	<li class="nav-item @isset($slug) @if($slug=='contact') active @endif @endisset">
				        	<a class="nav-link" href="/contact" title="{{ $slug ?? '' }}">Contact</a>
				      	</li>
			    	</ul>
				    <form class="form-inline my-2 my-lg-0">
				      	<input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
				      	<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
				    </form>
			  	</div>
			</nav>
		  <!-- main nav ends here -->
		</header><!-- header ends here -->

		<!-- main Content -->
		<div id="content" class="col-xs-12" style="overflow: hidden;">
			<!-- slider div -->
			@yield('content')

		</div><!-- content div ends here -->

		<footer class="main col-xs-12">
			<!-- footer navigation -->
			<nav class="footer1">
				<ul class="menu">
					<li class="nav-item-footer @isset($slug) @if($slug=='home') active-footer-nav @endif @endisset"><a href="/" title="{{ $slug ?? '' }}">Home</a></li>
					<li class="nav-item @isset($slug) @if($slug=='about') active-footer-nav @endif @endisset" ><a href="/about" title="{{ $slug ?? '' }}">About</a></li>
					<li class="nav-item-footer @isset($slug) @if($slug=='shop') active-footer-nav @endif @endisset"><a href="/shop" title="{{ $slug ?? '' }}" >Shop</a></li>
					<li class="nav-item-footer @isset($slug) @if($slug=='faq') active-footer-nav @endif @endisset"><a href="/faq" title="{{ $slug ?? '' }}">Faq</a></li>		
					<li class="nav-item @isset($slug) @if($slug=='contact') active-footer-nav @endif @endisset"><a href="/contact" title="{{ $slug ?? '' }}">Contact</a></li>
				</ul>
		  </nav><!-- footer1 nav ends here -->
		  <nav class="footer2">
		  	<ul  class="menu">
		  		<li><a href="https://www.facebook.com/"><img src="/images/facebook.png" alt="facebook" /></a></li>
		  		<li><a href="https://www.instagram.com/"><img src="/images/instagram.png" alt="instagram" /></a></li>
		  		<li><a href="https://twitter.com/?lang=en"><img src="/images/twitter.png" alt="twitter" /></a></li>
		  		<li><a href="https://www.youtube.com/"><img src="/images/youtube.png" alt="youtube" /></a></li>
		  		<li><a href="https://in.pinterest.com/"><img src="/images/pinterest.png" alt="pinterest" /></a></li>
		  	</ul>
		  </nav><!-- footer2 nav ends here -->
		  <nav class="footer3">
		  	<ul  class="menu">
					<li class="nav-item-footer @isset($slug) @if($slug=='terms') active-footer-nav @endif @endisset"><a href="/terms" title="{{ $slug ?? '' }}">Terms & Conditions</a></li>
					<li class="nav-item-footer @isset($slug) @if($slug=='privacy') active-footer-nav @endif @endisset"><a href="/privacy" title="{{ $slug ?? '' }}">Privacy Policy</a></li>
					<li class="nav-item-footer @isset($slug) @if($slug=='') active-footer-nav @endif @endisset"><a href="#" title="{{ $slug ?? '' }}" class="copy">&copy; 2019 by Blossom Fantasy</a></li>
				</ul>
		  </nav><!-- footer3 nav ends here -->
		</footer><!-- footer ends here -->

	</div><!-- div container --> 
</body>
</html>