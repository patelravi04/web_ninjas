@extends('layout')

@section('content')
<style>
.labels {
    /* position: absolute; */
    right: 0;
    background-color: rgba(236, 57, 57, 0.6);
    padding: 8px 40px;
    font-size: 20px;
    color: #fff;
}


p.card-text1 {
    color: #c53337;
    border: 2px solid;
    border-radius: 5px;
    display: inline-block;
    padding: 5px 20px;
}

.profile{
    padding: 30px;
}

h1{
    text-align: center;
    padding: 10px;
}

a {
    text-decoration: none;
    color: #ffffff;
}

</style>

<div class="profile">
                <h1>Profile</h1>
            
            <!-- <div>
                <p style="padding: 0px 20px 30px 20px;">
                    Donec et odio pellentesque diam volutpat commodo sed. Praesent
                    tristique magna sit amet purus gravida quis blandit turpis. </p>
            </div> -->

            <div>
                <img src="/images/flowers/default200.png" alt="logo blosson_fantasy" class="d-block mx-auto rounded-circle img-fluid" />
            </div>
            
            <div style="text-align: center; margin: 40px 5px 80px 5px;">
                <h2>
               Name: {{Auth::user()->first_name}} {{Auth::user()->last_name}}
                </h2>
                <h3>
               Email: {{Auth::user()->email}}
                </h3>

            </div>
            
           <!-- Stats div -->
			<div class="col-xs-12">
				<div class="row" style="text-align:center;">
					<div class="col-md-4">
						<h4 class="labels">Subscriptions</h4>
						<p>5</p>
					</div>
					<div class="col-md-4">
						<h4 class="labels"><a href="/orders">view order history</a></h4>
				    </div>
                    <div class="col-md-4">
						<h4 class="labels">Most recent order</h4>
						<p>Thanksgiving Flowers</p>
					</div>
				</div>
			</div>

            <div style="text-align: center; margin: 80px 5px 80px 5px;">
                <h5>
                Contact Number
                </h5>
                <h6>
                {{Auth::user()->phone}}
                </h6>

            </div>

            <div class="row" style="padding: 20px;">
                <div class="col-md-6" style="margin: 0 auto;">
                    <h6>Address</h6>
                    <span>123456 Patel Drive</span><br/>
                    <span>Winnipeg, Manitoba R2D 3C0</span>
                    <p>Canada</p>
                    <!--
                    <form>
                        <div class="form-group">
                            <label for="Name">Name</label>
                            <input type="text" class="form-control" id="name" aria-describedby="nameHelp"
                                placeholder="Name">
                            <small id="name" class="form-text text-muted">We'll never share your information with anyone
                                else.</small>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Email address</label>
                            <input type="email" class="form-control" id="exampleInputEmail1"
                                aria-describedby="emailHelp" placeholder="Enter email">

                        </div>
                        <div class="form-group">
                            <label for="Phonenumber">Phone number</label>
                            <input type="text" class="form-control" id="Phonenumber" aria-describedby="phoneHelp"
                                placeholder="Phone Number">
                        </div>

                        <div class="form-group">
                            <label for="exampleFormControlTextarea1">Message</label>
                            <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                        </div>
                        <div class="form-group form-check">
                            <input type="checkbox" class="form-check-input" id="exampleCheck1">
                            <label class="form-check-label" for="exampleCheck1">Check to subscribe to our mailing
                                list</label>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                    -->

                </div>


            </div>
            </div>

@endsection('content')