 @extends('layout')

@section('style')
<!-- css for about us page -->
<style type="text/css">
  
    .content1{
      padding: 50px 80px 50px 80px;
    }

    div.content1 h1,h2{
      color: #c53337;
      text-align: center;
      margin-bottom: 50px;
    }

    div.hero_image img{
      width: 100%;
      height: auto;
    }

    .card{
      border: none;
    }

  @media screen and (max-width: 440px){
   div.content1{
      padding: 20px;
   }   
  }    


</style>

@endsection('style')
@section('content')
  <div class="hero_image">
    <img src="/images/about.jpg" alt="logo blosson_fantasy" />
  </div>
  <div class="content1">    
    <div>
      <h1>
        We want to make your day happy and interesting.
      </h1>
      <div class="card mb-3 col-md-12" >
        <div class="row no-gutters">
          <div class="col-md-4">
            <img src="/images/baguioph.jpg" class="card-img" alt="about_blosson_fantasy">
          </div>

          <div class="col-md-8">
            <div class="card-body">
              <p class="card-text">When we see flowers we are reminded that this world is
              still beautiful inspite of all the challenges we encounter everyday. It is
            still worth sharing happiness to other people. It is still a magical and
            colorful world where things can be wonderful and interesting.</p>
            </div>
          </div>

        </div>
      </div>
      <!-- heading div -->
      <div style="margin-top: 50px;">
        <h2>
          Our commitment to you and the Community.
        </h2>
      </div>
      <!-- crd images with caption -->
      <div class="card-deck">
        <!-- image 1 div -->
        <div class="card">
          <img src="/images/clark350_216.jpg" class="rounded card-img-top" alt="pic1">
          <div class="card-body">
            <h5 class="card-title">Create moments</h5>            
          </div>
        </div>
        <!-- image 2 div -->
        <div class="card">
          <img src="/images/sunflowerjpg390_216.jpg" class="rounded card-img-top" alt="pic2">
          <div class="card-body">
            <h5 class="card-title">Help</h5>
          </div>
        </div>
        <!-- image 3 div -->
        <div class="card">
          <img src="/images/baguio350_216.jpg" class="rounded card-img-top" alt="pic3">
          <div class="card-body">
            <h5 class="card-title">Give back</h5>
          </div>
        </div>
      </div>
      <!-- heading div -->
      <div style="margin-top: 50px;">
        <h1>Timeline</h1>

        <div class="card mb-3 col-md-12" >
        <div class="row no-gutters">

        @php
          $counter = 0;
        @endphp

        @foreach($abouts as $about)
            @php
                $counter++;
            @endphp

            @if($counter%2 == 1)
            <div class="col-md-8">
              <div class="card-body " style="padding-top: 90px;">
                <p class="card-text">{{ $about->timeline }}</p>
              </div>
            </div>

            <div class="col-md-4" style="margin-top: 30px;">
                @if(!empty($about->image))
                <img 
                  src="{{ asset('storage') . '/images/about/' . $about->image }}"
                  class="rounded img-fluid" 
                  alt="{{ $about->image }}">
                @endif
            </div>
            @else
            <div class="col-md-4" style="margin-top: 30px;">
                @if(!empty($about->image))
                <img 
                  src="{{ asset('storage') . '/images/about/' . $about->image }}"
                  class="rounded img-fluid" 
                  alt="{{ $about->image }}">
                @endif
            </div>

            <div class="col-md-8" style="padding-top: 50px;">
              <div class="card-body" >
                <p class="card-text">{{ $about->timeline }}</p>
              </div>
            </div>
            @endif
            
          @endforeach  
        </div>
      </div>
    
     

     
    </div>
  </div><!-- content div ends here -->
@endsection('content')