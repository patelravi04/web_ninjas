 @extends('layout')

@section('style')
<!-- css for about us page -->
<style type="text/css">
.more-items h5{
	border-bottom: 2px solid #cfcfcf;
	padding-bottom: 10px;
}

#content{
	padding: 50px 50px 50px 80px;
}

div#content h1{
	color: #c53337;
	text-align: center;
	margin-bottom: 50px;
	text-shadow: 2px 2px 3px #999;
}

div.thank_message h3,p{
	text-align: center;
	color: #999;
}

div.thank_message p{
	text-align: center;
	color: #9c9c9c;
	margin-bottom: 60px;
}

/*more products*/
.products_three{
	margin-top: 50px;
}
.card-deck{
	margin-left: 12px;
	margin-right: 12px;
}

p.price{
	position: absolute;
	right:0;
	background-color: rgba(236, 57, 57, 0.6);
	padding: 4px 20px;
	font-size: 12px;
	color: #fff;
}

p.img_caption{
	background-color: rgba(25, 25, 25, 0.6);
	padding: 8px 20px;
	font-size: 16px;
	color: #fff;
	text-align: center;
	margin-bottom: 0
}

p.card-text1{
	color: #c53337;
	border-radius: 5px;
	display: inline-block;
	padding: 5px 10px;
	margin-bottom: 0;
}

div.more{
	text-align: center;
	font-size:16px;
	font-weight: bold;
	border: 2px solid #c53337;
}

div.continue{
	text-align: center;
}

header,
footer{
	display: none !important;
}

</style>
@endsection('style')
<!-- section content ends here -->
@section('content')
	<h1>Thank You for your purchase!</h1>
	<div class="thank_message">
		<h3>
			Your order reference number: <strong>{{$order_reference}}</strong><br>
			Your transaction reference number: <strong>{{$transaction_reference}}</strong>
			<br>
			Thanks For Being awesome<br>
			we hope you enjoy your purchase
		</h3>
		<br>
		<p>if you have any questions and concerms regarding this orders, Please feel free to reach at blossom_fantasy@flowers.com and 1800-452-9889</p>
	</div>
	<!-- thank_message ends here -->
@endsection('content')