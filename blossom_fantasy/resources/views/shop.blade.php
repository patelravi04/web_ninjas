@extends('layout')

@section('style')
<style>
	#categories{
		min-height: 142px;
		background: #EC3939;
		width: 100%;
		margin: 0 auto;
	}

	.row{
		margin-left: 10px;
		margin-bottom: 20px;
	}

	div #categories ul{
		padding: 0;
		margin: 0;
	}
	img.img-fluid.radius{
		border-radius: 8px;
	}

	div #categories ul li{
		padding: 0 20px;
		margin: 0;
		list-style-type: none;
		display: inline-block;
	}

	div #categories ul li a{
		color: #fff;
		text-decoration: none;
		border: 2px solid #3E3E3E;
		background-color: #3E3E3E;
		padding: 12px 15px;
		line-height: 75px;
		width: 100%;
		margin: 0 auto;
		border-radius: 8px;
	}

	div #categories ul li a:hover{
		text-decoration: underline;
	}

	div #categories ul li a.category-active{
		background-color: #EFEFEF;
	    color: #1D1D1D;
	    text-decoration: underline;
	}

	div #pages ul{
		padding: 0;
		margin: 0;
	}

	div #pages ul li{
		padding: 0;
		margin: 0;
		list-style-type: none;
		display: inline-block;
	}

	div #pages ul li a{
		
		
	}
	#primary {
		padding: 15px;
		background-color: #FFF;
		margin-top: 30px;
	}

	#secondary{
		padding-top: 10px;
	}

	#secondary #pages nav{
		margin: 0 auto;
	}

	a.btn-primary{
		background-color: #C53337;
		border: 2px solid #C53337;
		width: 100%;
	}

	a.btn-primary:hover,
	a.btn-primary:active{
		background-color: #FFF !important;
		border: 2px solid #C53337 !important;
		color: #C53337 !important;
	}

	h2.price{
		color: #C53337;
		text-align: center;
		font-size: 1.5em;
	}

	.card-body{
		background-color: rgba(196,196,196,.5);
	}

	p.card-text{
		text-align: justify;
	}

	h1.head{
		text-align: center;
		background-color: rgba(196,196,196,.5);
		margin-bottom: 0;
		padding: 10px 0 15px;
		color:#1D1D1D;
	}

	div.product-image{
		width: 100%;
		min-height: 300px;
	    max-height: 300px;
	    background-color: #EFEFEF;
	    overflow: hidden;
	    text-align: center;
	}

	.page-item.active .page-link {
	    background-color: #C53337;
	    border-color: #C53337;
	}

	.page-link,
	.page-link:hover {
	    color: #C53337;
	}

</style>
@endsection('style')
@section('content')
<div id="categories" class="col-md-12">
	<ul class="text-center">
		<li>
			<a href="/shop/category/all" 
			class="@if('all' == $categorySlug) category-active @endif">
				All Categories
			</a>
		</li>
		@foreach($categories as $category)
		<li>
			<a href="/shop/category/{{ $category->id }}" 
			class="@if($category->id == $categorySlug) category-active @endif">
				{{ $category->name }}
			</a>
		</li>
		@endforeach
	</ul>
</div> <!-- categories -->

<div id="primary">
	<div class="row row-cols-1 row-cols-md-2">
		@foreach($products as $product)
		<div class="col-md-6 mb-4">
		    <div class="card">
		      <h1 class="head">{{ $product->title }}</h1>
		      <div class="product-image">
		      	<img src="{{ asset('storage') . '/images/flowers/' . $product->image}}" class="card-img-top" alt="card_image">
		      </div>
		      <div class="card-body">
		        <h2 class="card-title price">$ {{ $product->price }}</h2>
		        <p class="card-text">{!! Str::limit($product->description, 200) !!}</p>
				<a href="/shop/{{$product->id}}" class="btn btn-primary">More Details</a>
		      </div>
		    </div>
		</div>
		@endforeach
	</div><!-- div row row-cols-1 ends here -->

</div> <!-- primary -->

<div id="secondary">
    <div id="pages" class="row">
		{{ $products->links() }}
	</div><!-- div row-->
</div><!-- end secondary -->
@endsection('content')