@extends('layout')

@section('style')
<style>
.table-image {
  td, th {
    vertical-align: middle;
  }
}

.product_image{
	height: 110px;
	width: 150px;
}

.new{
	padding: 20px;
}

</style>

@endsection('style')

@include('partials.flash')

@section('content')
<div class="table-responsive-xl new">
<h1 style="text-align: center; padding: 20px;">My Orders</h1>
<table class="table table-hover">
  <thead class="thead-dark">
    <tr>
      <th scope="col">Date</th>    	
      <th scope="col">Product</th>
      <th scope="col">Product Title</th>
      <th scope="col">Reference Number</th>
      <th scope="col">Quantity</th>
      <th scope="col">Amount</th>
      <th scope="col">Status</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">December 6, 2019</th>
      <td><img class="img-fluid img-thumbnail product_image" src="/images/shop_2.png" alt="product_image"></td>
      <td>The Macchiato</td>
      <td>B11</td>
      <td>2</td>
      <td>$80.00</td>
      <td>Ordered</td>
    </tr>

    <tr>
      <th scope="row">December 6, 2019</th>
      <td><img class="img-fluid img-thumbnail product_image" src="/images/shop_2.png" alt="product_image"></td>
      <td>The Macchiato</td>
      <td>B11</td>
      <td>2</td>
      <td>$80.00</td>
      <td>Ordered</td>
    </tr>    
    <tr>
      <th scope="row">December 6, 2019</th>
      <td><img class="img-fluid img-thumbnail product_image" src="/images/shop_2.png" alt="product_image"></td>
      <td>The Macchiato</td>
      <td>B11</td>
      <td>2</td>
      <td>$80.00</td>
      <td>Ordered</td>
    </tr>
    <tr>
      <th scope="row">December 6, 2019</th>
      <td><img class="img-fluid img-thumbnail product_image" src="/images/shop_2.png" alt="product_image"></td>
      <td>The Macchiato</td>
      <td>B11</td>
      <td>2</td>
      <td>$80.00</td>
      <td>Ordered</td>
    </tr>
    <tr>
      <th scope="row">December 6, 2019</th>
      <td><img class="img-fluid img-thumbnail product_image" src="/images/shop_2.png" alt="product_image"></td>
      <td>The Macchiato</td>
      <td>B11</td>
      <td>2</td>
      <td>$80.00</td>
      <td>Ordered</td>
    </tr>            
  </tbody>
</table>
</div>

@endsection('content')