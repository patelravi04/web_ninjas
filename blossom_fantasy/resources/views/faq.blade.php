@extends('layout')

@section('style')
<style type="text/css">

#content{
  padding: 50px 80px 50px 80px;
}

div#content h1{
  color: #c53337;
  text-align: center;
  margin-bottom: 50px;
}

.btn-link{
  color: #fff;
}

.btn-link:hover{
  color: #fff;
}

.card-header{  
  background: #c53337;
}

.card-header,.card-body{
  border: 1px solid #cfcfcf;
}
@media screen and (max-width: 440px){
 #content{
    padding: 20px;
 }   
}

</style>
@endsection('style')
@section('content')
  <h1>FAQ</h1>
  <div class="bs-example">
    <div class="panel-group" id="accordion">
    @foreach($faqs as $index => $faq)
        <div class="panel panel-default">
            <div class="panel-heading card-header">
                <h4 class="panel-title">
                    <a data-toggle="collapse" class="btn btn-link" data-parent="#accordion" href="#collapse-{{ $index }}">{{ $faq->questions }}</a>
                </h4>
            </div><!-- card-hader dic ends here -->
            <div id="collapse-{{ $index }}" class="panel-collapse collapse in">
                <div class="card-body">
                    <p>{{ $faq->answers}}</p>
                </div>
            </div><!-- panel-collapse dic ends here -->
        </div><!-- panel div ends here -->
    @endforeach
    </div><!-- panel-group div -->
  </div><!-- bs-example div ends here -->

@endsection('content')