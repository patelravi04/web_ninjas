@extends('admin.layout')

@section('content')
		<h1 class="dash-title">{{ $title }}</h1>

        <div class="row">
            <div class="col">
                <div class="card spur-card">
                    <div class="card-header bg-secondary text-white">
                        <div class="spur-card-icon">
                            <i class="fas fa-chart-bar"></i>
                        </div>
                        <div class="spur-card-title"> Product Form </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="/admin/products"
                            enctype="multipart/form-data" id="product" novalidate>
                            @csrf

                            <div class="form-group">
                                <label for="title">Title</label>
                                <input type="text" class="form-control" 
                                    id="title" name="title" placeholder="Product Title" 
                                    value="{{ old('title') }}">
                                @if($errors->has('title'))
                                    <div class="alert alert-danger" role="alert">
                                        {{ $errors->first('title') }}
                                    </div>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea class="form-control" id="description" 
                                    name="description" 
                                    rows="3">{!! old('description') !!}</textarea>
                                @if($errors->has('description'))
                                    <div class="alert alert-danger" role="alert">
                                        {{ $errors->first('description') }}
                                    </div>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="title">Price</label>
                                <input type="text" class="form-control" 
                                    id="price" name="price" placeholder="Product Price" 
                                    value="{{ old('price') }}">
                                @if($errors->has('price'))
                                    <div class="alert alert-danger" role="alert">
                                        {{ $errors->first('price') }}
                                    </div>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="title">Quantity</label>
                                <input type="text" class="form-control" 
                                    id="quantity" name="quantity" placeholder="Product Quantity" 
                                    value="{{ old('quantity') }}">
                                @if($errors->has('quantity'))
                                    <div class="alert alert-danger" role="alert">
                                        {{ $errors->first('quantity') }}
                                    </div>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="title">Maximum Days For Delivery</label>
                                <input type="text" class="form-control" 
                                    id="max_days_for_delivery" name="max_days_for_delivery" 
                                    placeholder="Max Days For Delivery" 
                                    value="{{ old('max_days_for_delivery') }}">
                                @if($errors->has('max_days_for_delivery'))
                                    <div class="alert alert-danger" role="alert">
                                        {{ $errors->first('max_days_for_delivery') }}
                                    </div>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="title">Reorder Level</label>
                                <input type="text" class="form-control" 
                                    id="reorder_level" name="reorder_level" placeholder="Reorder Level" 
                                    value="{{ old('reorder_level') }}">
                                @if($errors->has('reorder_level'))
                                    <div class="alert alert-danger" role="alert">
                                        {{ $errors->first('reorder_level') }}
                                    </div>
                                @endif
                            </div>

                            <div class="form-group">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" 
                                    id="discount_applicable" name="discount_applicable" 
                                    @if(old('discount_applicable'))
                                        checked
                                    @endif>
                                    <label class="custom-control-label" for="discount_applicable">
                                        Discount Applicable?
                                    </label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="title">Discount in price</label>
                                <input type="text" class="form-control" 
                                    id="discount_in_price" name="discount_in_price" placeholder="Discount in price" 
                                    value="{{ old('discount_in_price', 0) }}">
                                @if($errors->has('discount_in_price'))
                                    <div class="alert alert-danger" role="alert">
                                        {{ $errors->first('discount_in_price') }}
                                    </div>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="Categories">Categories</label>
                                @foreach($categories as $category)
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" 
                                    id="chkCat{{ $category->id }}" name="checkbox_for_categories[]" 
                                    value="{{ $category->id }}"
                                    @if(in_array($category->id, (old('checkbox_for_categories') ?? [])))
                                        checked
                                    @endif
                                    >
                                    <label class="custom-control-label" for="chkCat{{ $category->id }}">
                                        {{ $category->name }}
                                    </label>
                                    <br>
                                    
                                </div>
                                @endforeach
                                @if($errors->has('checkbox_for_categories'))
                                    <div class="alert alert-danger" role="alert">
                                        {{ $errors->first('checkbox_for_categories') }}
                                    </div>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="title">Image</label>
                                <input type="file" class="form-control" 
                                    id="image" name="image" placeholder="Product Image" 
                                    value="{{ old('image') }}">
                                @if($errors->has('image'))
                                    <div class="alert alert-danger" role="alert">
                                        {{ $errors->first('image') }}
                                    </div>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="title">Image 2</label>
                                <input type="file" class="form-control" 
                                    id="image2" name="image2" placeholder="Product Image 2" 
                                    value="{{ old('image2') }}">
                                @if($errors->has('image2'))
                                    <div class="alert alert-danger" role="alert">
                                        {{ $errors->first('image2') }}
                                    </div>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="title">Image 3</label>
                                <input type="file" class="form-control" 
                                    id="image3" name="image3" placeholder="Product Image 3" 
                                    value="{{ old('image3') }}">
                                @if($errors->has('image3'))
                                    <div class="alert alert-danger" role="alert">
                                        {{ $errors->first('image3') }}
                                    </div>
                                @endif
                            </div>

                            <button type="submit" class="btn btn-primary">Save</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
@endsection