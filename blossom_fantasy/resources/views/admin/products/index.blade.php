@extends('admin.layout')
<!-- secition content from layout added here -->
@section('content')
		<h1 class="dash-title">{{ $title }}</h1>

        @include('partials.flash')

        <div class="row">
            <div class="col">
                <a href="/admin/products/create" class="btn btn-primary mb-1">
                    Add New Product
                </a>

                <div class="card spur-card">
                    <div class="card-header bg-secondary text-white">
                        <div class="spur-card-icon">
                            <i class="fas fa-table"></i>
                        </div>
                        <div class="spur-card-title">List of Products</div>
                    </div>
                    <div class="card-body card-body-with-dark-table">
                        <table class="table table-dark table-in-card" 
                            id="table_products">
                            <thead>
                                <tr>
                                    <th scope="col">Title</th>
                                    <th scope="col">Category</th>
                                    <th scope="col">Details</th>
                                    <th scope="col">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($products)>0)
                                    @foreach($products as $product)
                                        <tr>
                                            <td class="title">
                                                {{ $product->title }}
                                                <br>
                                                <!--<img 
                                                src="/{{ $product->image }}"
                                                class="img-fluid" 
                                                alt="{{ $product->image }}">-->
                                                @if(!empty($product->image))
                                                <img 
                                                src="{{ asset('storage') . '/images/flowers/thumbs/' . $product->image }}"
                                                class="img-fluid" 
                                                alt="{{ $product->image }}">
                                                @endif

                                                @if(!empty($product->image2))
                                                <img 
                                                src="{{ asset('storage') . '/images/flowers/thumbs/' . $product->image2 }}"
                                                class="img-fluid" 
                                                alt="{{ $product->image }}">
                                                @endif

                                                @if(!empty($product->image3))
                                                <img 
                                                src="{{ asset('storage') . '/images/flowers/thumbs/' . $product->image3 }}"
                                                class="img-fluid" 
                                                alt="{{ $product->image }}">
                                                @endif
                                            </td>
                                            <td>
                                                @foreach($product->categories as $category)
                                                    <span>{{ $category->name }}</span><br>
                                                @endforeach
                                            </td>
                                            <td>
                                                <span class="badge badge-light">
                                                    Price
                                                </span>
                                                <span class="badge badge-dark">
                                                    {{ $product->price }}</span>
                                                <br />
                                                <span class="badge badge-light">
                                                    Quantity
                                                </span>
                                                <span class="badge badge-dark">
                                                    {{ $product->quantity }}
                                                </span>
                                                <br />
                                                <span class="badge badge-light">
                                                    Max days for delivery
                                                </span>
                                                <span class="badge badge-dark">
                                                    {{ $product->max_days_for_delivery }}
                                                </span>
                                                <br />
                                                <span class="badge badge-light">
                                                    Reorder Level
                                                </span>
                                                <span class="badge badge-dark">
                                                    {{ $product->reorder_level }}
                                                </span>
                                                <br />
                                                <span class="badge badge-light">
                                                    Has Discount
                                                </span>
                                                <span class="badge badge-dark">
                                                    @if($product->has_discount)
                                                        Yes
                                                    @else
                                                        No
                                                    @endif
                                                </span>
                                                <br />
                                                <span class="badge badge-light">
                                                    Discounted Price</span>
                                                <span class="badge badge-dark">
                                                    {{ $product->discount_in_price }}
                                                </span>
                                            </td>
                                            <td>
                                                <a href="/admin/products/{{$product->id}}" class="btn btn-primary btn-sm mb-1">
                                                    Edit
                                                </a>
                                                <form class="form d-inline form-inline" 
                                                action="/admin/products/{{$product->id}}" 
                                                method="post">
                                                    @csrf 
                                                    @method('DELETE')
                                                    <button class="btn btn-danger btn-sm mb-1">Delete</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="3">There is no product available</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>

                        <div class="pagination">
                            {{ $products->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection