@extends('admin.layout')

@section('content')
	<h1 class="dash-title">{{ $title ?? '' }}</h1>
    <div class="row">
      <div class="col">
        <div class="card spur-card">
          <div class="card-header bg-secondary text-white">
              <div class="spur-card-icon">
                  <i class="fas fa-chart-bar"></i>
              </div>
              <div class="spur-card-title"> Testimonial -- {{$testimonial->users->first_name}} </div>
          </div>
          <div class="card-body">
            <table class="table table-dark table-in-card" 
                id="table_tours">
                <!-- first name column -->
                <tr>
                  <th scope="col">First Name</th>
                  <td>{{ $testimonial->users->first_name }}</td>
                </tr>
                <!-- last naem column -->
                <tr>
                  <th scope="col">Last Name</th>
                  <td>{{ $testimonial->users->last_name }}</td>
                </tr>
                <!-- phonr column -->
                <tr>
                  <th scope="col">Phone</th>
                  <td>{{ $testimonial->users->phone }}</td>
                </tr>
                <!-- email comumn -->
                <tr>
                  <th scope="col">Email</th>
                  <td>{{ $testimonial->users->email }}</td>
                </tr>
                <!-- Role Column -->
                <tr>
                  <th scope="col">Role</th>
                  <td>{{ $testimonial->users->role }}</td>
                </tr>
                <!-- tiem column -->
                <tr>
                  <th scope="col">Created At</th>                  
                  <td>{{Carbon\Carbon::parse( $testimonial->created_at )->diffForhumans() }}</td>  
                </tr>
                <!-- description column -->
                <tr>
                  <th scope="col">Description</th> 
                  <td>{{ $testimonial->description }}</td>               
                </tr>
             </table><!-- table ends here -->
          </div>
        </div>
      </div>
    </div>
@endsection