@extends('admin.layout')
<!-- content section added -->
@section('content')
<!-- header for the testimonials page -->
	<h1 class="dash-title">{{ $title }}</h1>
  
  @include('partials.flash')

    <div class="row">
      <div class="col">
        <div class="card spur-card">
          <div class="card-header bg-secondary text-white">
            <div class="spur-card-icon">
              <i class="fas fa-table"></i>
            </div><!-- div spur-card-icon ends here -->
            <div class="spur-card-title">List of Testimonials</div>
          </div><!-- div card-header ends here -->
          <div class="card-body card-body-with-dark-table">
            <table class="table table-dark table-in-card" 
                id="table_tours">
              <thead>
                <tr>
                  <th scope="col">Name</th>
                  <th scope="col">Description</th>
                  <th scope="col">Created at</th>
                  <th scope="col">Action</th>
                </tr>
              </thead>
              <tbody>
                <!-- if else condition to check testimonials greater then zero -->
                @if(count($testimonials)>0)
                <!-- foreach for testimonials  -->
                  @foreach($testimonials as $testimonial)
                    <tr>
                      <td class="title">{{ $testimonial->users->first_name }} {{ $testimonial->users->last_name }}</td>
                      <td>{{ $testimonial->description }}</td>
                      <td>{{ $testimonial->created_at->todatestring() }}</td>
                      <td>
                        <a href="/admin/testimonials/{{$testimonial->id}}" class="btn btn-primary btn-sm mb-1">
                            More Details
                        </a>
                      </td>
                    </tr>
                  @endforeach
                  <!-- otherwise default message if no testimonials is not there -->
                @else
                  <tr>
                      <td colspan="3">There is no testimonial available</td>
                  </tr>
                @endif
              </tbody>
            </table><!-- table ends here -->
          </div><!-- div card-body ends here -->
        </div><!-- div card ends here -->
      </div><!-- div col ends here -->
    </div><!-- div row ends here -->
@endsection