@extends('admin.layout')

@section('content')
		<h1 class="dash-title">{{ $title }}</h1>

        <div class="row">
            <div class="col">
                <div class="card spur-card">
                    <div class="card-header bg-secondary text-white">
                        <div class="spur-card-icon">
                            <i class="fas fa-chart-bar"></i>
                        </div>
                        <div class="spur-card-title"> Add Users Form </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="/admin/users" id="user" novalidate>
                            @csrf

                            <div class="form-group">
                                <label for="title">First Name</label>
                                <input type="text" class="form-control" 
                                    id="name" name="first_name" placeholder="First Name" 
                                    value="{{ old('first_name') }}">
                                @if($errors->has('first_name'))
                                    <div class="alert alert-danger" role="alert">
                                        {{ $errors->first('first_name') }}
                                    </div>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="title">Last Name</label>
                                <input type="text" class="form-control" 
                                    id="name" name="last_name" placeholder="Last Name" 
                                    value="{{ old('last_name') }}">
                                @if($errors->has('last_name'))
                                    <div class="alert alert-danger" role="alert">
                                        {{ $errors->first('last_name') }}
                                    </div>
                                @endif
                            </div> 


                            <div class="form-group">
                                <label for="phone">Phone</label>
                                <input type="text" class="form-control" 
                                    id="phone" name="phone" placeholder="Phone" 
                                    value="{{ old('phone') }}">
                                @if($errors->has('phone'))
                                    <div class="alert alert-danger" role="alert">
                                        {{ $errors->first('phone') }}
                                    </div>
                                @endif
                            </div>                             

                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="text" class="form-control" 
                                    id="email" name="email" placeholder="Email" 
                                    value="{{ old('email') }}">
                                @if($errors->has('email'))
                                    <div class="alert alert-danger" role="alert">
                                        {{ $errors->first('email') }}
                                    </div>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="password">Password</label>
                                <input type="password" class="form-control" 
                                    id="password" name="password" placeholder="password" 
                                    value="{{ old('password') }}">
                                @if($errors->has('password'))
                                    <div class="alert alert-danger" role="alert">
                                        {{ $errors->first('password') }}
                                    </div>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="password"> Confirm Password</label>
                                <input type="password" class="form-control" 
                                    id="password" name="confirm_password" placeholder="password" 
                                    value="{{ old('confirm_password') }}">
                                @if($errors->has('password'))
                                    <div class="alert alert-danger" role="alert">
                                        {{ $errors->first('confirm_password') }}
                                    </div>
                                @endif
                            </div>                                                                                   

                            <div class="form-group">
                                <label for="role">Role</label>
                                <select name="role" class="form-control">
                                    @foreach($roles as $role)
                                        <option value={{$role}}
                                        @if(old('role') == $role)
                                            selected 
                                        @endif
                                        >
                                            {{ ucfirst($role) }}
                                        </option>
                                    @endforeach
                                </select>
                                @if($errors->has('role'))
                                    <div class="alert alert-danger" role="alert">
                                        {{ $errors->first('role') }}
                                    </div>
                                @endif
                            </div>                            

                            <button type="submit" class="btn btn-primary">Add</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
@endsection