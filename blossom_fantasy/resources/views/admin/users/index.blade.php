@extends('admin.layout')

@section('content')
		<h1 class="dash-title">{{ $title }}</h1>
        @include('partials.flash')
        <div class="row">
            <div class="col">
                <a href="/admin/users/create" class="btn btn-primary mb-1">
                    Add New Users
                </a>

                <div class="card spur-card">
                    <div class="card-header bg-secondary text-white">
                        <div class="spur-card-icon">
                            <i class="fas fa-table"></i>
                        </div>
                        <div class="spur-card-title">List of Users</div>
                    </div>
                    <div class="card-body card-body-with-dark-table">
                        <table class="table table-dark table-in-card" 
                            id="table_tours">
                            <thead>
                                <tr>
                                    <th scope="col">User Id</th>
                                    <th scope="col">First Name</th>
                                    <th scope="col">Last Name</th>
                                    <th scope="col">Phone</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Role</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($users)>0)
                                    @foreach($users as $user)
                                        <tr>
                                            <td class="title">{{ $user->id }}</td>
                                            <td>{{ $user->first_name }}</td>
                                            <td>{{ $user->last_name }}</td>
                                            <td>{{ $user->phone }}</td>
                                            <td>{{ $user->email }}</td>              
                                            <td>{{ $user->role }}</td>
                                            <td>
                                                <a href="/admin/users/{{$user->id}}" class="btn btn-primary btn-sm mb-1">
                                                    Edit
                                                </a>
                                                <form class="form d-inline form-inline" 
                                                action="/admin/users/{{$user->id}}" 
                                                method="post">
                                                    @csrf
                                                    @method('DELETE') 
                                                    <button class="btn btn-danger btn-sm mb-1">Delete</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="3">There is no user available</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
@endsection