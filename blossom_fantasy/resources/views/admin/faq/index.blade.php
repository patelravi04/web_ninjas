@extends('admin.layout')

@section('content')
	<h1 class="dash-title">{{ $title }}</h1>

  @include('partials.flash')
  
    <div class="row">
      <div class="col">
        <!-- link for add new charges for delivery -->
        <a href="/admin/faq/create" class="btn btn-primary mb-1">
            Add New FAQ
        </a>

        <!-- Starting div for listine delivery charges varies on differnt days -->
        <div class="card spur-card">
          <div class="card-header bg-secondary text-white">
            <div class="spur-card-icon">
                <i class="fas fa-table"></i>
            </div>
            <div class="spur-card-title">List of FAQs</div>
          </div>
          <div class="card-body card-body-with-dark-table">
            <table class="table table-dark table-in-card" 
                id="table_tours">
              <thead>
                <!-- table headings -->
                <tr>
                  <th scope="col">Question</th>
                  <th scope="col">Answer</th>
                  <th scope="col">Actions</th>
                </tr>
              </thead>
              <tbody>
                <!-- if condition to check dat ain database -->
                @if(count($faqs)>0)
                <!-- loop to display delivery charges list -->
                  @foreach($faqs as $faq)
                    <!-- table contents -->
                    <tr>
                      <td class="title">{{ $faq->questions }}</td>
                      <td>{{ $faq->answers }}</td>
                      <td>
                        <!-- edit button -->
                        <a href="/admin/faq/{{$faq->id}}" class="btn btn-primary btn-sm mb-1">
                            Edit
                        </a>
                        <!-- delete button -->
                        <form class="form d-inline form-inline" 
                        action="/admin/faq/{{ $faq->id }}" 
                        method="post">
                            @csrf 
                            @method('DELETE') 
                            <button class="btn btn-danger btn-sm mb-1">Delete</button>
                        </form>
                      </td>
                    </tr>
                  @endforeach
                @else
                <!-- else case if no data in delivery charges list -->
                  <tr>
                      <td colspan="3">There is no Faq</td>
                  </tr>
                @endif
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
@endsection