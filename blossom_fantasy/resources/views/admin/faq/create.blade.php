@extends('admin.layout')
<!-- seciton added for content -->
@section('content')
	<h1 class="dash-title">{{ $title }}</h1>
    <!-- div started -->
    <div class="row">
      <div class="col">
        <div class="card spur-card">
          <div class="card-header bg-secondary text-white">
            <div class="spur-card-icon">
              <i class="fas fa-chart-bar"></i>
            </div><!-- div spur-card-icon ends here -->
            <div class="spur-card-title"> FAQ Form </div>
          </div><!-- div card-header  ends here -->
          <div class="card-body">
            <form method="post" action="/admin/faq" id="faq" novalidate>
              @csrf
              <!-- form field for charges for delivery -->
              <div class="form-group">
                <label for="questions">Question</label>
                <input type="text" class="form-control" 
                  id="questions" name="questions" placeholder="Quesiton" 
                  value="{{old('questions')}}">
                @if($errors->has('questions'))
                  <div class="alert alert-danger" role="alert">
                    {{ $errors->first('questions') }}
                  </div>
                @endif
              </div><!-- div form-group ends here -->

              <!-- form field for days for delivery -->
              <div class="form-group">
                <label for="answers">Answer</label>
                <input type="test" class="form-control" 
                  id="answers" name="answers" placeholder="Answer" 
                  value="{{ old('answers') }}">
                @if($errors->has('answers'))
                  <div class="alert alert-danger" role="alert">
                    {{ $errors->first('answers') }}
                  </div>
                @endif
              </div><!-- div form-group ends here -->

              <button type="submit" class="btn btn-primary">Save</button>
            </form><!-- Form ends here -->
          </div><!-- div card-body ends here -->
        </div><!-- div card ends here -->
      </div><!-- div row ends here -->
    </div><!-- div row ends here -->
@endsection