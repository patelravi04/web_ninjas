@extends('admin.layout')
<!-- content added from layout -->
@section('content')
  <h1 class="dash-title">{{ $title }}</h1>
    <div class="row">
      <div class="col">
        <div class="card spur-card">
          <div class="card-header bg-secondary text-white">
            <div class="spur-card-icon">
              <i class="fas fa-chart-bar"></i>
            </div>
            <div class="spur-card-title"> Edit FAQ Form </div>
          </div>
          <div class="card-body">
            <form method="post" action="/admin/faq/{{$faq->id}}" id="faq" novalidate>
              @csrf
              @method('put')
              <!-- hidden field for edited id  -->
              <input type="hidden" name="id" value="{{ old('id', $faq->id) }}">
              <!-- form field for question -->
              <div class="form-group">
                <label for="questions">Question</label>
                <input type="text" class="form-control" 
                  id="questions" name="questions" placeholder="Question" 
                  value="{{ old('questions', $faq->questions) }}">
                @if($errors->has('questions'))
                    <div class="alert alert-danger" role="alert">
                        {{ $errors->first('questions') }}
                    </div>  
                @endif
              </div><!-- div form-group ends here -->

              <!-- form field for Answer -->
              <div class="form-group">
                <label for="answers">Answer</label>
                <input type="text" class="form-control" id="answers" name="answers" placeholder="Answer"
                    value="{{ old('answers', $faq->answers) }}" >
                @if($errors->has('answers'))
                    <div class="alert alert-danger" role="alert">
                        {{ $errors->first('answers') }}
                    </div>
                @endif
              </div><!-- div form-group ends here -->

              <button type="submit" class="btn btn-primary">Save</button>
            </form>
          </div>
        </div>
      </div>
    </div>
@endsection