@extends('admin.layout')

@section('content')
		<h1 class="dash-title">{{ $title }}</h1>
        @include('partials.flash')
        <div class="row">
            <div class="col">
                <a href="/admin/tax/create" class="btn btn-primary mb-1">
                    Add New Tax
                </a>

                <div class="card spur-card">
                    <div class="card-header bg-secondary text-white">
                        <div class="spur-card-icon">
                            <i class="fas fa-table"></i>
                        </div>
                        <div class="spur-card-title">List of Taxes</div>
                    </div>
                    <div class="card-body card-body-with-dark-table">
                        <table class="table table-dark table-in-card" 
                            id="table_tours">
                            <thead>
                                <tr>
                                    <th scope="col">Name</th>
                                    <th scope="col">Description</th>
                                    <th scope="col">Tax Percentage</th>
                                    <th scope="col">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($taxes)>0)
                                    @foreach($taxes as $tax)
                                        <tr>
                                            <td class="title">{{ $tax->name }}</td>
                                            <td>{{ $tax->description }}</td>
                                            <td>{{ $tax->percentage }}</td>
                                            <td>
                                                <a href="/admin/tax/edit/{{$tax->id}}" class="btn btn-primary btn-sm mb-1">
                                                    Edit
                                                </a>
                                                <form class="form d-inline form-inline" 
                                                action="/admin/tax/delete/{{$tax->id}}" 
                                                method="post">
                                                    @csrf 
                                                    @method('DELETE')
                                                    <button class="btn btn-danger btn-sm mb-1">Delete</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="3">There are no taxes available</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
@endsection