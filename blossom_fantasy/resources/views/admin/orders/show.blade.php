@extends('admin.layout')

@section('content')
	<h1 class="dash-title">{{ $title ?? '' }}</h1>
    <div class="row">
      <div class="col">
        <div class="card spur-card">
          <div class="card-header bg-secondary text-white">
              <div class="spur-card-icon">
                  <i class="fas fa-chart-bar"></i>
              </div>
              <div class="spur-card-title"> Order -- {{$order->user->first_name}} </div>
          </div>
          <div class="card-body">
            <table class="table table-dark table-in-card" 
                id="table_tours">
                <!-- first name column -->
                <tr>
                  <th scope="col">First Name</th>
                  <td>{{ $order->user->first_name }}</td>
                </tr>
                <!-- last naem column -->
                <tr>
                  <th scope="col">Last Name</th>
                  <td>{{ $order->user->last_name }}</td>
                </tr>
                <!-- phonr column -->
                <tr>
                  <th scope="col">Phone</th>
                  <td>{{ $order->user->phone }}</td>
                </tr>
                <!-- email comumn -->
                <tr>
                  <th scope="col">Email</th>
                  <td>{{ $order->user->email }}</td>
                </tr>
                
                <tr>
                  <th scope="col">Customer Address</th>
                  <td>{{ $order->billing_address }}</td>
                </tr> 

                <tr>
                  <th scope="col">Amount Before Tax</th>
                  <td>$ {{ $order->sub_total }}</td>
                </tr>

                <tr>
                  <th scope="col">GST</th>
                  <td>$ {{ $order->gst }}</td>
                </tr> 

                <tr>
                  <th scope="col">PST</th>
                  <td>$ {{ $order->pst }}</td>
                </tr> 

                <tr>
                  <th scope="col">Total Amount</th>
                  <td>$ {{ $order->total }}</td>
                </tr>                                                                                 
             </table><!-- table ends here -->
            </div>
          </div>
        </div>
    </div>
@endsection