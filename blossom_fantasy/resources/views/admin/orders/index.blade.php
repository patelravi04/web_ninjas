@extends('admin.layout')
<!-- content section added -->
@section('content')
<!-- header for the testimonials page -->
	<h1 class="dash-title">{{ $title }}</h1>
    <div class="row">
      <div class="col">
        <div class="card spur-card">
          <div class="card-header bg-secondary text-white">
            <div class="spur-card-icon">
              <i class="fas fa-table"></i>
            </div><!-- div spur-card-icon ends here -->
            <div class="spur-card-title">List of Orders</div>
          </div><!-- div card-header ends here -->
          <div class="card-body card-body-with-dark-table">
            <table class="table table-dark table-in-card" 
                id="table_tours">
              <thead>
                <tr>
                  <th scope="col">Order Id</th>
                  <th scope="col">User</th>
                  <th scope="col">Transaction</th>
                  <th scope="col">Order Date</th>
                  <th scope="col">Total</th>
                  <th scope="col">Status</th>
                  <th scope="col">Action</th>
                </tr>
              </thead>
              <tbody>
                <!-- if else condition to check testimonials greater then zero -->
                @if(count($orders)>0)
                <!-- foreach for testimonials  -->
                  @foreach($orders as $order)
                    <tr>
                      <td class="title">{{ $order->id }}</td>
                      <td class="title">{{$order->user_id }}</td>
                      <td>{{ $order->transaction_id }}</td>
                      <td>{{ $order->ordered_date }}</td>
                      <td>{{ $order->total }}</td>
                      <td>{{ $order->status }}</td>
                      <td>
                        <a href="/admin/orders/{{$order->id}}" class="btn btn-primary btn-sm mb-1">
                            More Details
                        </a>
                      </td>                       
                    </tr>
                  @endforeach
                  <!-- otherwise default message if no testimonials is not there -->
                @else
                  <tr>
                      <td colspan="3">There are no orders available</td>
                  </tr>
                @endif
              </tbody>
            </table><!-- table ends here -->
          </div><!-- div card-body ends here -->
        </div><!-- div card ends here -->
      </div><!-- div col ends here -->
    </div><!-- div row ends here -->
@endsection