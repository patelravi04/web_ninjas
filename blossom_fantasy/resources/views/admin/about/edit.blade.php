@extends('admin.layout')

@section('content')
		<h1 class="dash-title">{{ $title }}</h1>

        <div class="row">
            <div class="col">
                <div class="card spur-card">
                    <div class="card-header bg-secondary text-white">
                        <div class="spur-card-icon">
                            <i class="fas fa-chart-bar"></i>
                        </div>
                        <div class="spur-card-title"> Edit Timeline Form </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="/admin/about/{{$about->id}}"
                            enctype="multipart/form-data" id="product" novalidate>
                            @csrf
                            @method('put')

                            <div class="form-group">
                                <label for="timeline">Timeline</label>
                                <textarea class="form-control" id="timeline" 
                                    name="timeline" 
                                    rows="3">{!! old('timeline', $about->timeline) !!}</textarea>
                                @if($errors->has('timeline'))
                                    <div class="alert alert-danger" role="alert">
                                        {{ $errors->first('timeline') }}
                                    </div>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="image">Image</label>
                                @if(!empty($about->image))
                                <br>
                                <img 
                                    src="{{ asset('storage') . '/images/about/thumbs/' . $about->image }}"
                                    class="img-fluid thumb-edit" 
                                    alt="{{ $about->image }}">
                                @endif
                                <input type="file" class="form-control" 
                                    id="image" name="image" placeholder="Timeline Image" 
                                    value="{{ old('image', $about->image) }}">
                                @if($errors->has('image'))
                                    <div class="alert alert-danger" role="alert">
                                        {{ $errors->first('image') }}
                                    </div>
                                @endif
                            </div>

                            <button type="submit" class="btn btn-primary">Save</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
@endsection