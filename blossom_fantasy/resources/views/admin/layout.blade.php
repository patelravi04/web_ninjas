<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" 
                content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" 
                href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" 
                integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Nunito:400,600%7cOpen+Sans:400,600,700" 
        rel="stylesheet">
        <link rel="stylesheet" href="/css/spur.css">
        <link rel="stylesheet" href="/css/admin.css">
        <link rel="icon" href="/images/favicon/favicon.png" />
        <link rel="apple-touch-icon" sizes="16x16" href="/images/favicon/favicon-16x16.png">
        <link rel="apple-touch-icon" sizes="32x32" href="/images/favicon/favicon-32x32.png">  
        <link rel="apple-touch-icon" sizes="192x192" href="images/android-chrome-192x192.png">
        <link rel="apple-touch-icon" sizes="512x512" href="images/android-chrome-512x512.png">        

        <title>{{$subtitle ?? ''}}</title>
    </head>

    <body>
        <div class="dash">
            <div class="dash-nav dash-nav-dark">
                <header>
                    <a href="#!" class="menu-toggle">
                        <i class="fas fa-bars"></i>
                    </a>
                    <a href="/admin" class="spur-logo">
                        <i class="fas fa-globe"></i> <span>BF Admin</span>
                    </a>
                </header>
                
                <nav class="dash-nav-list">
                
                    <a href="/admin" class="dash-nav-item">
                        <i class="fas fa-home"></i> Dashboard
                    </a>
                    <a href="/admin/categories" class="dash-nav-item">
                        <i class="fas fa-layer-group"></i> Categories
                    </a>
                    <a href="/admin/users" class="dash-nav-item">
                        <i class="fas fa-layer-group"></i> Users
                    </a>                    
                    <a href="/admin/products" class="dash-nav-item">
                        <i class="fas fa-layer-group"></i> Products
                    </a>
                    <a href="/admin/discounts" class="dash-nav-item">
                        <i class="fas fa-layer-group"></i> Discounts
                    <a href="/admin/delivery_charges" class="dash-nav-item">
                        <i class="fas fa-layer-group"></i> Delivery Charges
                    </a>
                    <a href="/admin/tax" class="dash-nav-item">
                        <i class="fas fa-layer-group"></i> Tax
                    </a>
                    <a href="/admin/faq" class="dash-nav-item">
                        <i class="fas fa-layer-group"></i> Faq
                    </a>

                    <a href="/admin/testimonials" class="dash-nav-item">
                        <i class="fas fa-layer-group"></i> Testimonials
                    </a>
                    <a href="/admin/transactions" class="dash-nav-item">
                        <i class="fas fa-layer-group"></i> Transactions
                    </a>
                    <a href="/admin/orders" class="dash-nav-item">
                        <i class="fas fa-layer-group"></i> Orders
                    </a>
                    <a href="/admin/about" class="dash-nav-item">
                        <i class="fas fa-layer-group"></i> About
                    </a>                                                     
                </nav>
                
            </div>

            <div class="dash-app">
                <header class="dash-toolbar">
                    <a href="#" class="menu-toggle">
                        <i class="fas fa-bars"></i>
                    </a>

                    <div class="row fullWidth">
                        <div class="col childLinkInline textAlignRight">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->first_name }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" >
                                    @csrf
                                </form>
                            </div>
                        </div>
                    </div>
                </header>
                <main class="dash-content">
                    <div class="container-fluid">

                        @yield('content')

                    </div> <!-- end of container-fluid -->
                </main>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" 
                integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <script src="/js/spur.js"></script>
    </body>
</html>