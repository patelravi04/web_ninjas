@extends('admin.layout')

@section('content')
		<h1 class="dash-title">{{ $title }}</h1>

        <div class="row">
            <div class="col">
                <div class="card spur-card">
                    <div class="card-header bg-secondary text-white">
                        <div class="spur-card-icon">
                            <i class="fas fa-chart-bar"></i>
                        </div>
                        <div class="spur-card-title"> Add Discounts Form </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="/admin/discounts" id="discount" novalidate>
                            @csrf

                            <div class="form-group">
                                <label for="title">Description</label>
                                <input type="text" class="form-control" 
                                    id="description" name="description" placeholder="Description" 
                                    value="{{ old('description') }}">
                                @if($errors->has('description'))
                                    <div class="alert alert-danger" role="alert">
                                        {{ $errors->first('description') }}
                                    </div>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="title">Percentage</label>
                                <input type="text" class="form-control" 
                                    id="percentage" name="percentage" placeholder="Percentage" 
                                    value="{{ old('percentage') }}">
                                @if($errors->has('percentage'))
                                    <div class="alert alert-danger" role="alert">
                                        {{ $errors->first('percentage') }}
                                    </div>
                                @endif
                            </div> 

                            <button type="submit" class="btn btn-primary">Add</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
@endsection