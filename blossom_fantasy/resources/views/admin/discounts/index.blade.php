@extends('admin.layout')

@section('content')
		<h1 class="dash-title">{{ $title }}</h1>

        @include('partials.flash')

        <div class="row">
            <div class="col">
                <a href="/admin/discounts/create" class="btn btn-primary mb-1">
                    Add New Discount
                </a>

                <div class="card spur-card">
                    <div class="card-header bg-secondary text-white">
                        <div class="spur-card-icon">
                            <i class="fas fa-table"></i>
                        </div>
                        <div class="spur-card-title">List of Discounts</div>
                    </div>
                    <div class="card-body card-body-with-dark-table">
                        <table class="table table-dark table-in-card" 
                            id="table_tours">
                            <thead>
                                <tr>
                                    <th scope="col">Discount Id</th>
                                    <th scope="col">Description</th>
                                    <th scope="col">Discount Percentage</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($discounts)>0)
                                    @foreach($discounts as $discount)
                                        <tr>
                                            <td class="title">{{ $discount->id }}</td>
                                            <td>{{ $discount->description }}</td>
                                            <td>{{ $discount->percentage }}</td>
                                            
                                            <td>
                                                <a href="/admin/discounts/{{$discount->id}}" class="btn btn-primary btn-sm mb-1">
                                                    Edit
                                                </a>
                                                <form class="form d-inline form-inline" 
                                                action="/admin/discounts/{{$discount->id}}" 
                                                method="post">
                                                    @csrf 
                                                    @method('DELETE') 
                                                    <button class="btn btn-danger btn-sm mb-1">Delete</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="3">There is no discount available</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
@endsection