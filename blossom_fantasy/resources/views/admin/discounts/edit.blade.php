@extends('admin.layout')

@section('content')
		<h1 class="dash-title">{{ $title }}</h1>

        <div class="row">
            <div class="col">
                <div class="card spur-card">
                    <div class="card-header bg-secondary text-white">
                        <div class="spur-card-icon">
                            <i class="fas fa-chart-bar"></i>
                        </div>
                        <div class="spur-card-title"> Edit Discounts Form </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="/admin/discounts/{{$discount->id}}" id="discount" novalidate>
                            @csrf
                            @method('put')
                            <input type="hidden" name="discount_id" value="{{ old('id', $discount->id) }}">
                            <div class="form-group">
                                <label for="title">Description</label>
                                <input type="text" class="form-control" 
                                    id="description" name="description" placeholder="Description" 
                                    value="{{ old('description', $discount->description) }}">
                                @if($errors->has('description'))
                                    <div class="alert alert-danger" role="alert">
                                        {{ $errors->first('description') }}
                                    </div>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="title">Discount Percentage</label>
                                <input type="text" class="form-control" 
                                    id="percentage" name="percentage" placeholder="Discount Percentage" 
                                    value="{{ old('percentage', $discount->percentage) }}">
                                @if($errors->has('percentage'))
                                    <div class="alert alert-danger" role="alert">
                                        {{ $errors->first('percentage') }}
                                    </div>
                                @endif
                            </div> 

                            <button type="submit" class="btn btn-primary">Update</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
@endsection