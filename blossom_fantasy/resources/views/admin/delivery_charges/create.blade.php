@extends('admin.layout')
<!-- seciton added for content -->
@section('content')
	<h1 class="dash-title">{{ $title }}</h1>
    <!-- div started -->
    <div class="row">
      <div class="col">
        <div class="card spur-card">
          <div class="card-header bg-secondary text-white">
            <div class="spur-card-icon">
              <i class="fas fa-chart-bar"></i>
            </div><!-- div spur-card-icon ends here -->
            <div class="spur-card-title"> Delivery Charges Form </div>
          </div><!-- div card-header  ends here -->
          <div class="card-body">
            <form method="post" action="/admin/delivery_charges" id="delivery_charges" novalidate>
              @csrf
              <!-- form field for charges for delivery -->
              <div class="form-group">
                <label for="cost">Charges for Delivery</label>
                <input type="text" class="form-control" 
                  id="cost" name="cost" placeholder="Cost" 
                  value="{{old('cost')}}">
                @if($errors->has('cost'))
                  <div class="alert alert-danger" role="alert">
                    {{ $errors->first('cost') }}
                  </div>
                @endif
              </div><!-- div form-group ends here -->

              <!-- form field for days for delivery -->
              <div class="form-group">
                <label for="days_for_delivery">Days for Delivery</label>
                <input type="number" class="form-control" 
                  id="days_for_delivery" name="days_for_delivery" placeholder="No. of Days" 
                  value="{{ old('days_for_delivery') }}">
                @if($errors->has('days_for_delivery'))
                  <div class="alert alert-danger" role="alert">
                    {{ $errors->first('days_for_delivery') }}
                  </div>
                @endif
              </div><!-- div form-group ends here -->

              <button type="submit" class="btn btn-primary">Save</button>
            </form><!-- Form ends here -->
          </div><!-- div card-body ends here -->
        </div><!-- div card ends here -->
      </div><!-- div row ends here -->
    </div><!-- div row ends here -->
@endsection