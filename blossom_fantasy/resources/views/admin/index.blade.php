@extends('admin.layout')

@section('content')
		<h1 class="dash-title">{{ $title }}</h1>

        <div class="row">
            <div class="col">

                <div class="card spur-card">
                    <div class="card-header bg-secondary text-white">
                        <div class="spur-card-icon">
                            <i class="fas fa-table"></i>
                        </div>
                        <div class="spur-card-title">Dashboard</div>
                    </div>
                    <div class="card-body card-body-with-dark-table">
                        <div class="card-body">
                            <h1>Total Records</h1> <br />
                            <table class="table table-dark table-in-card" id="table_tools">
                                <tr>
                                    <th scope="col">Categories</th> 
                                    <td>{{ $category_sum }}</td>
                                </tr>
                                <tr>
                                    <th scope="col">Users</th> 
                                    <td>{{ $user_sum }}</td>
                                </tr>
                                <tr>
                                    <th scope="col">Products</th> 
                                    <td>{{ $product_sum }}</td>
                                </tr>
                                <tr>
                                    <th scope="col">Discount</th> 
                                    <td>{{ $discount_sum }}</td>
                                </tr>
                                <tr>
                                    <th scope="col">Tax</th> 
                                    <td>{{ $tax_sum }}</td>
                                </tr>
                                <tr>
                                    <th scope="col">Testimonial</th> 
                                    <td>{{ $testimonial_sum }}</td>
                                </tr>
                                <tr>
                                    <th scope="col">Orders</th>
                                    <td>{{ $order_sum }}</td> 
                                </tr>
                                <tr>
                                    <th scope="col">Transaction</th>
                                    <td>{{ $transaction_sum }}</td>
                                </tr>
                            </table> <!-- first table ends -->
                        </div> <!-- card-body ends -->
                        <div class="card-body">
                            <h1>Aggregate of Records</h1> <br />
                            <table class="table table-dark table-in-card" id="table_tools">
                                <tr>
                                    <th scope="col">Highest Price of Product: </th>
                                    <td>$ {{ number_format($max_product, 2) }}</td>
                                </tr>
                            
                                <tr>
                                    <th scope="col">Lowest Price of Product: </th>
                                    <td>$ {{ number_format($min_product, 2) }}</td>
                                </tr>

                                <tr>
                                    <th scope="col">Average Price of Product:</th>
                                    <td>$ {{ number_format($avg_product, 2) }}</td>
                                </tr>
                                <tr>
                                    <th scope="col">Highest Amount of Order:</th>
                                    <td>$ {{ number_format($max_order, 2) }}</td>
                                </tr>
                                <tr>
                                    <th scope="col">Lowest Amount of Order:</th>
                                    <td>$ {{ number_format($min_order, 2) }}</td>
                                </tr>
                                <tr>
                                    <th scope="col">Average Amount of Order:</th>
                                    <td>$ {{ number_format($avg_order, 2) }}</td>
                                </tr>
                            </table> <!-- second table ends -->
                        </div> <!-- card-body ends -->
                    
                    </div> <!-- card-body card-body-with-dark-table -->
                </div> <!-- card spur-card -->
            </div> <!-- col -->
        </div> <!-- row -->
@endsection