@extends('admin.layout')
<!-- secition content from layout added here -->
@section('content')
		<h1 class="dash-title">{{ $title }}</h1>

        @include('partials.flash')

        <div class="row">
            <div class="col">
                <a href="/admin/categories/create" class="btn btn-primary mb-1">
                    Add New Category
                </a>

                <div class="card spur-card">
                    <div class="card-header bg-secondary text-white">
                        <div class="spur-card-icon">
                            <i class="fas fa-table"></i>
                        </div>
                        <div class="spur-card-title">List of Categories</div>
                    </div>
                    <div class="card-body card-body-with-dark-table">
                        <table class="table table-dark table-in-card" 
                            id="table_categories">
                            <thead>
                                <tr>
                                    <th scope="col">Name</th>
                                    <th scope="col">Description</th>
                                    <th scope="col">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($categories)>0)
                                    @foreach($categories as $category)
                                        <tr>
                                            <td class="title">{{ $category->name }}</td>
                                            <td>{{ $category->description }}</td>
                                            <td>
                                                <a href="/admin/categories/{{$category->id}}" class="btn btn-primary btn-sm mb-1">
                                                    Edit
                                                </a>
                                                <form class="form d-inline form-inline" 
                                                action="/admin/categories/{{$category->id}}"
                                                method="post">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button class="btn btn-danger btn-sm mb-1">Delete</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="3">There is no category available</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
@endsection