@extends('admin.layout')

@section('content')
		<h1 class="dash-title">{{ $title }}</h1>

        <div class="row">
            <div class="col">
                <div class="card spur-card">
                    <div class="card-header bg-secondary text-white">
                        <div class="spur-card-icon">
                            <i class="fas fa-chart-bar"></i>
                        </div>
                        <div class="spur-card-title"> Edit Category Form </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="/admin/categories/{{$category->id}}" id="category" novalidate>
                            @csrf
                            @method('put')

                            <input type="hidden" name="category_id" value="{{ old('id', $category->id) }}">

                            <div class="form-group">
                                <label for="title">Name</label>
                                <input type="text" class="form-control" 
                                    id="name" name="name" placeholder="Category Name" 
                                    value="{{ old('name', $category->name) }}">
                                @if($errors->has('name'))
                                    <div class="alert alert-danger" role="alert">
                                        {{ $errors->first('name') }}
                                    </div>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea class="form-control" id="description" 
                                    name="description" 
                                    rows="3">{!! old('description', $category->description) !!}</textarea>
                                @if($errors->has('description'))
                                    <div class="alert alert-danger" role="alert">
                                        {{ $errors->first('description') }}
                                    </div>
                                @endif
                            </div>

                            <button type="submit" class="btn btn-primary">Save</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
@endsection