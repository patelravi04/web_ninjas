@extends('admin.layout')

@section('content')
	<h1 class="dash-title">{{ $title ?? '' }}</h1>
    <div class="row">
      <div class="col">
        <div class="card spur-card">
          <div class="card-header bg-secondary text-white">
              <div class="spur-card-icon">
                  <i class="fas fa-chart-bar"></i>
              </div>
              <div class="spur-card-title"> Transactions -- {{$transaction->users->first_name}} </div>
          </div>
          <div class="card-body">
            <table class="table table-dark table-in-card" 
                id="table_tours">
                <!-- first name column -->
                <tr>
                  <th scope="col">First Name</th>
                  <td>{{ $transaction->users->first_name }}</td>
                </tr>
                <!-- last naem column -->
                <tr>
                  <th scope="col">Last Name</th>
                  <td>{{ $transaction->users->last_name }}</td>
                </tr>
                <!-- phonr column -->
                <tr>
                  <th scope="col">Phone</th>
                  <td>{{ $transaction->users->phone }}</td>
                </tr>
                <!-- email comumn -->
                <tr>
                  <th scope="col">Email</th>
                  <td>{{ $transaction->users->email }}</td>
                </tr>
                <!-- Date Column -->
                <tr>
                  <th scope="col">Transaction Date</th>
                  <td>{{Carbon\Carbon::parse( $transaction->created_at->todatestring() ) }}</td>
                </tr>
                <tr>
                  <th scope="col">Reference Number</th>
                  <td>{{ $transaction->reference_number }}</td>
                </tr>                
                
             </table><!-- table ends here -->
            </div>
          </div>
        </div>
    </div>
@endsection