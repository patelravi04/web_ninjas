@extends('admin.layout')
<!-- content section added -->
@section('content')
<!-- header for the testimonials page -->
	<h1 class="dash-title">{{ $title }}</h1>
    <div class="row">
      <div class="col">
        <div class="card spur-card">
          <div class="card-header bg-secondary text-white">
            <div class="spur-card-icon">
              <i class="fas fa-table"></i>
            </div><!-- div spur-card-icon ends here -->
            <div class="spur-card-title">List of Transactions</div>
          </div><!-- div card-header ends here -->
          <div class="card-body card-body-with-dark-table">
            <table class="table table-dark table-in-card" 
                id="table_tours">
              <thead>
                <tr>
                  <th scope="col">Transaction Id</th>
                  <th scope="col">User</th>
                  <th scope="col">Reference Number</th>
                  <th scope="col">Action</th>
                </tr>
              </thead>
              <tbody>
                <!-- if else condition to check testimonials greater then zero -->
                @if(count($transactions)>0)
                <!-- foreach for testimonials  -->
                  @foreach($transactions as $transaction)
                    <tr>
                      <td class="title">{{ $transaction->id }}</td>
                      <td class="title">
                          {{ $transaction->users->first_name }}
                          {{ $transaction->users->last_name }}
                      </td>
                      <td>{{ $transaction->reference_number }}</td>
                      <td>
                        <a href="/admin/transactions/{{$transaction->id}}" class="btn btn-primary btn-sm mb-1">
                            More Details
                        </a>
                      </td>                      
                    </tr>
                  @endforeach
                  <!-- otherwise default message if no testimonials is not there -->
                @else
                  <tr>
                      <td colspan="3">There are no transaction available</td>
                  </tr>
                @endif
              </tbody>
            </table><!-- table ends here -->
          </div><!-- div card-body ends here -->
        </div><!-- div card ends here -->
      </div><!-- div col ends here -->
    </div><!-- div row ends here -->
@endsection