INSERT INTO address(user_id, address_type, address, city, province, postal_code, created_by) 
			VALUES (1, 'billing', 'Portage Ave', 'Winnipeg', 'Manitoba', 'R5B 2D4', 1);

INSERT INTO address(user_id, address_type, address, city, province, postal_code, created_by) 
			VALUES (1, 'shipping', 'Portage Ave', 'Winnipeg', 'Manitoba', 'R5B 2D4', 1);

INSERT INTO address(user_id, address_type, address, city, province, postal_code, created_by) 
			VALUES (2, 'billing', 'Sherbook Ave', 'Montreal', 'Quebec', 'H3A 0G4', 2);

INSERT INTO address(user_id, address_type, address, city, province, postal_code, created_by) 
			VALUES (2, 'shipping', 'Sherbook Ave', 'Montreal', 'Quebec', 'H3A 0G4', 1);

INSERT INTO address(user_id, address_type, address, city, province, postal_code, created_by) 
			VALUES (3, 'billing', 'Jefferson Ave', 'Winnipeg', 'Manitoba', 'R5B 2D4', 1);

INSERT INTO address(user_id, address_type, address, city, province, postal_code, created_by) 
			VALUES (3, 'shipping', 'Jefferson Ave', 'Winnipeg', 'Manitoba', 'R5B 2D4', 1);

INSERT INTO address(user_id, address_type, address, city, province, postal_code, created_by) 
			VALUES (4, 'billing', 'Sheepard Street', 'Winnipeg', 'Manitoba', 'R5B 2D4', 4);

INSERT INTO address(user_id, address_type, address, city, province, postal_code, created_by) 
			VALUES (4, 'shipping', 'Sheepard Street', 'Winnipeg', 'Manitoba', 'R5B 2D4', 4);

INSERT INTO address(user_id, address_type, address, city, province, postal_code, created_by) 
			VALUES (5, 'billing', 'Martin Ave', 'Winnipeg', 'Manitoba', 'R5B 2D4', 1);

INSERT INTO address(user_id, address_type, address, city, province, postal_code, created_by) 
			VALUES (5, 'shipping', 'Martin Ave', 'Winnipeg', 'Manitoba', 'R5B 2D4', 1);

INSERT INTO address(user_id, address_type, address, city, province, postal_code, created_by) 
			VALUES (6, 'billing', 'Adsum Drive', 'Winnipeg', 'Manitoba', 'R5B 2D4', 1);

INSERT INTO address(user_id, address_type, address, city, province, postal_code, created_by) 
			VALUES (6, 'shipping', 'Adsum Drive', 'Winnipeg', 'Manitoba', 'R5B 2D4', 1);

INSERT INTO address(user_id, address_type, address, city, province, postal_code, created_by) 
			VALUES (7, 'billing', 'Queen Street', 'Toronto', 'Ontario', 'M5H 2N1', 1);

INSERT INTO address(user_id, address_type, address, city, province, postal_code, created_by) 
			VALUES (7, 'shipping', 'Queen Street', 'Toronto', 'Ontario', 'M5H 2N1', 1);

INSERT INTO address(user_id, address_type, address, city, province, postal_code, created_by) 
			VALUES (8, 'billing', 'Robson Street', 'Vancouver', 'British Columbia', 'V6B 3K9', 1);

INSERT INTO address(user_id, address_type, address, city, province, postal_code, created_by) 
			VALUES (8, 'shipping', 'Robson Street', 'Vancouver', 'British Columbia', 'V6B 3K9', 1);

INSERT INTO address(user_id, address_type, address, city, province, postal_code, created_by) 
			VALUES (9, 'billing', 'Patterosn Ave', 'Calgary', 'Alberta', 'T3H 2E1', 1);

INSERT INTO address(user_id, address_type, address, city, province, postal_code, created_by) 
			VALUES (9, 'shipping', 'Patterosn Ave', 'Calgary', 'Alberta', 'T3H 2E1', 1);

INSERT INTO address(user_id, address_type, address, city, province, postal_code, created_by) 
			VALUES (10, 'billing', 'King Ave', 'Halifax', 'Nova Scotia', 'B3J 3A5', 1);

INSERT INTO address(user_id, address_type, address, city, province, postal_code, created_by) 
			VALUES (10, 'shipping', 'King Ave', 'Halifax', 'Nova Scotia', 'B3J 3A5', 1);