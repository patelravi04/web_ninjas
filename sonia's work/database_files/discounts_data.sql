SET FOREIGN_KEY_CHECKS = 0;
INSERT INTO discounts(description, discount_percentage, created_by) 
VALUES ('Cyber Monday Sale', 30, 1);
INSERT INTO discounts(description, discount_percentage, created_by) 
VALUES ('Black Friday Sale', 20, 1);
INSERT INTO discounts(description, discount_percentage, created_by) 
VALUES ('Sunday Sale Sale', 10, 1);
INSERT INTO discounts(description, discount_percentage, created_by) 
VALUES ('Thanksgiving Day Sale', 15, 1);
INSERT INTO discounts(description, discount_percentage, created_by) 
VALUES ('Rememberance Day Sale', 30, 1);
INSERT INTO discounts(description, discount_percentage, created_by) 
VALUES ('Canada Day Sale', 25, 1);
INSERT INTO discounts(description, discount_percentage, created_by) 
VALUES ('Labour Day Sale', 30, 1);
INSERT INTO discounts(description, discount_percentage, created_by) 
VALUES ('New Year Sale', 10, 1);
INSERT INTO discounts(description, discount_percentage, created_by) 
VALUES ('Good Friday Sale', 40, 1);
INSERT INTO discounts(description, discount_percentage, created_by) 
VALUES ('Boxing Day Sale', 50, 1);
SET FOREIGN_KEY_CHECKS = 1;