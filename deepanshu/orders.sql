INSERT INTO orders(user_id, transaction_id, ordered_date, customer_address, sub_total, gst, pst, total, status, created_by) 
VALUES (10, 4, '2019-02-25', '33 Hargrave Street', 100, 7, 5, 112, 'ordered', 1 );
INSERT INTO orders(user_id, transaction_id, ordered_date, customer_address, sub_total, gst, pst, total, status, created_by) 
VALUES (8, 2, '2017-04-25', '33 Adsum Drive', 50, 3.5, 2.5, 56, 'paid', 1 );
INSERT INTO orders(user_id, transaction_id, ordered_date, customer_address, sub_total, gst, pst, total, status, created_by) 
VALUES (2, 8, '2018-03-20', '104 Jefferson Ave', 75, 5.25, 3.75, 84, 'paid', 1 );
INSERT INTO orders(user_id, transaction_id, ordered_date, customer_address, sub_total, gst, pst, total, status, created_by) 
VALUES (1, 5, '2019-10-15', '404 Portage Ave', 200, 14, 10, 224, 'ordered', 1 );
INSERT INTO orders(user_id, transaction_id, ordered_date, customer_address, sub_total, gst, pst, total, status, created_by) 
VALUES (4, 6, '2019-04-10', '32 Logon Street', 150,10.50, 7.50, 168, 'paid', 1 );
INSERT INTO orders(user_id, transaction_id, ordered_date, customer_address, sub_total, gst, pst, total, status, created_by) 
VALUES (6, 3, '2019-08-12', '21 Ellice Ave', 300, 21, 15, 336, 'ordered', 1 );
INSERT INTO orders(user_id, transaction_id, ordered_date, customer_address, sub_total, gst, pst, total, status, created_by) 
VALUES (7, 1, '2019-09-10', '21 Langside', 40, 2.8, 2, 44.8, 'paid', 1 );
INSERT INTO orders(user_id, transaction_id, ordered_date, customer_address, sub_total, gst, pst, total, status, created_by) 
VALUES (9, 7, '2018-02-25', '24 St. Annes', 80, 5.6, 4, 89.6, 'ordered', 1 );
INSERT INTO orders(user_id, transaction_id, ordered_date, customer_address, sub_total, gst, pst, total, status, created_by) 
VALUES (3, 9, '2017-08-29', '54 South St. Vital', 90, 6.3, 4.5, 100.8, 'paid', 1 );
INSERT INTO orders(user_id, transaction_id, ordered_date, customer_address, sub_total, gst, pst, total, status, created_by) 
VALUES (10, 4, '2019-05-11', '33 Osborne Village', 105, 7.35, 5.25, 117.6, 'ordered', 1 );

