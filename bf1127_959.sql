-- MySQL dump 10.13  Distrib 8.0.16, for Win64 (x86_64)
--
-- Host: localhost    Database: bf
-- ------------------------------------------------------
-- Server version	5.7.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8mb4 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `categories` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=201 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (101,'Micareaceae','Micarea erratica (Körb.) Hertel, Rambold & Pietschmann',NULL,NULL),(102,'Passifloraceae','Passiflora anadenia Urb.',NULL,NULL),(103,'Fabaceae','Astragalus flavus Nutt.',NULL,NULL),(104,'Fabaceae','Astragalus humistratus A. Gray',NULL,NULL),(105,'Fabaceae','Astragalus missouriensis Nutt.',NULL,NULL),(106,'Brassicaceae','Arabis holboellii Hornem. var. holboellii',NULL,NULL),(107,'Fabaceae','Macroptilium lathyroides (L.) Urb.',NULL,NULL),(108,'Asteraceae','Melampodium L.',NULL,NULL),(109,'Cyperaceae','Schoenus apogon Roem. & Schult.',NULL,NULL),(110,'Ericaceae','Arctostaphylos glandulosa Eastw. ssp. glaucomollis P.V. Wells',NULL,NULL),(111,'Aspleniaceae','Asplenium contiguum Kaulf. var. hirtulum C. Chr.',NULL,NULL),(112,'Poaceae','Panicum amarum Elliott',NULL,NULL),(113,'Asteraceae','Hieracium scabrum Michx.',NULL,NULL),(114,'Polemoniaceae','Aliciella leptomeria (A. Gray) J.M. Porter',NULL,NULL),(115,'Betulaceae','Nothofagus Blume, nom. cons.',NULL,NULL),(116,'Dicranaceae','Dicranella grevilleana (Brid.) Schimp.',NULL,NULL),(117,'Asteraceae','Baccharis bigelovii A. Gray',NULL,NULL),(118,'Boraginaceae','Plagiobothrys tener (Greene) I.M. Johnst. var. subglaber I.M. Johnst.',NULL,NULL),(119,'Cyperaceae','Morelotia gahniiformis Gaudich.',NULL,NULL),(120,'Asteraceae','Sigesbeckia orientalis L.',NULL,NULL),(121,'Rhamnaceae','Ceanothus ×vanrensselaeri J.B. Roof',NULL,NULL),(122,'Bryaceae','Brachymenium speciosum (Hook. f. & Wilson) Steere',NULL,NULL),(123,'Rubiaceae','Hedyotis flynnii W.L. Wagner & D.H. Lorence',NULL,NULL),(124,'Funariaceae','Funaria groutiana Fife',NULL,NULL),(125,'Fabaceae','Clitoria laurifolia Poir.',NULL,NULL),(126,'Timmiaceae','Timmia megapolitana Hedw.',NULL,NULL),(127,'Fabaceae','Lupinus bicolor Lindl. ssp. marginatus D. Dunn',NULL,NULL),(128,'Celastraceae','Euonymus alatus (Thunb.) Siebold',NULL,NULL),(129,'Orchidaceae','Spiranthes ×intermedia Ames',NULL,NULL),(130,'Collemataceae','Collema F.H. Wigg',NULL,NULL),(131,'Boraginaceae','Amsinckia vernicosa Hook. & Arn. var. furcata (Suksd.) Hoover',NULL,NULL),(132,'Ranunculaceae','Ranunculus bonariensis Poir. var. trisepalus (Gillies ex Hook. & Arn.) Lourteig',NULL,NULL),(133,'Asteraceae','Erigeron aequifolius H.M. Hall',NULL,NULL),(134,'Hymeneliaceae','Aspicilia verrucigera Hue',NULL,NULL),(135,'Hydrophyllaceae','Phacelia saxicola A. Gray',NULL,NULL),(136,'Polemoniaceae','Leptosiphon parviflorus (Benth.) J.M. Porter & L.A. Johnson',NULL,NULL),(137,'Brassicaceae','Erysimum franciscanum G. Rossb.',NULL,NULL),(138,'Asteraceae','Stephanomeria wrightii A. Gray',NULL,NULL),(139,'Boraginaceae','Rochefortia Sw.',NULL,NULL),(140,'Hymeneliaceae','Aspicilia subplicigera (H. Magn.) Oksner',NULL,NULL),(141,'Lecanoraceae','Lecanora carpinea (L.) Vain.',NULL,NULL),(142,'Boraginaceae','Cryptantha celosioides (Eastw.) Payson',NULL,NULL),(143,'Cyperaceae','Rhynchospora plumosa Elliott',NULL,NULL),(144,'Gesneriaceae','Gesneria L.',NULL,NULL),(145,'Cyperaceae','Carex ×flavicans F. Nyl.',NULL,NULL),(146,'Poaceae','Stipa sibirica (L.) Lam.',NULL,NULL),(147,'Fabaceae','Acacia victoriae Benth.',NULL,NULL),(148,'Fabaceae','Acacia stowardii Maiden',NULL,NULL),(149,'Rutaceae','Thamnosma Torr. & Frém.',NULL,NULL),(150,'Lamiaceae','Salvia columbariae Benth. var. ziegleri Munz',NULL,NULL),(151,'Saxifragaceae','Lepuropetalon Elliott',NULL,NULL),(152,'Lythraceae','Ginoria rohrii (Vahl) Koehne',NULL,NULL),(153,'Linaceae','Linum australe A. Heller var. australe',NULL,NULL),(154,'Fabaceae','Neptunia plena (L.) Benth.',NULL,NULL),(155,'Capnodiaceae','Echinothecium reticulatum Zopf',NULL,NULL),(156,'Apiaceae','Cymopterus aboriginum M.E. Jones',NULL,NULL),(157,'Brassicaceae','Draba nivalis Lilj. var. nivalis',NULL,NULL),(158,'Parmeliaceae','Usnea dimorpha (Müll. Arg.) Mot.',NULL,NULL),(159,'Salicaceae','Salix ×cryptodonta Fernald (pro sp.)',NULL,NULL),(160,'Rhamnaceae','Ziziphus nummularia (Burm. f.) Wight & Arn.',NULL,NULL),(161,'Primulaceae','Primula cusickiana (A. Gray) A. Gray var. cusickiana',NULL,NULL),(162,'Asteraceae','Erigeron subtrinervis Rydb. ex Porter & Britton var. conspicuus (Rydb.) Cronquist',NULL,NULL),(163,'Ericaceae','Kalmia polifolia Wangenh.',NULL,NULL),(164,'Scrophulariaceae','Castilleja parviflora Bong. var. oreopola (Greenm.) Ownbey',NULL,NULL),(165,'Fabaceae','Hoita strobilina (Hook. & Arn.) Rydb.',NULL,NULL),(166,'Fabaceae','Coursetia caribaea (Jacq.) Lavin',NULL,NULL),(167,'Lamiaceae','Dicerandra immaculata Lakela',NULL,NULL),(168,'Boraginaceae','Cryptantha microstachys (Greene ex A. Gray) Greene',NULL,NULL),(169,'Rosaceae','Oemleria Rchb.',NULL,NULL),(170,'Hydrangeaceae','Jamesia americana Torr. & A. Gray var. macrocalyx (Small) Engl.',NULL,NULL),(171,'Rubiaceae','Coprosma pubens A. Gray',NULL,NULL),(172,'Geraniaceae','Geranium lucidum L.',NULL,NULL),(173,'Papaveraceae','Arctomecon californica Torr. & Frém.',NULL,NULL),(174,'Brassicaceae','Arabis glaucovalvula M.E. Jones',NULL,NULL),(175,'Brassicaceae','Leavenworthia alabamica Rollins',NULL,NULL),(176,'Chenopodiaceae','Beta trigyna Waldst. & Kit.',NULL,NULL),(177,'Fabaceae','Trifolium andersonii A. Gray ssp. monoense (Greene) J.M. Gillett',NULL,NULL),(178,'Portulacaceae','Lewisia pygmaea (A. Gray) B.L. Rob.',NULL,NULL),(179,'Fabaceae','Vigna umbellata (Thunb.) Ohwi & H. Ohashi',NULL,NULL),(180,'Scrophulariaceae','Pedicularis cystopteridifolia Rydb.',NULL,NULL),(181,'Asteraceae','Symphyotrichum falcatum (Lindl.) G.L. Nesom var. commutatum (Torr. & A. Gray) G.L. Nesom',NULL,NULL),(182,'Fabaceae','Trifolium longipes Nutt. ssp. multipedunculatum (Kennedy) J.M. Gillett',NULL,NULL),(183,'Collemataceae','Collema F.H. Wigg',NULL,NULL),(184,'Polygonaceae','Polygonum scandens L. var. dumetorum (L.) Gleason',NULL,NULL),(185,'Asteraceae','Hymenopappus scabiosaeus L\'Hér. var. scabiosaeus',NULL,NULL),(186,'Asteraceae','Lygodesmia grandiflora (Nutt.) Torr. & A. Gray',NULL,NULL),(187,'Melastomataceae','Miconia affinis DC.',NULL,NULL),(188,'Acanthaceae','Justicia culebritae Urb.',NULL,NULL),(189,'Cyperaceae','Carex striata Michx.',NULL,NULL),(190,'Lamiaceae','Trichostema lanatum Benth.',NULL,NULL),(191,'Amaranthaceae','Amaranthus graecizans L. ssp. sylvestris (Vill.) Brenan',NULL,NULL),(192,'Primulaceae','Anagallis arvensis L. ssp. arvensis',NULL,NULL),(193,'Fabaceae','Astragalus asclepiadoides M.E. Jones',NULL,NULL),(194,'Fabaceae','Adenanthera L.',NULL,NULL),(195,'Asteraceae','Solidago petiolaris Aiton var. petiolaris',NULL,NULL),(196,'Polemoniaceae','Gilia mexicana A.D. Grant & V.E. Grant',NULL,NULL),(197,'Cornaceae','Cornus foemina Mill.',NULL,NULL),(198,'Zingiberaceae','Etlingera cevuga (Seem.) R.M. Sm.',NULL,NULL),(199,'Hydrophyllaceae','Phacelia hughesii N.D. Atwood',NULL,NULL),(200,'Ranunculaceae','Ranunculus trichophyllus Chaix var. trichophyllus',NULL,NULL);
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;
