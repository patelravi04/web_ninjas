-- MySQL dump 10.13  Distrib 5.7.24, for Win64 (x86_64)
--
-- Host: localhost    Database: blossom_fantasy
-- ------------------------------------------------------
-- Server version	5.7.24

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `addresses`
--

DROP TABLE IF EXISTS `addresses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `addresses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `address_type` enum('billing','shipping') NOT NULL,
  `address` varchar(100) NOT NULL,
  `city` varchar(50) NOT NULL,
  `province` varchar(20) NOT NULL,
  `postal_code` char(7) NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_address` (`user_id`),
  CONSTRAINT `fk_user_address` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `addresses`
--

LOCK TABLES `addresses` WRITE;
/*!40000 ALTER TABLE `addresses` DISABLE KEYS */;
INSERT INTO `addresses` VALUES (1,1,'billing','Portage Ave','Winnipeg','Manitoba','R5B 2D4',NULL,'2019-12-03 18:44:45',1,NULL,NULL),(2,1,'shipping','Portage Ave','Winnipeg','Manitoba','R5B 2D4',NULL,'2019-12-03 18:44:45',1,NULL,NULL),(3,2,'billing','Sherbook Ave','Montreal','Quebec','H3A 0G4',NULL,'2019-12-03 18:44:45',2,NULL,NULL),(4,2,'shipping','Sherbook Ave','Montreal','Quebec','H3A 0G4',NULL,'2019-12-03 18:44:45',1,NULL,NULL),(5,3,'billing','Jefferson Ave','Winnipeg','Manitoba','R5B 2D4',NULL,'2019-12-03 18:44:45',1,NULL,NULL),(6,3,'shipping','Jefferson Ave','Winnipeg','Manitoba','R5B 2D4',NULL,'2019-12-03 18:44:45',1,NULL,NULL),(7,4,'billing','Sheepard Street','Winnipeg','Manitoba','R5B 2D4',NULL,'2019-12-03 18:44:45',4,NULL,NULL),(8,4,'shipping','Sheepard Street','Winnipeg','Manitoba','R5B 2D4',NULL,'2019-12-03 18:44:46',4,NULL,NULL),(9,5,'billing','Martin Ave','Winnipeg','Manitoba','R5B 2D4',NULL,'2019-12-03 18:44:46',1,NULL,NULL),(10,5,'shipping','Martin Ave','Winnipeg','Manitoba','R5B 2D4',NULL,'2019-12-03 18:44:46',1,NULL,NULL),(11,6,'billing','Adsum Drive','Winnipeg','Manitoba','R5B 2D4',NULL,'2019-12-03 18:44:46',1,NULL,NULL),(12,6,'shipping','Adsum Drive','Winnipeg','Manitoba','R5B 2D4',NULL,'2019-12-03 18:44:46',1,NULL,NULL),(13,7,'billing','Queen Street','Toronto','Ontario','M5H 2N1',NULL,'2019-12-03 18:44:46',1,NULL,NULL),(14,7,'shipping','Queen Street','Toronto','Ontario','M5H 2N1',NULL,'2019-12-03 18:44:46',1,NULL,NULL),(15,8,'billing','Robson Street','Vancouver','British Columbia','V6B 3K9',NULL,'2019-12-03 18:44:46',1,NULL,NULL),(16,8,'shipping','Robson Street','Vancouver','British Columbia','V6B 3K9',NULL,'2019-12-03 18:44:46',1,NULL,NULL),(17,9,'billing','Patterosn Ave','Calgary','Alberta','T3H 2E1',NULL,'2019-12-03 18:44:46',1,NULL,NULL),(18,9,'shipping','Patterosn Ave','Calgary','Alberta','T3H 2E1',NULL,'2019-12-03 18:44:46',1,NULL,NULL),(19,10,'billing','King Ave','Halifax','Nova Scotia','B3J 3A5',NULL,'2019-12-03 18:44:46',1,NULL,NULL),(20,10,'shipping','King Ave','Halifax','Nova Scotia','B3J 3A5',NULL,'2019-12-03 18:44:46',1,NULL,NULL);
/*!40000 ALTER TABLE `addresses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `description` varchar(255) NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'Birthday','tortor duis mattis egestas metus aenean fermentum donec ut mauris eget massa tempor convallis',NULL,'2019-12-02 14:30:47',6,NULL,NULL),(2,'Anniversary','tempus semper est quam pharetra magna ac consequat metus sapien ut',NULL,'2019-12-02 14:30:47',4,NULL,NULL),(3,'Wedding','molestie nibh in lectus pellentesque at nulla suspendisse potenti cras in purus eu magna',NULL,'2019-12-02 14:30:47',2,NULL,NULL),(4,'Debut','sed interdum venenatis turpis enim blandit mi in porttitor pede justo eu massa donec dapibus duis at velit eu',NULL,'2019-12-02 14:30:47',2,NULL,NULL),(5,'Prom','felis eu sapien cursus vestibulum proin eu mi nulla ac enim in tempor turpis nec euismod',NULL,'2019-12-02 14:30:47',5,NULL,NULL),(6,'Grand Opening','interdum in ante vestibulum ante ipsum primis in faucibus orci',NULL,'2019-12-02 14:30:47',4,NULL,NULL),(7,'Funeral','eros vestibulum ac est lacinia nisi venenatis tristique fusce congue diam id ornare imperdiet sapien urna pretium nisl',NULL,'2019-12-02 14:30:47',6,NULL,NULL),(8,'Festival','pellentesque eget nunc donec quis orci eget orci vehicula condimentum curabitur in libero ut',NULL,'2019-12-02 14:30:47',4,NULL,NULL),(9,'Christmas','consequat varius integer ac leo pellentesque ultrices mattis odio donec vitae nisi nam ultrices libero',NULL,'2019-12-02 14:30:47',8,NULL,NULL),(10,'Thanksgiving','pharetra magna vestibulum aliquet ultrices erat tortor sollicitudin mi sit amet lobortis sapien sapien non mi integer ac neque duis',NULL,'2019-12-02 14:30:52',4,NULL,NULL);
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category_product`
--

DROP TABLE IF EXISTS `category_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_prod_cat_product` (`product_id`),
  KEY `fk_prod_cat_category` (`category_id`),
  CONSTRAINT `fk_prod_cat_category` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON UPDATE NO ACTION,
  CONSTRAINT `fk_prod_cat_product` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category_product`
--

LOCK TABLES `category_product` WRITE;
/*!40000 ALTER TABLE `category_product` DISABLE KEYS */;
INSERT INTO `category_product` VALUES (1,7,72),(2,2,107),(3,3,101),(4,8,78),(5,8,75),(6,7,102),(7,5,97),(8,10,89),(9,2,102),(10,1,68),(11,6,87),(12,4,61),(13,1,70),(14,10,91),(15,2,59),(16,3,108),(17,7,109),(18,6,85),(19,2,59),(20,2,72),(21,4,92),(22,5,89),(23,10,60),(24,5,74),(25,8,110),(26,8,106),(27,7,81),(28,5,81),(29,3,68),(30,9,102),(31,8,95),(32,3,63),(33,10,71),(34,1,111),(35,6,71),(36,5,103),(37,7,72),(38,9,86),(39,8,86),(40,10,75),(41,1,59),(42,6,111),(43,4,71),(44,1,79),(45,10,100),(46,1,105),(47,7,73),(48,3,92),(49,10,99),(50,9,110),(51,10,69),(52,1,103),(53,8,80),(54,8,91),(55,3,104),(56,5,69);
/*!40000 ALTER TABLE `category_product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `delivery_charges`
--

DROP TABLE IF EXISTS `delivery_charges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `delivery_charges` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cost` decimal(5,2) NOT NULL,
  `days_for_delivery` int(11) NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `delivery_charges`
--

LOCK TABLES `delivery_charges` WRITE;
/*!40000 ALTER TABLE `delivery_charges` DISABLE KEYS */;
INSERT INTO `delivery_charges` VALUES (1,0.00,7,NULL,'2019-12-03 18:49:54',1,NULL,NULL),(2,10.00,1,NULL,'2019-12-03 18:49:54',1,NULL,NULL),(3,7.00,2,NULL,'2019-12-03 18:49:54',1,NULL,NULL),(4,7.00,3,NULL,'2019-12-03 18:49:54',1,NULL,NULL),(5,5.00,4,NULL,'2019-12-03 18:49:54',1,NULL,NULL),(6,5.00,5,NULL,'2019-12-03 18:49:54',1,NULL,NULL),(7,5.00,6,NULL,'2019-12-03 18:49:54',1,NULL,NULL),(8,0.00,8,NULL,'2019-12-03 18:49:54',1,NULL,NULL),(9,0.00,9,NULL,'2019-12-03 18:49:54',1,NULL,NULL),(10,0.00,10,NULL,'2019-12-03 18:49:54',1,NULL,NULL);
/*!40000 ALTER TABLE `delivery_charges` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `discounts`
--

DROP TABLE IF EXISTS `discounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `discounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` text NOT NULL,
  `percentage` int(11) NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `discounts`
--

LOCK TABLES `discounts` WRITE;
/*!40000 ALTER TABLE `discounts` DISABLE KEYS */;
INSERT INTO `discounts` VALUES (1,'Cyber Monday Sale',30,NULL,'2019-12-03 18:46:33',1,NULL,NULL),(2,'Black Friday Sale',20,NULL,'2019-12-03 18:46:33',1,NULL,NULL),(3,'Sunday Sale Sale',10,NULL,'2019-12-03 18:46:33',1,NULL,NULL),(4,'Thanksgiving Day Sale',15,NULL,'2019-12-03 18:46:34',1,NULL,NULL),(5,'Rememberance Day Sale',30,NULL,'2019-12-03 18:46:34',1,NULL,NULL),(6,'Canada Day Sale',25,NULL,'2019-12-03 18:46:34',1,NULL,NULL),(7,'Labour Day Sale',30,NULL,'2019-12-03 18:46:34',1,NULL,NULL),(8,'New Year Sale',10,NULL,'2019-12-03 18:46:34',1,NULL,NULL),(9,'Good Friday Sale',40,NULL,'2019-12-03 18:46:34',1,NULL,NULL),(10,'Boxing Day Sale',50,NULL,'2019-12-03 18:46:34',1,NULL,NULL);
/*!40000 ALTER TABLE `discounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_line_items`
--

DROP TABLE IF EXISTS `order_line_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_line_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `unit_price` decimal(5,2) NOT NULL,
  `discount` decimal(5,2) NOT NULL,
  `quantity` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_line_item_order` (`order_id`),
  KEY `fk_line_item_product` (`product_id`),
  CONSTRAINT `fk_line_item_order` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON UPDATE NO ACTION,
  CONSTRAINT `fk_line_item_product` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_line_items`
--

LOCK TABLES `order_line_items` WRITE;
/*!40000 ALTER TABLE `order_line_items` DISABLE KEYS */;
INSERT INTO `order_line_items` VALUES (1,2,57,45.00,0.00,1),(2,2,58,55.00,0.00,2),(3,3,60,75.00,10.00,1),(4,4,62,35.00,0.00,2),(5,4,63,35.00,0.00,2),(6,5,67,85.00,0.00,1),(7,6,70,55.00,4.00,3),(8,6,72,40.00,0.00,2),(9,7,75,95.00,9.50,1),(10,8,78,100.00,10.00,1),(11,8,80,50.00,5.00,2),(12,8,82,75.00,10.00,3),(13,9,85,90.00,0.00,1),(14,10,88,80.00,0.00,1),(15,11,90,70.00,0.00,2),(16,11,93,60.00,5.00,4),(17,11,95,120.00,10.00,3);
/*!40000 ALTER TABLE `order_line_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `transaction_id` int(11) NOT NULL,
  `ordered_date` date NOT NULL,
  `billing_address` text NOT NULL,
  `shipping_address` text NOT NULL,
  `sub_total` decimal(6,2) NOT NULL,
  `gst` decimal(5,2) NOT NULL,
  `pst` decimal(5,2) NOT NULL,
  `total` decimal(6,2) NOT NULL,
  `status` enum('ordered','paid') NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_order` (`user_id`),
  KEY `fk_order_transaction` (`transaction_id`),
  CONSTRAINT `fk_order_transaction` FOREIGN KEY (`transaction_id`) REFERENCES `transactions` (`id`) ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_order` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES (2,10,4,'2019-02-25','33 Hargrave Street','33 Hargrave Street',100.00,7.00,5.00,112.00,'ordered','2019-12-03 18:53:17',1,NULL,NULL),(3,8,2,'2017-04-25','33 Adsum Drive','33 Adsum Drive',50.00,3.50,2.50,56.00,'paid','2019-12-03 18:53:18',1,NULL,NULL),(4,2,8,'2018-03-20','104 Jefferson Ave','104 Jefferson Ave',75.00,5.25,3.75,84.00,'paid','2019-12-03 18:53:18',1,NULL,NULL),(5,1,5,'2019-10-15','404 Portage Ave','404 Portage Ave',200.00,14.00,10.00,224.00,'ordered','2019-12-03 18:53:18',1,NULL,NULL),(6,4,6,'2019-04-10','32 Logon Street','32 Logon Street',150.00,10.50,7.50,168.00,'paid','2019-12-03 18:53:18',1,NULL,NULL),(7,6,3,'2019-08-12','21 Ellice Ave','21 Ellice Ave',300.00,21.00,15.00,336.00,'ordered','2019-12-03 18:53:18',1,NULL,NULL),(8,7,1,'2019-09-10','21 Langside','21 Langside',40.00,2.80,2.00,44.80,'paid','2019-12-03 18:53:18',1,NULL,NULL),(9,9,7,'2018-02-25','24 St. Annes','24 St. Annes',80.00,5.60,4.00,89.60,'ordered','2019-12-03 18:53:18',1,NULL,NULL),(10,3,9,'2017-08-29','54 South St. Vital','54 South St. Vital',90.00,6.30,4.50,100.80,'paid','2019-12-03 18:53:18',1,NULL,NULL),(11,10,4,'2019-05-11','33 Osborne Village','33 Osborne Village',105.00,7.35,5.25,117.60,'ordered','2019-12-03 18:53:18',1,NULL,NULL);
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `price` decimal(5,2) NOT NULL,
  `quantity` int(11) NOT NULL,
  `max_days_for_delivery` int(11) NOT NULL,
  `reorder_level` int(11) NOT NULL,
  `image` varchar(100) DEFAULT NULL,
  `discount_applicable` tinyint(1) NOT NULL DEFAULT '0',
  `discount_in_price` decimal(5,2) DEFAULT '0.00',
  `discount_id` int(11) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_product_discount` (`discount_id`),
  CONSTRAINT `fk_product_discount` FOREIGN KEY (`discount_id`) REFERENCES `discounts` (`id`) ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=113 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (57,'Pioneer Violet','aenean sit amet justo morbi ut odio cras mi pede',27.69,109,2,7,'images\\flowers\\alyssum.jpe',0,4.89,9,NULL,'2019-12-02 15:06:41',8,NULL,4),(58,'Ghost Flower','pharetra magna vestibulum aliquet ultrices erat tortor sollicitudin mi sit',34.27,165,5,9,'images\\flowers\\Amc-DvMayDelaware.jpe',1,1.89,9,NULL,'2019-12-02 15:06:41',10,NULL,10),(59,'Pineland Daisy','ut suscipit a feugiat et eros vestibulum ac est lacinia nisi venenatis tristique fusce congue diam id ornare',49.65,253,4,6,'images\\flowers\\beeBalm.jpg',1,1.76,2,NULL,'2019-12-02 15:06:41',7,NULL,7),(60,'Maguire\'s Penstemon','nulla pede ullamcorper augue a suscipit nulla elit ac nulla sed vel enim sit amet nunc viverra',12.13,370,1,5,'images\\flowers\\bittersweet.jpe',0,1.03,2,NULL,'2019-12-02 15:06:41',7,NULL,2),(61,'False Goldeneye','vivamus in felis eu sapien cursus vestibulum proin eu mi nulla ac',65.32,276,2,7,'images\\flowers\\bittersweett.jpe',1,4.98,10,NULL,'2019-12-02 15:06:41',9,NULL,9),(62,'Taxiphyllum Moss','magnis dis parturient montes nascetur ridiculus mus vivamus vestibulum sagittis sapien cum sociis natoque penatibus et magnis dis',6.65,167,5,9,'images\\flowers\\blackEyedSusanFlower.jpe',0,2.80,7,NULL,'2019-12-02 15:06:41',10,NULL,7),(63,'Thrift Seapink','consequat lectus in est risus auctor sed tristique in tempus sit amet',82.26,837,2,9,'images\\flowers\\blueFlagIris.jpg',1,1.72,2,NULL,'2019-12-02 15:06:41',6,NULL,2),(64,'Szatala\'s Crabseye Lichen','gravida sem praesent id massa id nisl venenatis lacinia aenean sit amet justo morbi ut odio cras',84.31,445,3,7,'images\\flowers\\butterflyWeed.jpe',0,1.73,2,NULL,'2019-12-02 15:06:41',3,NULL,3),(65,'Foothill Rush','nulla facilisi cras non velit nec nisi vulputate nonummy maecenas',60.35,197,5,7,'images\\flowers\\camprisRadicum.jpe',0,4.57,8,NULL,'2019-12-02 15:06:41',6,NULL,5),(66,'Skyttella Lichen','in hac habitasse platea dictumst morbi vestibulum velit id pretium iaculis diam erat fermentum justo nec condimentum neque sapien placerat',59.60,399,5,10,'images\\flowers\\cercisCanadensis.jpg',0,3.05,8,NULL,'2019-12-02 15:06:41',5,NULL,3),(67,'Butterweed','cum sociis natoque penatibus et magnis dis parturient montes nascetur ridiculus mus etiam vel augue vestibulum rutrum rutrum neque aenean',71.45,140,3,6,'images\\flowers\\Cornation.jpe',0,3.12,1,NULL,'2019-12-02 15:06:41',1,NULL,4),(68,'Baker\'s Goldfields','morbi a ipsum integer a nibh in quis justo maecenas rhoncus aliquam',34.66,521,2,7,'images\\flowers\\dagwood.jpe',1,3.93,1,NULL,'2019-12-02 15:06:41',7,NULL,6),(69,'Candle Snuffer Moss','ultrices posuere cubilia curae duis faucibus accumsan odio curabitur convallis duis consequat dui nec nisi volutpat eleifend',38.93,846,4,10,'images\\flowers\\Dianthus.jpe',0,3.63,5,NULL,'2019-12-02 15:06:41',5,NULL,6),(70,'Lindheimer\'s Ticktrefoil','condimentum curabitur in libero ut massa volutpat convallis morbi odio odio elementum eu interdum eu tincidunt in leo maecenas',20.17,658,2,10,'images\\flowers\\downyLobelia.jpe',1,1.75,4,NULL,'2019-12-02 15:06:41',9,NULL,7),(71,'Mountain Chickweed','nam dui proin leo odio porttitor id consequat in consequat ut nulla sed accumsan',81.32,37,1,5,'images\\flowers\\DustyZenobia.jpg',1,3.93,5,NULL,'2019-12-02 15:06:41',5,NULL,1),(72,'Rio Grande Bugheal','nisl duis bibendum felis sed interdum venenatis turpis enim blandit mi in porttitor pede',4.05,208,3,8,'images\\flowers\\ekluntnaFlats.jpg',1,2.64,5,NULL,'2019-12-02 15:06:41',5,NULL,3),(73,'Sixweeks Fescue','quisque porta volutpat erat quisque erat eros viverra eget congue eget semper rutrum nulla nunc purus phasellus in felis',57.99,890,4,8,'images\\flowers\\FES.jpe',1,1.41,9,NULL,'2019-12-02 15:06:41',5,NULL,10),(74,'Common Woolly Sunflower','nulla integer pede justo lacinia eget tincidunt eget tempus vel pede morbi porttitor lorem id ligula suspendisse ornare',90.87,146,4,5,'images\\flowers\\globalgalleryMexican.jpe',0,1.33,2,NULL,'2019-12-02 15:06:41',5,NULL,1),(75,'Jones\' Nailwort','magna vestibulum aliquet ultrices erat tortor sollicitudin mi sit amet lobortis sapien sapien non mi integer',58.21,596,4,9,'images\\flowers\\growingCelosiaFlowers.jpg',1,2.82,5,NULL,'2019-12-02 15:06:41',1,NULL,6),(76,'Littleleaf Brickellbush','at velit vivamus vel nulla eget eros elementum pellentesque quisque',79.70,79,1,5,'images\\flowers\\highRes.jpe',1,2.56,4,NULL,'2019-12-02 15:06:41',10,NULL,7),(77,'Rosy Palafox','id mauris vulputate elementum nullam varius nulla facilisi cras non',37.32,451,4,6,'images\\flowers\\indianBlanket.jpe',0,3.14,4,NULL,'2019-12-02 15:06:41',7,NULL,9),(78,'Fuzzy Mock Orange','duis ac nibh fusce lacus purus aliquet at feugiat non pretium',76.01,306,3,5,'images\\flowers\\Iris.jpe',0,3.92,4,NULL,'2019-12-02 15:06:41',1,NULL,10),(79,'Inland Muilla','lobortis sapien sapien non mi integer ac neque duis bibendum morbi non quam nec dui luctus rutrum nulla tellus',25.69,586,3,10,'images\\flowers\\irisCristata.jpg',1,1.63,6,NULL,'2019-12-02 15:06:41',8,NULL,6),(80,'Idaho Kittentails','pede justo eu massa donec dapibus duis at velit eu est',40.10,14,3,8,'images\\flowers\\lantana.jpg',0,3.74,2,NULL,'2019-12-02 15:06:41',5,NULL,10),(81,'Watercrown Grass','scelerisque mauris sit amet eros suspendisse accumsan tortor quis turpis sed ante vivamus tortor duis mattis egestas metus',91.42,51,3,8,'images\\flowers\\Lupiness.jpe',1,1.83,4,NULL,'2019-12-02 15:06:41',6,NULL,5),(82,'Howell\'s Fawnlily','erat nulla tempus vivamus in felis eu sapien cursus vestibulum proin eu mi',67.23,28,5,8,'images\\flowers\\mayDelaware.jpe',1,4.83,6,NULL,'2019-12-02 15:06:41',1,NULL,9),(83,'Newberry\'s Tickseed','penatibus et magnis dis parturient montes nascetur ridiculus mus vivamus vestibulum',68.63,384,1,5,'images\\flowers\\Mayflower.jpe',1,2.92,9,NULL,'2019-12-02 15:06:41',8,NULL,2),(84,'Vitt Tube Lichen','sit amet eros suspendisse accumsan tortor quis turpis sed ante vivamus',69.55,311,2,6,'images\\flowers\\mexican.jpe',0,2.62,5,NULL,'2019-12-02 15:06:41',7,NULL,8),(85,'Leptarrhena','convallis nunc proin at turpis a pede posuere nonummy integer non velit donec diam',83.52,166,1,8,'images\\flowers\\mountainLaurel.jpe',0,3.49,10,NULL,'2019-12-02 15:06:41',1,NULL,7),(86,'Herbertia','condimentum neque sapien placerat ante nulla justo aliquam quis turpis eget elit sodales scelerisque mauris sit amet eros suspendisse',3.33,963,2,7,'images\\flowers\\mullein-verbascum-thapsus.jpg',0,2.87,1,NULL,'2019-12-02 15:06:41',3,NULL,3),(87,'Asian Taro','donec ut dolor morbi vel lectus in quam fringilla rhoncus',75.39,548,4,9,'images\\flowers\\muskMallow.jpe',0,2.55,7,NULL,'2019-12-02 15:06:41',2,NULL,8),(88,'Bristlyleaf Rockcress','sapien placerat ante nulla justo aliquam quis turpis eget elit sodales scelerisque mauris sit',24.64,367,5,8,'images\\flowers\\NiftyPlant.jpe',0,4.54,4,NULL,'2019-12-02 15:06:41',2,NULL,4),(89,'Beyrich\'s Hooded Orchid','sapien arcu sed augue aliquam erat volutpat in congue etiam justo etiam',88.78,729,3,6,'images\\flowers\\nineOfcups.jpe',1,4.21,7,NULL,'2019-12-02 15:06:41',10,NULL,5),(90,'Mildred\'s Clarkia','donec diam neque vestibulum eget vulputate ut ultrices vel augue vestibulum ante ipsum primis in faucibus orci luctus et',53.79,199,3,6,'images\\flowers\\northAmericanPitcher.jpe',1,2.92,7,NULL,'2019-12-02 15:06:41',4,NULL,9),(91,'San Bernardino Vervain','consectetuer eget rutrum at lorem integer tincidunt ante vel ipsum praesent blandit lacinia erat vestibulum sed magna at nunc',64.28,471,3,10,'images\\flowers\\OshoOrchard.jpe',0,4.17,4,NULL,'2019-12-02 15:06:41',2,NULL,10),(92,'Mancos Shale Packera','vel pede morbi porttitor lorem id ligula suspendisse ornare consequat lectus in',76.46,563,2,7,'images\\flowers\\PinkLily.jpe',0,4.18,7,NULL,'2019-12-02 15:06:41',8,NULL,5),(93,'Barbula Moss','scelerisque quam turpis adipiscing lorem vitae mattis nibh ligula nec',87.38,990,4,6,'images\\flowers\\pinkYellowDaylilies.jpe',1,4.16,8,NULL,'2019-12-02 15:06:41',8,NULL,1),(94,'Toad Rush','dui maecenas tristique est et tempus semper est quam pharetra',57.12,399,2,7,'images\\flowers\\pitcher.jpe',0,1.95,8,NULL,'2019-12-02 15:06:41',10,NULL,3),(95,'Sierra Ancha Fleabane','porttitor pede justo eu massa donec dapibus duis at velit eu est',97.99,502,4,8,'images\\flowers\\polemoniacaephlox.jpe',0,3.83,1,NULL,'2019-12-02 15:06:41',10,NULL,9),(96,'Pseudopanax','eget orci vehicula condimentum curabitur in libero ut massa volutpat convallis morbi odio odio elementum eu interdum eu tincidunt in',55.89,627,4,5,'images\\flowers\\priklypopy.jpe',1,1.78,6,NULL,'2019-12-02 15:06:41',6,NULL,7),(97,'Flannelbush','eros viverra eget congue eget semper rutrum nulla nunc purus phasellus',97.84,226,1,6,'images\\flowers\\purpleFlowers.jpe',0,1.53,9,NULL,'2019-12-02 15:06:41',4,NULL,4),(98,'New Mexico Orange Lichen','ultrices posuere cubilia curae nulla dapibus dolor vel est donec odio justo sollicitudin ut',21.95,990,2,8,'images\\flowers\\rareFlowers.jpe',1,3.38,8,NULL,'2019-12-02 15:06:41',8,NULL,6),(99,'Texas Snakeweed','mauris vulputate elementum nullam varius nulla facilisi cras non velit nec nisi',63.35,379,3,10,'images\\flowers\\redGingerFlowers.jpe',0,4.33,7,NULL,'2019-12-02 15:06:41',8,NULL,10),(100,'Grimmia Dry Rock Moss','nam nulla integer pede justo lacinia eget tincidunt eget tempus vel pede morbi porttitor lorem',2.29,468,1,5,'images\\flowers\\rubyThroatedHummingbird.jpe',1,1.85,2,NULL,'2019-12-02 15:06:41',7,NULL,10),(101,'Italian Catchfly','nunc donec quis orci eget orci vehicula condimentum curabitur in libero ut',86.42,537,1,9,'images\\flowers\\shutterStock.jpe',0,4.81,8,NULL,'2019-12-02 15:06:41',3,NULL,8),(102,'Vahl\'s Alkaligrass','congue etiam justo etiam pretium iaculis justo in hac habitasse platea dictumst',80.46,314,5,6,'images\\flowers\\springflowers.jpe',0,1.28,7,NULL,'2019-12-02 15:06:41',4,NULL,10),(103,'Mexican Primrose-willow','suspendisse potenti in eleifend quam a odio in hac habitasse',89.57,202,1,5,'images\\flowers\\Tropical.jpe',1,3.10,2,NULL,'2019-12-02 15:06:41',2,NULL,8),(104,'Olapalapa','lobortis sapien sapien non mi integer ac neque duis bibendum morbi non',21.18,759,4,5,'images\\flowers\\tulip.jpe',0,2.60,5,NULL,'2019-12-02 15:06:41',2,NULL,5),(105,'Cretan Bryony','amet lobortis sapien sapien non mi integer ac neque duis bibendum morbi non quam nec dui luctus rutrum nulla tellus',37.88,941,2,6,'images\\flowers\\umbels.jpg',1,3.71,7,NULL,'2019-12-02 15:06:41',7,NULL,6),(106,'Waxy Checkerbloom','aliquet ultrices erat tortor sollicitudin mi sit amet lobortis sapien sapien non mi integer ac neque',84.40,828,3,5,'images\\flowers\\verbascumThapsus.jpg',1,3.05,7,NULL,'2019-12-02 15:06:41',7,NULL,8),(107,'Scentless Mock Orange','leo rhoncus sed vestibulum sit amet cursus id turpis integer aliquet massa id lobortis convallis tortor',36.24,10,5,10,'images\\flowers\\verbascumThapsus1.jpg',1,4.30,1,NULL,'2019-12-02 15:06:42',2,NULL,10),(108,'Scarlet Spiderling','quis justo maecenas rhoncus aliquam lacus morbi quis tortor id nulla',98.68,573,2,5,'images\\flowers\\wildFlowerBlanket.jpg',0,1.90,8,NULL,'2019-12-02 15:06:42',2,NULL,4),(109,'Milletgrass','in eleifend quam a odio in hac habitasse platea dictumst maecenas ut massa quis augue luctus tincidunt',99.66,113,1,8,'images\\flowers\\wildflowers.jpg',0,4.55,7,NULL,'2019-12-02 15:06:42',5,NULL,7),(110,'Antilles Calophyllum','nisi volutpat eleifend donec ut dolor morbi vel lectus in quam fringilla rhoncus mauris enim',57.68,181,4,6,'images\\flowers\\wildIris.jpe',0,1.17,6,NULL,'2019-12-02 15:06:42',4,NULL,2),(111,'Gall Of The Earth','nulla facilisi cras non velit nec nisi vulputate nonummy maecenas tincidunt lacus',18.68,188,5,10,'images\\flowers\\yellowTriliums.jpg',1,3.29,10,NULL,'2019-12-02 15:06:42',3,NULL,1),(112,'Littlehip Hawthorn','sapien in sapien iaculis congue vivamus metus arcu adipiscing molestie hendrerit at vulputate vitae nisl',35.42,903,5,8,'images\\flowers\\zenobiaPulverulenta.jpe',1,3.45,9,NULL,'2019-12-02 15:06:42',2,NULL,10);
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `taxes`
--

DROP TABLE IF EXISTS `taxes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `taxes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(3) NOT NULL,
  `description` varchar(100) NOT NULL,
  `percentage` int(11) NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `taxes`
--

LOCK TABLES `taxes` WRITE;
/*!40000 ALTER TABLE `taxes` DISABLE KEYS */;
INSERT INTO `taxes` VALUES (1,'gst','Goods and Service Tax',7,NULL,'2019-12-03 18:54:18',1,NULL,NULL),(2,'pst','Provincial Sales Tax',8,NULL,'2019-12-03 18:54:18',1,NULL,NULL),(3,'gst','Goods and Service Tax',5,NULL,'2019-12-03 18:54:18',1,NULL,NULL),(4,'pst','Provincial Sales Tax',7,NULL,'2019-12-03 18:54:18',1,NULL,NULL),(5,'gst','Goods and Service Tax',7,NULL,'2019-12-03 18:54:18',1,NULL,NULL),(6,'pst','Provincial Sales Tax',5,NULL,'2019-12-03 18:54:18',1,NULL,NULL),(7,'gst','Goods and Service Tax',8,NULL,'2019-12-03 18:54:18',1,NULL,NULL),(8,'pst','Provincial Sales Tax',7,NULL,'2019-12-03 18:54:18',1,NULL,NULL),(9,'gst','Goods and Service Tax',8,NULL,'2019-12-03 18:54:18',1,NULL,NULL),(10,'pst','Provincial Sales Tax',5,NULL,'2019-12-03 18:54:19',1,NULL,NULL);
/*!40000 ALTER TABLE `taxes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `testimonials`
--

DROP TABLE IF EXISTS `testimonials`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `testimonials` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `is_published` tinyint(1) NOT NULL DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_testimonial` (`user_id`),
  CONSTRAINT `fk_user_testimonial` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `testimonials`
--

LOCK TABLES `testimonials` WRITE;
/*!40000 ALTER TABLE `testimonials` DISABLE KEYS */;
INSERT INTO `testimonials` VALUES (1,2,'Words can’t express how grateful I am that you were our florist for our big \r\n				day. Working with you was so enjoyable and straightforward because you understood \r\n				what I wanted straight away and I trusted you completely!\r\n				So many people commented on how gorgeous the flowers were (even boys!) so I \r\n				have sung your praises to anyone who will listen.',0,NULL,'2019-12-03 18:54:38',1,NULL,NULL),(2,3,'I wanted to drop you a line and thank you SOOO much for the beautiful \r\n				flowers. You took my brief and made it EVEN better. They were simply STUNNING.',0,NULL,'2019-12-03 18:54:38',1,NULL,NULL),(3,4,'I just wanted to say a HUGE HUGE thank you for doing my beautiful flowers \r\n				for the wedding. The arch was absolutely gorgeous and I can’t believe it was all \r\n				for my wedding. I was so overwhelmed when the flowers arrived at Chilly Powder too, \r\n				they matched the girls dresses perfectly, the colours were subtle and the blushes \r\n				just made the flowers all that much sweeter.',0,NULL,'2019-12-03 18:54:38',1,NULL,NULL),(4,5,'Florists at your company are the loveliest persons to deal with and \r\n				could not have been more helpful; even going so far as to find us a replacement organist \r\n				at the last minute, what service!',0,NULL,'2019-12-03 18:54:38',1,NULL,NULL),(5,6,'Thank you SO SO much for such beautiful flowers at our wedding. Our big \r\n				day was all we could have dreamed of and more. We had such a wonderful day, and \r\n				the style/ colours of the flowers in the Chapel and Cabane looked fantastic and \r\n				perfect for us',0,NULL,'2019-12-03 18:54:38',1,NULL,NULL),(6,8,'The bouquet and the large arrangement in the Farmhouse entrance especially. I \r\n				just loved them all. The bouquet is here centre stage in our apartment and it \r\n				makes me drift into a bubble of memories of our special day each time I see it.',0,NULL,'2019-12-03 18:54:38',1,NULL,NULL),(7,1,'Thank you so much for making our wedding day so amazing with your beautiful \r\n				flowers! We were so impressed with the arrangements. It was everything we had imagined. \r\n				ou were so wonderful to work with. Thank you again.',0,NULL,'2019-12-03 18:54:38',1,NULL,NULL),(8,9,'It has been a pleasure ordering flowers from you for the past couple of years. \r\n				I will never forget the help you gave me in arranging flowers for each of my staff members \r\n				for my first day of school way back when.',0,NULL,'2019-12-03 18:54:38',1,NULL,NULL),(9,10,'Thank you very much for the beautiful arrangement and your good wishes. It was much appreciated at this busy but exciting time for us.\r\n',0,NULL,'2019-12-03 18:54:38',1,NULL,NULL),(10,7,'Thank you very much for the flowers that you gave to us to share with \r\n				out residents. They enjoyed receiving them as well as brightened their day. \r\n				We are grateful for your thoughtfulness and kindness. Thank you again.',0,NULL,'2019-12-03 18:54:38',1,NULL,NULL);
/*!40000 ALTER TABLE `testimonials` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transactions`
--

DROP TABLE IF EXISTS `transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `reference_number` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_transaction` (`user_id`),
  CONSTRAINT `fk_user_transaction` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transactions`
--

LOCK TABLES `transactions` WRITE;
/*!40000 ALTER TABLE `transactions` DISABLE KEYS */;
INSERT INTO `transactions` VALUES (1,10,'B11','2019-12-03 18:53:02',1),(2,8,'C12','2019-12-03 18:53:02',1),(3,5,'D45','2019-12-03 18:53:02',1),(4,4,'E56','2019-12-03 18:53:02',1),(5,2,'V43','2019-12-03 18:53:02',1),(6,1,'N92','2019-12-03 18:53:02',1),(7,6,'I82','2019-12-03 18:53:02',1),(8,9,'U76','2019-12-03 18:53:02',1),(9,3,'Q21','2019-12-03 18:53:02',1),(10,7,'S12','2019-12-03 18:53:02',1);
/*!40000 ALTER TABLE `transactions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` enum('admin','blogger','customer','support') NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Ravi','Patel','204 333-3823','patel.ravi04@gmail.com','mypass','admin',NULL,'2019-12-03 18:43:45',1,NULL,NULL),(2,'John','Doe','204 123-4567','john@doe.com','mypass','customer',NULL,'2019-12-03 18:43:45',1,NULL,NULL),(3,'Steve','George','431 223-4567','steve@george.com','mypass','admin',NULL,'2019-12-03 18:43:45',1,NULL,NULL),(4,'Leslie','William','204 867-4567','leslie@gmail.com','mypass','customer',NULL,'2019-12-03 18:43:45',1,NULL,NULL),(5,'Deep','Bhalotia','431 227-4587','bandar@gmail.com','mypass','customer',NULL,'2019-12-03 18:43:45',1,NULL,NULL),(6,'Sonia','Verma','204 190-4567','sonia@verma.com','mypass','customer',NULL,'2019-12-03 18:43:46',1,NULL,NULL),(7,'Lloyd','Gates','204 456-4567','lloyd@gates.com','mypass','admin',NULL,'2019-12-03 18:43:46',1,NULL,NULL),(8,'Sean','James','204 453-4567','sean@james.com','mypass','customer',NULL,'2019-12-03 18:43:46',1,NULL,NULL),(9,'Jeffrey','Way','204 768-4996','jeffrey@way.com','mypass','customer',NULL,'2019-12-03 18:43:46',1,NULL,NULL),(10,'Kelly','Carpick','431 123-8789','kelly@carpick.com','mypass','customer',NULL,'2019-12-03 18:43:46',1,NULL,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-04 13:05:03
