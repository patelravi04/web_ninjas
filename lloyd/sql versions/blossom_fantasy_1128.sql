-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Nov 28, 2019 at 11:58 PM
-- Server version: 5.7.26
-- PHP Version: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `blossom_fantasy`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `description` varchar(255) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`category_id`, `name`, `description`, `is_deleted`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 'Florida Prairie Clover', 'sapien placerat ante nulla justo aliquam quis turpis eget elit sodales scelerisque mauris sit amet eros suspendisse accumsan tortor quis turpis sed ante vivamus tortor duis', 1, '2019-11-28 16:11:45', 93, NULL, NULL),
(2, 'Capitaneja', 'eget congue eget semper rutrum nulla nunc purus phasellus in felis donec', 1, '2019-11-28 16:11:45', 70, NULL, NULL),
(3, 'Parks\' Beeblossom', 'eget elit sodales scelerisque mauris sit amet eros suspendisse accumsan tortor quis turpis sed ante vivamus tortor duis mattis egestas metus aenean fermentum donec ut mauris eget', 0, '2019-11-28 16:11:45', 71, NULL, NULL),
(4, 'Viscid Mallow', 'metus aenean fermentum donec ut mauris eget massa tempor convallis nulla neque', 1, '2019-11-28 16:11:45', 64, NULL, NULL),
(5, 'Limestone Bugheal', 'ipsum praesent blandit lacinia erat vestibulum sed magna at nunc commodo placerat praesent blandit nam nulla integer pede justo lacinia eget tincidunt eget tempus vel', 1, '2019-11-28 16:11:45', 96, NULL, NULL),
(6, 'Zeuxine', 'suspendisse potenti in eleifend quam a odio in hac habitasse platea dictumst maecenas ut massa quis', 0, '2019-11-28 16:11:45', 72, NULL, NULL),
(7, 'Sticky Leaf Arnica', 'pharetra magna ac consequat metus sapien ut nunc vestibulum ante ipsum primis in faucibus orci luctus et ultrices', 0, '2019-11-28 16:11:45', 85, NULL, NULL),
(8, 'Australian Amaranth', 'nunc commodo placerat praesent blandit nam nulla integer pede justo lacinia eget tincidunt eget tempus', 1, '2019-11-28 16:11:45', 8, NULL, NULL),
(9, 'Cusick\'s Bluegrass', 'aenean auctor gravida sem praesent id massa id nisl venenatis lacinia aenean sit amet justo morbi', 0, '2019-11-28 16:11:45', 83, NULL, NULL),
(10, 'Brassavola', 'porttitor pede justo eu massa donec dapibus duis at velit eu', 0, '2019-11-28 16:11:45', 91, NULL, NULL),
(11, 'House Range Primrose', 'pulvinar sed nisl nunc rhoncus dui vel sem sed sagittis', 1, '2019-11-28 16:11:45', 25, NULL, NULL),
(12, 'Slippery Burr', 'id sapien in sapien iaculis congue vivamus metus arcu adipiscing molestie hendrerit', 1, '2019-11-28 16:11:45', 97, NULL, NULL),
(13, 'Fivehorn Smotherweed', 'justo sit amet sapien dignissim vestibulum vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae nulla dapibus dolor vel est donec odio justo sollicitudin ut', 1, '2019-11-28 16:11:45', 82, NULL, NULL),
(14, 'Lowbush Blueberry', 'hac habitasse platea dictumst morbi vestibulum velit id pretium iaculis diam erat fermentum justo nec condimentum neque sapien placerat ante nulla justo aliquam quis turpis eget elit sodales', 0, '2019-11-28 16:11:45', 72, NULL, NULL),
(15, 'Tonto Basin Century Plant', 'mattis nibh ligula nec sem duis aliquam convallis nunc proin at', 0, '2019-11-28 16:11:45', 53, NULL, NULL),
(16, 'Christmas Mistletoe', 'venenatis turpis enim blandit mi in porttitor pede justo eu massa donec dapibus duis at', 1, '2019-11-28 16:11:45', 39, NULL, NULL),
(17, 'Havard\'s False Willow', 'eu est congue elementum in hac habitasse platea dictumst morbi vestibulum velit id pretium iaculis diam erat fermentum justo', 1, '2019-11-28 16:11:45', 11, NULL, NULL),
(18, 'Butterfly Milkweed', 'tincidunt in leo maecenas pulvinar lobortis est phasellus sit amet erat nulla tempus vivamus in felis eu sapien cursus vestibulum proin', 0, '2019-11-28 16:11:45', 3, NULL, NULL),
(19, 'Indusiella Moss', 'massa tempor convallis nulla neque libero convallis eget eleifend luctus ultricies eu nibh quisque id justo sit amet sapien dignissim vestibulum vestibulum ante', 1, '2019-11-28 16:11:45', 2, NULL, NULL),
(20, 'Dwarf Ginseng', 'vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae mauris viverra diam vitae quam suspendisse potenti nullam', 1, '2019-11-28 16:11:45', 14, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `delivery_charges`
--

DROP TABLE IF EXISTS `delivery_charges`;
CREATE TABLE IF NOT EXISTS `delivery_charges` (
  `delivery_charge_id` int(11) NOT NULL AUTO_INCREMENT,
  `delivery_cost` decimal(5,2) NOT NULL,
  `days_for_delivery` int(11) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`delivery_charge_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `discounts`
--

DROP TABLE IF EXISTS `discounts`;
CREATE TABLE IF NOT EXISTS `discounts` (
  `discount_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `discount_percentage` int(11) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`discount_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE IF NOT EXISTS `orders` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `transaction_id` int(11) NOT NULL,
  `ordered_date` date NOT NULL,
  `customer_address` text NOT NULL,
  `sub_total` decimal(6,2) NOT NULL,
  `gst` decimal(5,2) NOT NULL,
  `pst` decimal(5,2) NOT NULL,
  `total` decimal(6,2) NOT NULL,
  `status` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `order_line_items`
--

DROP TABLE IF EXISTS `order_line_items`;
CREATE TABLE IF NOT EXISTS `order_line_items` (
  `order_line_item_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `unit_price` decimal(5,2) NOT NULL,
  `quantity` int(11) NOT NULL,
  PRIMARY KEY (`order_line_item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL DEFAULT '1',
  `title` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `price` decimal(5,2) NOT NULL,
  `quantity` int(11) NOT NULL,
  `max_days_for_delivery` int(11) NOT NULL,
  `reorder_level` int(11) NOT NULL,
  `image` varchar(100) NOT NULL,
  `has_discount` tinyint(1) NOT NULL DEFAULT '0',
  `discounted_price` decimal(5,2) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`product_id`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `products`
--
SET FOREIGN_KEY_CHECKS=0;
INSERT INTO `products` (`product_id`, `category_id`, `title`, `description`, `price`, `quantity`, `max_days_for_delivery`, `reorder_level`, `image`, `has_discount`, `discounted_price`, `is_deleted`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 5, 'Georgia Rush', 'Juncus georgianus Coville', '25.21', 98, 2, 16, 'http://dummyimage.com/221x113.bmp/ff4444/ffffff', 1, '14.73', 1, '2019-11-28 17:33:02', 9, NULL, 16),
(2, 5, 'Andreaea Moss', 'Andreaea Hedw.', '45.14', 82, 5, 13, 'http://dummyimage.com/328x216.png/5fa2dd/ffffff', 1, '15.37', 1, '2019-11-28 17:33:02', 9, NULL, 8),
(3, 8, 'Carolina Bugbane', 'Trautvetteria caroliniensis (Walter) Vail var. caroliniensis', '58.12', 25, 2, 16, 'http://dummyimage.com/220x111.bmp/5fa2dd/ffffff', 1, '12.98', 0, '2019-11-28 17:33:02', 3, NULL, 16),
(4, 1, 'Low Sphagnum', 'Sphagnum compactum DC.', '79.79', 67, 2, 9, 'http://dummyimage.com/307x148.bmp/dddddd/000000', 0, '11.60', 0, '2019-11-28 17:33:02', 9, NULL, 19),
(5, 1, 'Cochineal Nopal Cactus', 'Opuntia cochenillifera (L.) Mill.', '7.99', 22, 2, 10, 'http://dummyimage.com/233x184.jpg/ff4444/ffffff', 0, '18.11', 0, '2019-11-28 17:33:02', 10, NULL, 2),
(6, 8, 'Waxyleaf Nightshade', 'Solanum glaucophyllum Desf.', '45.80', 73, 4, 7, 'http://dummyimage.com/211x154.png/ff4444/ffffff', 0, '2.47', 0, '2019-11-28 17:33:02', 10, NULL, 9),
(7, 10, 'Wax Mallow', 'Malvaviscus arboreus Dill. ex Cav. var. drummondii (Torr. & A. Gray) Schery', '80.73', 21, 1, 1, 'http://dummyimage.com/111x168.bmp/dddddd/000000', 0, '11.87', 0, '2019-11-28 17:33:02', 2, NULL, 23),
(8, 10, 'Nohoanu', 'Geranium hillebrandii Aedo & Mu?oz-Garm.', '86.96', 98, 5, 16, 'http://dummyimage.com/289x123.jpg/5fa2dd/ffffff', 0, '14.65', 1, '2019-11-28 17:33:02', 9, NULL, 19),
(9, 18, 'Jared\'s Pepperweed', 'Lepidium jaredii Brandegee', '43.06', 47, 1, 9, 'http://dummyimage.com/190x234.png/ff4444/ffffff', 0, '4.78', 0, '2019-11-28 17:33:03', 9, NULL, 5),
(10, 18, 'Low False Bindweed', 'Calystegia spithamaea (L.) Pursh ssp. purshiana (Wherry) Brummitt', '88.48', 43, 4, 19, 'http://dummyimage.com/186x203.png/cc0000/ffffff', 0, '13.78', 1, '2019-11-28 17:33:03', 8, NULL, 23),
(11, 16, 'Russian Peashrub', 'Caragana frutex (L.) K. Koch', '14.99', 66, 1, 6, 'http://dummyimage.com/216x239.jpg/cc0000/ffffff', 1, '8.00', 1, '2019-11-28 17:33:03', 6, NULL, 12),
(12, 1, 'Short-rayed Alkali Aster', 'Symphyotrichum frondosum (Nutt.) G.L. Nesom', '85.26', 44, 4, 2, 'http://dummyimage.com/231x123.png/ff4444/ffffff', 0, '8.14', 1, '2019-11-28 17:33:03', 5, NULL, 15),
(13, 16, 'Squirreltail', 'Elymus elymoides (Raf.) Swezey', '85.09', 79, 4, 2, 'http://dummyimage.com/180x155.jpg/cc0000/ffffff', 0, '9.89', 0, '2019-11-28 17:33:03', 1, NULL, 21),
(14, 9, 'Roseflower Stonecrop', 'Sedum laxum (Britton) A. Berger ssp. flavidum Denton', '44.51', 9, 5, 11, 'http://dummyimage.com/114x212.png/5fa2dd/ffffff', 1, '2.61', 0, '2019-11-28 17:33:03', 10, NULL, 5),
(15, 4, 'Nevada Rockdaisy', 'Perityle megalocephala (S. Watson) J.F. Macbr.', '34.33', 26, 5, 8, 'http://dummyimage.com/178x218.jpg/ff4444/ffffff', 1, '6.23', 1, '2019-11-28 17:33:03', 2, NULL, 6),
(16, 4, 'Bloodleaf', 'Iresine P. Br.', '53.13', 13, 3, 2, 'http://dummyimage.com/250x135.jpg/cc0000/ffffff', 0, '19.19', 1, '2019-11-28 17:33:03', 4, NULL, 20),
(17, 18, 'Hayfield Tarweed', 'Hemizonia congesta DC. ssp. congesta', '86.77', 7, 2, 7, 'http://dummyimage.com/136x232.png/dddddd/000000', 0, '14.06', 0, '2019-11-28 17:33:03', 7, NULL, 1),
(18, 9, 'Orthotrichum Moss', 'Orthotrichum sordidum Sull. & Lesq.', '12.34', 90, 4, 6, 'http://dummyimage.com/263x203.png/dddddd/000000', 0, '9.97', 1, '2019-11-28 17:33:03', 5, NULL, 11),
(19, 20, 'Douglas\' Tickseed', 'Coreopsis douglasii (DC.) H.M. Hall', '87.24', 23, 3, 19, 'http://dummyimage.com/136x221.jpg/5fa2dd/ffffff', 0, '16.99', 0, '2019-11-28 17:33:03', 4, NULL, 15),
(20, 20, 'Rosilla', 'Helenium puberulum DC.', '16.83', 47, 5, 13, 'http://dummyimage.com/382x247.bmp/cc0000/ffffff', 0, '16.60', 0, '2019-11-28 17:33:03', 5, NULL, 24),
(21, 13, 'Myrrh', 'Commiphora Jacq.', '18.81', 37, 5, 5, 'http://dummyimage.com/361x210.jpg/ff4444/ffffff', 1, '5.16', 0, '2019-11-28 17:33:03', 9, NULL, 13),
(22, 13, 'Dot Lichen', 'Arthonia caudata Willey', '37.09', 10, 1, 20, 'http://dummyimage.com/267x115.png/cc0000/ffffff', 1, '12.53', 0, '2019-11-28 17:33:03', 3, NULL, 22),
(23, 17, 'Autumn Crocus', 'Colchicum autumnale L.', '12.57', 37, 1, 13, 'http://dummyimage.com/390x177.jpg/cc0000/ffffff', 1, '6.96', 1, '2019-11-28 17:33:03', 2, NULL, 3),
(24, 11, 'Elliott\'s Bentgrass', 'Agrostis elliottiana Schult.', '46.37', 45, 3, 1, 'http://dummyimage.com/343x244.png/dddddd/000000', 0, '1.06', 1, '2019-11-28 17:33:03', 5, NULL, 24),
(25, 19, 'Pale Callicostella Moss', 'Callicostella pallida (Hornsch.) ?ngstr.', '76.04', 63, 5, 19, 'http://dummyimage.com/240x100.jpg/dddddd/000000', 1, '17.28', 0, '2019-11-28 17:33:03', 6, NULL, 14),
(26, 19, 'Aerva', 'Aerva Forssk.', '60.39', 54, 4, 19, 'http://dummyimage.com/374x128.bmp/dddddd/000000', 1, '2.60', 1, '2019-11-28 17:33:03', 4, NULL, 22),
(27, 14, 'Montane Peperomia', 'Peperomia distachya (L.) A. Dietr.', '68.25', 7, 4, 13, 'http://dummyimage.com/240x147.bmp/cc0000/ffffff', 1, '12.87', 1, '2019-11-28 17:33:03', 1, NULL, 20),
(28, 5, 'Mono Clover', 'Trifolium andersonii A. Gray ssp. monoense (Greene) J.M. Gillett', '54.61', 13, 1, 12, 'http://dummyimage.com/178x172.jpg/dddddd/000000', 0, '7.51', 0, '2019-11-28 17:33:03', 1, NULL, 9),
(29, 14, 'Woollyjoint Pricklypear', 'Opuntia tomentosa Salm-Dyck', '96.15', 71, 4, 7, 'http://dummyimage.com/328x232.png/dddddd/000000', 0, '12.01', 0, '2019-11-28 17:33:03', 7, NULL, 24),
(30, 3, 'Pacific Lovegrass', 'Eragrostis deflexa Hitchc.', '81.14', 86, 3, 8, 'http://dummyimage.com/163x187.bmp/cc0000/ffffff', 0, '8.19', 0, '2019-11-28 17:33:03', 3, NULL, 22),
(31, 3, 'Big Western Bittercress', 'Cardamine occidentalis (S. Watson ex B.L. Rob.) Howell', '39.47', 77, 4, 2, 'http://dummyimage.com/130x249.jpg/5fa2dd/ffffff', 0, '9.47', 0, '2019-11-28 17:33:03', 2, NULL, 24),
(32, 12, 'Camphor', 'Dryobalanops C.F. Gaertn.', '67.04', 47, 3, 4, 'http://dummyimage.com/195x105.png/ff4444/ffffff', 0, '15.63', 1, '2019-11-28 17:33:03', 4, NULL, 19),
(33, 12, 'Hawai\'i Brushholly', 'Xylosma hawaiiensis Seem.', '65.58', 44, 1, 11, 'http://dummyimage.com/177x213.jpg/dddddd/000000', 0, '18.35', 1, '2019-11-28 17:33:03', 4, NULL, 13),
(34, 7, 'Showy Phlox', 'Phlox speciosa Pursh ssp. occidentalis (Durand ex Torr.) Wherry', '52.91', 75, 2, 2, 'http://dummyimage.com/191x120.jpg/cc0000/ffffff', 1, '8.18', 0, '2019-11-28 17:33:03', 8, NULL, 15),
(35, 7, 'Forest Tetramolopium', 'Tetramolopium consanguineum (A. Gray) Hillebr. ssp. leptophyllum (Sherff) Lowrey var. leptophyllum Sherff', '14.94', 45, 5, 10, 'http://dummyimage.com/307x203.png/dddddd/000000', 0, '9.08', 0, '2019-11-28 17:33:03', 1, NULL, 16),
(36, 15, 'Disc Lichen', 'Buellia bolacina Tuck.', '7.75', 45, 4, 20, 'http://dummyimage.com/349x206.bmp/dddddd/000000', 1, '14.69', 0, '2019-11-28 17:33:03', 4, NULL, 10),
(37, 4, 'Smooth Penstemon', 'Penstemon subglaber Rydb.', '43.25', 94, 3, 15, 'http://dummyimage.com/346x220.bmp/ff4444/ffffff', 1, '12.08', 1, '2019-11-28 17:33:03', 2, NULL, 8),
(38, 7, 'Drummond\'s Hedgenettle', 'Stachys drummondii Benth.', '93.17', 6, 3, 16, 'http://dummyimage.com/223x135.jpg/dddddd/000000', 0, '7.50', 0, '2019-11-28 17:33:03', 3, NULL, 10),
(39, 15, 'Columbian Lewisia', 'Lewisia columbiana (Howell ex A. Gray) B.L. Rob. var. columbiana', '1.33', 37, 5, 10, 'http://dummyimage.com/173x218.bmp/ff4444/ffffff', 0, '5.60', 1, '2019-11-28 17:33:03', 7, NULL, 3),
(40, 15, 'Mountain Ash', 'Sorbus L.', '80.48', 65, 1, 16, 'http://dummyimage.com/191x138.png/dddddd/000000', 1, '4.20', 1, '2019-11-28 17:33:03', 6, NULL, 6),
(41, 4, 'Cobralily', 'Arisaema speciosum (Wall.) Mart.', '44.24', 8, 5, 16, 'http://dummyimage.com/132x202.png/dddddd/000000', 0, '3.95', 0, '2019-11-28 17:33:03', 1, NULL, 1),
(42, 4, 'Trypelthelium Lichen', 'Trypethelium aeneum (Eschw.) Zahlbr.', '24.61', 28, 2, 3, 'http://dummyimage.com/221x227.png/cc0000/ffffff', 0, '15.91', 0, '2019-11-28 17:33:03', 6, NULL, 12),
(43, 6, 'Clammy Cherry', 'Cordia obliqua Willd.', '63.41', 47, 1, 8, 'http://dummyimage.com/327x178.png/5fa2dd/ffffff', 0, '9.85', 0, '2019-11-28 17:33:03', 8, NULL, 17),
(44, 6, 'Jumping Cholla', 'Cylindropuntia fulgida (Engelm.) F.M. Knuth', '48.41', 6, 2, 3, 'http://dummyimage.com/178x148.bmp/cc0000/ffffff', 0, '16.08', 1, '2019-11-28 17:33:03', 1, NULL, 20),
(45, 6, 'Alyssumleaf Phlox', 'Phlox alyssifolia Greene ssp. abdita (A. Nelson) Wherry', '48.59', 44, 2, 17, 'http://dummyimage.com/132x241.png/dddddd/000000', 0, '7.74', 1, '2019-11-28 17:33:03', 1, NULL, 7),
(46, 2, 'Erect Brome', 'Bromus erectus Huds.', '31.47', 78, 1, 11, 'http://dummyimage.com/220x160.bmp/dddddd/000000', 1, '16.11', 1, '2019-11-28 17:33:03', 4, NULL, 22),
(47, 6, 'Veiny Monardella', 'Monardella douglasii Benth. ssp. venosa (Torr.) Jokerst', '74.81', 74, 1, 5, 'http://dummyimage.com/393x148.jpg/dddddd/000000', 0, '6.01', 1, '2019-11-28 17:33:03', 10, NULL, 12),
(48, 5, 'Utah Columbine', 'Aquilegia scopulorum Tidestr.', '70.47', 94, 3, 17, 'http://dummyimage.com/232x162.jpg/dddddd/000000', 0, '16.49', 1, '2019-11-28 17:33:03', 4, NULL, 11),
(49, 3, 'Star Sedge', 'Carex echinata Murray ssp. phyllomanica (W. Boott) Reznicek', '13.68', 41, 3, 15, 'http://dummyimage.com/264x170.bmp/cc0000/ffffff', 0, '1.06', 0, '2019-11-28 17:33:03', 2, NULL, 5),
(50, 2, 'Rubber Rabbitbrush', 'Ericameria nauseosa (Pall. ex Pursh) G.L. Nesom & Baird ssp. consimilis (Greene) G.L. Nesom & Baird var. nitida (L.C. Anderson) G.L. Nesom & Baird', '76.97', 100, 5, 7, 'http://dummyimage.com/307x110.png/5fa2dd/ffffff', 1, '2.25', 0, '2019-11-28 17:33:03', 3, NULL, 18);
SET FOREIGN_KEY_CHECKS=1;
-- --------------------------------------------------------

--
-- Table structure for table `tax`
--

DROP TABLE IF EXISTS `tax`;
CREATE TABLE IF NOT EXISTS `tax` (
  `tax_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(3) NOT NULL,
  `description` varchar(100) NOT NULL,
  `tax_percentage` int(11) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`tax_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

DROP TABLE IF EXISTS `testimonials`;
CREATE TABLE IF NOT EXISTS `testimonials` (
  `testimonial_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`testimonial_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

DROP TABLE IF EXISTS `transactions`;
CREATE TABLE IF NOT EXISTS `transactions` (
  `transaction_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `reference_number` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`transaction_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `address` varchar(100) NOT NULL,
  `city` varchar(50) NOT NULL,
  `province` varchar(20) NOT NULL,
  `postal_code` char(7) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` enum('admin','blogger','customer','support') NOT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`category_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
