DROP TABLE IF EXISTS `abouts`;
CREATE TABLE IF NOT EXISTS `abouts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `timeline` text CHARACTER SET utf8mb4 NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;


INSERT INTO `abouts` (`id`, `image`, `timeline`, `deleted_at`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 'vw-bus-family-photos-9.jpg','Summer of 1990 the Blossfan family went on a road trip to western Canada. They passed by a flower farm and bought some flowers to give to their friends living along the path of their road trip. When they reached a town, the family stops to eat, later some people approached them, asking if they could buy some of the flowers. The family ended up selling all of the flowers that day. Coming back from the road trip, they did the same thing, bought some flowers from the farm and sell it. The same thing happened; all the flowers were sold.', NULL, '2019-12-09 23:54:04', 1, NULL, NULL)
