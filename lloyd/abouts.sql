-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Dec 12, 2019 at 07:36 PM
-- Server version: 5.7.26
-- PHP Version: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `blossom_fantasy`
--

-- --------------------------------------------------------

--
-- Table structure for table `abouts`
--

DROP TABLE IF EXISTS `abouts`;
CREATE TABLE IF NOT EXISTS `abouts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `timeline` text CHARACTER SET utf8mb4 NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `abouts`
--

INSERT INTO `abouts` (`id`, `image`, `timeline`, `deleted_at`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 'veITMC3TyZQfc2yd8k8tuqWcC3kXtRpAjW62fCrd.jpeg', 'Summer of 1990 the Blossfan family went on a road trip to Western Canada. They passed by a flower farm and bought some flowers to give to their friends living along the path of their road trip. When they reached a town, the family stops to eat, later some people approached them, asking if they could buy some of the flowers. The family ended up selling all of the flowers that day. Coming back from the road trip, they did the same thing, bought some flowers from the farm and sell it. The same thing happened; all the flowers were sold.', NULL, '2019-12-09 23:54:04', 1, '2019-12-12 18:56:57', 1),
(12, '3JmrqWu9EZifspL1yBvzfBQ6qazDhuPJ9YTEjoY7.jpeg', 'Since that road trip happened, Mr. and Mrs. Blossfan \r\n              decided to dig deeper into the flower business. They first started \r\n              doing it on the weekends or holidays. For a few months, they sold flowers \r\n              using their van. They were amazed because the business was excellent, and they are \r\n              happy doing it. At first, they were moving from place to place each week around the city; \r\n              they did it also to start scouting for the best place where to sell flowers in the town. \r\n              In the same year they finally settled in the best place possible and the Blossom Fantasy \r\n              flower company was born.', NULL, '2019-12-11 21:47:45', 1, '2019-12-11 22:35:30', 1),
(13, 'images/about/thumbs/21pIGX6NCU5WROuJpLX8SbVinsNhELedtcvQH7VL.jpeg', 'Since that road trip happened, Mr. and Mrs. Blossfan \r\n              decided to dig deeper into the flower business. They first started \r\n              doing it on the weekends or holidays. For a few months, they sold flowers \r\n              using their van. They were amazed because the business was excellent, and they are \r\n              happy doing it. At first, they were moving from place to place each week around the city; \r\n              they did it also to start scouting for the best place where to sell flowers in the town. \r\n              In the same year they finally settled in the best place possible and the blossom fantasy \r\n              flower company was born.', '2019-12-11 22:18:04', '2019-12-11 21:49:10', 1, '2019-12-11 22:18:04', NULL),
(14, '7jNI1iTj1WZdMJlMQKoT13957iUy0648qSGHCuDx.jpeg', 'In 1995, The Blossfan couple decided to deliver the flowers at the doorsteps of their customers. At first, they had limited the radius to a maximum of ten kilometers from their store because at that time they are only using a bicycle to deliver the flowers. Robust business and high demand prompted the company to start contracting out deliveries to Greater Winnipeg Area.', NULL, '2019-12-11 21:54:53', 1, '2019-12-12 19:30:52', 1),
(17, 'DyoUJZYtd8ee5Xty98MvzpTiPsLBVN0SuBE3r6Ix.jpeg', 'In 2015, Blossom fantasy went Canada wide. It expanded to 13 branches across Canada, with 500 employees all over Canada working in Blossom fantasy flower farms and flower shops.', '2019-12-12 16:49:43', '2019-12-12 16:46:39', 1, '2019-12-12 16:49:43', NULL),
(15, 'ZaYz2PmtJX51fPUD4KVXMe9BPwSeIR7VWuHvWo0E.jpeg', 'In 2000, To ensure the supply of fresh flowers, Blossom\r\n              Fantasy invested in flower farms. The first farm is 1,000 hectares of flowers situated outside the perimeter of Winnipeg, equipped with state-of-the-art technology that could withstand even the harshest Canadian winter.\r\n              This business decision has separated Blossom Fantasy from its competitors\r\n              capturing a dominant market share of 90% of the entire Greater Winnipeg Area.', NULL, '2019-12-11 21:57:52', 1, '2019-12-12 19:00:02', 1),
(16, 'wuUllMwCU1zZv8uCHg5GCDFNORl1juU3nz2MvG5d.jpeg', 'In 2005, Blossom Fantasy expanded its operations to Saskatchewan. In the same year, the first Blossom Fantasy Website was launched. Initially it was an static website where people can see all the products and make a phone call to order flowers.', '2019-12-12 16:49:38', '2019-12-12 16:45:23', 1, '2019-12-12 16:49:38', NULL),
(18, 'lKzQPckwBOUdnPRWiUrFTnLg1ej1Rd2xwh8MU6ss.jpeg', 'In 2020, Blossom fantasy new website will be launch. Entire front office operations integrated into one ecommerce site. Its security have been enormously improved.', '2019-12-12 16:49:45', '2019-12-12 16:47:24', 1, '2019-12-12 16:49:45', NULL),
(19, 'BPs94dh3YTd4AkZgXUOJo3DcBHrn79nKzVGkukor.jpeg', 'In 2005, Blossom Fantasy expanded its operations to Saskatchewan. In the same year, the first Blossom Fantasy Website was launched. Initially, it was a static website where people can see all the products and make a phone call to order flowers. Since launching sales improved by 10% on a yearly basis.', NULL, '2019-12-12 16:51:29', 1, '2019-12-12 19:32:10', 1),
(20, 'nYRMxgcNTzjUUeA9I0uuH4V53GE3mTZSh3PwgPAl.jpeg', 'In 2015, Blossom fantasy went Canada wide. It expanded to 13 branches across Canada, with 500 employees all over Canada working in Blossom Fantasy flower farms and flower shops.', NULL, '2019-12-12 16:54:10', 1, '2019-12-12 16:54:10', NULL),
(21, 'ttdEsFSGI7RxMGY3d4aQmLHoXLhmdSoIf2TM3wzB.jpeg', 'In 2020, Blossom Fantasy the new website will be launch. The entire front office and back operations integrated into one eCommerce site. It adheres to the principles of usability, it is search engine optimized, it is lock-down secure, it accepts credit card payment. It has all the capabilities of the latest eCommerce website trends and technologies.', NULL, '2019-12-12 16:55:16', 1, '2019-12-12 19:25:55', 1);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
