--
-- Table structure for table `faqs`
--

DROP TABLE IF EXISTS `faqs`;
CREATE TABLE IF NOT EXISTS `faqs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `questions` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `answers` text CHARACTER SET utf8mb4 NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `faqs`
--

INSERT INTO `faqs` (`id`, `questions`, `answers`, `deleted_at`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 'How will I know if my flowers (or gifts) have been delivered?', 'A: This is the most common question we receive. When in need of confirmation of delivery, make your request in the Additional Information section on the secure order page at checkout. If you have ordered (and did not request confirmation) contact us at service@blossom_fantasy.ca or call us at 1-888-705-9999. We have an outstanding track record of delivering flowers on-time throughout Canada and across the world. Read more information about delivery here.', NULL, '2019-12-09 23:54:04', 1, NULL, NULL),
(2, 'Can you deliver to hospitals?', 'A: Absolutely! Please be sure to provide the legal first and last name so our affiliate can find the recipient. A maiden name may be required in some areas depending on the hospital\'s policy. Including Additional Information such as the room number or section of the hospital to find the recipient will help ensure a smooth delivery. Some situations may require the delivery to be left at the front desk of the hospital where an employee or volunteer at the hospital will ensure the gift reaches the recipient.', NULL, '2019-12-10 00:01:18', 1, NULL, NULL),
(3, 'Do you accept Visa Debit?', 'A: Our internal credit card processing system will not be accept Visa Debit cards, however, PayPal can be used to process Visa Debit payments. When checking out select the Debit/PayPal as your payment method. There is no additional cost to you for this service.', NULL, '2019-12-10 00:09:22', 1, '2019-12-11 05:08:01', 14),
(4, 'Do you charge tax?', 'A: Due to Canadian law, taxes are required for all orders sent within Canada. Outside of Canada, USA and International prices are all inclusive.', NULL, '2019-12-10 00:09:22', 1, NULL, NULL),
(5, 'How do I get a copy of my receipt?', 'A: Once your order is successfully submitted online, you will immediately receive a receipt (with order number) on your web browser screen. We suggest printing and retaining this receipt for your records. Our systems also send a 2nd copy of your receipt to the e-mail address that you used when placing your order. When we receive feedback that an e-mail receipt was not received, it is often found sitting in a customer’s junk mail folder. Receipts are emailed from service@flowers.ca. Please ensure your e-mail program or service is configured to receive e-mail from this address.', NULL, '2019-12-10 00:09:22', 1, NULL, NULL),
(6, 'I placed an order but forgot to enter the promotional code. Please Help!', 'A: Whoopsie! Have no fear, these things happen and we can certainly take care of this for you. If you\'ve placed an order and didn\'t get an opportunity to redeem an active promotion or you were made aware of a promotion after ordering with us, please reach out to us either by phone or email. Be sure to have your order number ready and a member of our customer service team would be more than happy to retroactively apply the promotional discount as a partial refund to your credit card.', NULL, '2019-12-10 00:09:22', 1, NULL, NULL),
(7, 'What information should I have ready before ordering?', 'A: We endeavour to ensure that ordering with us is an easy and fun process! Our recommendation is to order directly online, as you can go at your own pace and ensure firsthand that all the order information is entered correctly and to your satisfaction. If you prefer to speak to a human, we have great news! We have a team of dedicated humans! Real people who would be happy to talk to you directly and assist you with placing your order. Before ordering, you\'ll need to have the recipient name, a complete address and phone number ready for where you\'re sending your order to. It is also strongly encouraged to decide beforehand what you\'d like to write on the card message. Last but not least, we also require the billing information linked to the payment method you will be providing.', NULL, '2019-12-10 00:09:22', 1, NULL, NULL),
(8, 'Can I send an order anonymously?', 'A: Yes, you can send an order anonymously, and also type whatever you like for the card message. Please be advised that we are obliged, if requested by the recipient, to give out the sender\'s name. This is a common floral industry practice. In general, we advise against sending anonymous gifts, as these often lead to some distress for the recipient. In our experience, the recipient will almost always prefer to know who has sent a gift at the exact time it is received.<br />Before you decide to send an order anonymously, please also consider whether or not the recipient will consider this criminal harrassment, or \"stalking\". For more information on the law regarding criminal harrassment, please read here.', NULL, '2019-12-10 00:09:22', 1, NULL, NULL),
(9, 'I accidentally placed multiple orders!', 'A: Have no fear! Please let us know about the duplicate order(s) as soon as possible and we will cancel and refund the duplicate order immediately. We recommend informing us immediately. We always make our best effort, however, if the order has already been delivered or is in route, we cannot guarantee that we will be able to successfully cancel and refund your order.', NULL, '2019-12-10 00:09:22', 1, NULL, NULL);
