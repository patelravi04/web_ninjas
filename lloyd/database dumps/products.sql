-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Dec 02, 2019 at 10:11 PM
-- Server version: 5.7.26
-- PHP Version: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `blossom_fantasy`
--

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `price` decimal(5,2) NOT NULL,
  `quantity` int(11) NOT NULL,
  `max_days_for_delivery` int(11) NOT NULL,
  `reorder_level` int(11) NOT NULL,
  `image` varchar(100) NOT NULL,
  `has_discount` tinyint(1) NOT NULL DEFAULT '0',
  `discounted_price` decimal(5,2) DEFAULT '0.00',
  `discount_id` int(11) DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`product_id`),
  KEY `fk_product_discount` (`discount_id`)
) ENGINE=InnoDB AUTO_INCREMENT=113 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `products`
--
SET FOREIGN_KEY_CHECKS = 0;
INSERT INTO `products` (`product_id`, `title`, `description`, `price`, `quantity`, `max_days_for_delivery`, `reorder_level`, `image`, `has_discount`, `discounted_price`, `discount_id`, `is_deleted`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(57, 'Pioneer Violet', 'aenean sit amet justo morbi ut odio cras mi pede', '27.69', 109, 2, 7, 'images\\flowers\\alyssum.jpe', 0, '4.89', 9, 1, '2019-12-02 15:06:41', 8, NULL, 4),
(58, 'Ghost Flower', 'pharetra magna vestibulum aliquet ultrices erat tortor sollicitudin mi sit', '34.27', 165, 5, 9, 'images\\flowers\\Amc-DvMayDelaware.jpe', 1, '1.89', 9, 1, '2019-12-02 15:06:41', 10, NULL, 10),
(59, 'Pineland Daisy', 'ut suscipit a feugiat et eros vestibulum ac est lacinia nisi venenatis tristique fusce congue diam id ornare', '49.65', 253, 4, 6, 'images\\flowers\\beeBalm.jpg', 1, '1.76', 2, 0, '2019-12-02 15:06:41', 7, NULL, 7),
(60, 'Maguire\'s Penstemon', 'nulla pede ullamcorper augue a suscipit nulla elit ac nulla sed vel enim sit amet nunc viverra', '12.13', 370, 1, 5, 'images\\flowers\\bittersweet.jpe', 0, '1.03', 2, 1, '2019-12-02 15:06:41', 7, NULL, 2),
(61, 'False Goldeneye', 'vivamus in felis eu sapien cursus vestibulum proin eu mi nulla ac', '65.32', 276, 2, 7, 'images\\flowers\\bittersweett.jpe', 1, '4.98', 10, 1, '2019-12-02 15:06:41', 9, NULL, 9),
(62, 'Taxiphyllum Moss', 'magnis dis parturient montes nascetur ridiculus mus vivamus vestibulum sagittis sapien cum sociis natoque penatibus et magnis dis', '6.65', 167, 5, 9, 'images\\flowers\\blackEyedSusanFlower.jpe', 0, '2.80', 7, 1, '2019-12-02 15:06:41', 10, NULL, 7),
(63, 'Thrift Seapink', 'consequat lectus in est risus auctor sed tristique in tempus sit amet', '82.26', 837, 2, 9, 'images\\flowers\\blueFlagIris.jpg', 1, '1.72', 2, 1, '2019-12-02 15:06:41', 6, NULL, 2),
(64, 'Szatala\'s Crabseye Lichen', 'gravida sem praesent id massa id nisl venenatis lacinia aenean sit amet justo morbi ut odio cras', '84.31', 445, 3, 7, 'images\\flowers\\butterflyWeed.jpe', 0, '1.73', 2, 1, '2019-12-02 15:06:41', 3, NULL, 3),
(65, 'Foothill Rush', 'nulla facilisi cras non velit nec nisi vulputate nonummy maecenas', '60.35', 197, 5, 7, 'images\\flowers\\camprisRadicum.jpe', 0, '4.57', 8, 0, '2019-12-02 15:06:41', 6, NULL, 5),
(66, 'Skyttella Lichen', 'in hac habitasse platea dictumst morbi vestibulum velit id pretium iaculis diam erat fermentum justo nec condimentum neque sapien placerat', '59.60', 399, 5, 10, 'images\\flowers\\cercisCanadensis.jpg', 0, '3.05', 8, 1, '2019-12-02 15:06:41', 5, NULL, 3),
(67, 'Butterweed', 'cum sociis natoque penatibus et magnis dis parturient montes nascetur ridiculus mus etiam vel augue vestibulum rutrum rutrum neque aenean', '71.45', 140, 3, 6, 'images\\flowers\\Cornation.jpe', 0, '3.12', 1, 0, '2019-12-02 15:06:41', 1, NULL, 4),
(68, 'Baker\'s Goldfields', 'morbi a ipsum integer a nibh in quis justo maecenas rhoncus aliquam', '34.66', 521, 2, 7, 'images\\flowers\\dagwood.jpe', 1, '3.93', 1, 1, '2019-12-02 15:06:41', 7, NULL, 6),
(69, 'Candle Snuffer Moss', 'ultrices posuere cubilia curae duis faucibus accumsan odio curabitur convallis duis consequat dui nec nisi volutpat eleifend', '38.93', 846, 4, 10, 'images\\flowers\\Dianthus.jpe', 0, '3.63', 5, 0, '2019-12-02 15:06:41', 5, NULL, 6),
(70, 'Lindheimer\'s Ticktrefoil', 'condimentum curabitur in libero ut massa volutpat convallis morbi odio odio elementum eu interdum eu tincidunt in leo maecenas', '20.17', 658, 2, 10, 'images\\flowers\\downyLobelia.jpe', 1, '1.75', 4, 1, '2019-12-02 15:06:41', 9, NULL, 7),
(71, 'Mountain Chickweed', 'nam dui proin leo odio porttitor id consequat in consequat ut nulla sed accumsan', '81.32', 37, 1, 5, 'images\\flowers\\DustyZenobia.jpg', 1, '3.93', 5, 1, '2019-12-02 15:06:41', 5, NULL, 1),
(72, 'Rio Grande Bugheal', 'nisl duis bibendum felis sed interdum venenatis turpis enim blandit mi in porttitor pede', '4.05', 208, 3, 8, 'images\\flowers\\ekluntnaFlats.jpg', 1, '2.64', 5, 0, '2019-12-02 15:06:41', 5, NULL, 3),
(73, 'Sixweeks Fescue', 'quisque porta volutpat erat quisque erat eros viverra eget congue eget semper rutrum nulla nunc purus phasellus in felis', '57.99', 890, 4, 8, 'images\\flowers\\FES.jpe', 1, '1.41', 9, 1, '2019-12-02 15:06:41', 5, NULL, 10),
(74, 'Common Woolly Sunflower', 'nulla integer pede justo lacinia eget tincidunt eget tempus vel pede morbi porttitor lorem id ligula suspendisse ornare', '90.87', 146, 4, 5, 'images\\flowers\\globalgalleryMexican.jpe', 0, '1.33', 2, 0, '2019-12-02 15:06:41', 5, NULL, 1),
(75, 'Jones\' Nailwort', 'magna vestibulum aliquet ultrices erat tortor sollicitudin mi sit amet lobortis sapien sapien non mi integer', '58.21', 596, 4, 9, 'images\\flowers\\growingCelosiaFlowers.jpg', 1, '2.82', 5, 0, '2019-12-02 15:06:41', 1, NULL, 6),
(76, 'Littleleaf Brickellbush', 'at velit vivamus vel nulla eget eros elementum pellentesque quisque', '79.70', 79, 1, 5, 'images\\flowers\\highRes.jpe', 1, '2.56', 4, 0, '2019-12-02 15:06:41', 10, NULL, 7),
(77, 'Rosy Palafox', 'id mauris vulputate elementum nullam varius nulla facilisi cras non', '37.32', 451, 4, 6, 'images\\flowers\\indianBlanket.jpe', 0, '3.14', 4, 1, '2019-12-02 15:06:41', 7, NULL, 9),
(78, 'Fuzzy Mock Orange', 'duis ac nibh fusce lacus purus aliquet at feugiat non pretium', '76.01', 306, 3, 5, 'images\\flowers\\Iris.jpe', 0, '3.92', 4, 0, '2019-12-02 15:06:41', 1, NULL, 10),
(79, 'Inland Muilla', 'lobortis sapien sapien non mi integer ac neque duis bibendum morbi non quam nec dui luctus rutrum nulla tellus', '25.69', 586, 3, 10, 'images\\flowers\\irisCristata.jpg', 1, '1.63', 6, 0, '2019-12-02 15:06:41', 8, NULL, 6),
(80, 'Idaho Kittentails', 'pede justo eu massa donec dapibus duis at velit eu est', '40.10', 14, 3, 8, 'images\\flowers\\lantana.jpg', 0, '3.74', 2, 0, '2019-12-02 15:06:41', 5, NULL, 10),
(81, 'Watercrown Grass', 'scelerisque mauris sit amet eros suspendisse accumsan tortor quis turpis sed ante vivamus tortor duis mattis egestas metus', '91.42', 51, 3, 8, 'images\\flowers\\Lupiness.jpe', 1, '1.83', 4, 1, '2019-12-02 15:06:41', 6, NULL, 5),
(82, 'Howell\'s Fawnlily', 'erat nulla tempus vivamus in felis eu sapien cursus vestibulum proin eu mi', '67.23', 28, 5, 8, 'images\\flowers\\mayDelaware.jpe', 1, '4.83', 6, 1, '2019-12-02 15:06:41', 1, NULL, 9),
(83, 'Newberry\'s Tickseed', 'penatibus et magnis dis parturient montes nascetur ridiculus mus vivamus vestibulum', '68.63', 384, 1, 5, 'images\\flowers\\Mayflower.jpe', 1, '2.92', 9, 1, '2019-12-02 15:06:41', 8, NULL, 2),
(84, 'Vitt Tube Lichen', 'sit amet eros suspendisse accumsan tortor quis turpis sed ante vivamus', '69.55', 311, 2, 6, 'images\\flowers\\mexican.jpe', 0, '2.62', 5, 1, '2019-12-02 15:06:41', 7, NULL, 8),
(85, 'Leptarrhena', 'convallis nunc proin at turpis a pede posuere nonummy integer non velit donec diam', '83.52', 166, 1, 8, 'images\\flowers\\mountainLaurel.jpe', 0, '3.49', 10, 1, '2019-12-02 15:06:41', 1, NULL, 7),
(86, 'Herbertia', 'condimentum neque sapien placerat ante nulla justo aliquam quis turpis eget elit sodales scelerisque mauris sit amet eros suspendisse', '3.33', 963, 2, 7, 'images\\flowers\\mullein-verbascum-thapsus.jpg', 0, '2.87', 1, 0, '2019-12-02 15:06:41', 3, NULL, 3),
(87, 'Asian Taro', 'donec ut dolor morbi vel lectus in quam fringilla rhoncus', '75.39', 548, 4, 9, 'images\\flowers\\muskMallow.jpe', 0, '2.55', 7, 0, '2019-12-02 15:06:41', 2, NULL, 8),
(88, 'Bristlyleaf Rockcress', 'sapien placerat ante nulla justo aliquam quis turpis eget elit sodales scelerisque mauris sit', '24.64', 367, 5, 8, 'images\\flowers\\NiftyPlant.jpe', 0, '4.54', 4, 0, '2019-12-02 15:06:41', 2, NULL, 4),
(89, 'Beyrich\'s Hooded Orchid', 'sapien arcu sed augue aliquam erat volutpat in congue etiam justo etiam', '88.78', 729, 3, 6, 'images\\flowers\\nineOfcups.jpe', 1, '4.21', 7, 0, '2019-12-02 15:06:41', 10, NULL, 5),
(90, 'Mildred\'s Clarkia', 'donec diam neque vestibulum eget vulputate ut ultrices vel augue vestibulum ante ipsum primis in faucibus orci luctus et', '53.79', 199, 3, 6, 'images\\flowers\\northAmericanPitcher.jpe', 1, '2.92', 7, 0, '2019-12-02 15:06:41', 4, NULL, 9),
(91, 'San Bernardino Vervain', 'consectetuer eget rutrum at lorem integer tincidunt ante vel ipsum praesent blandit lacinia erat vestibulum sed magna at nunc', '64.28', 471, 3, 10, 'images\\flowers\\OshoOrchard.jpe', 0, '4.17', 4, 1, '2019-12-02 15:06:41', 2, NULL, 10),
(92, 'Mancos Shale Packera', 'vel pede morbi porttitor lorem id ligula suspendisse ornare consequat lectus in', '76.46', 563, 2, 7, 'images\\flowers\\PinkLily.jpe', 0, '4.18', 7, 0, '2019-12-02 15:06:41', 8, NULL, 5),
(93, 'Barbula Moss', 'scelerisque quam turpis adipiscing lorem vitae mattis nibh ligula nec', '87.38', 990, 4, 6, 'images\\flowers\\pinkYellowDaylilies.jpe', 1, '4.16', 8, 1, '2019-12-02 15:06:41', 8, NULL, 1),
(94, 'Toad Rush', 'dui maecenas tristique est et tempus semper est quam pharetra', '57.12', 399, 2, 7, 'images\\flowers\\pitcher.jpe', 0, '1.95', 8, 1, '2019-12-02 15:06:41', 10, NULL, 3),
(95, 'Sierra Ancha Fleabane', 'porttitor pede justo eu massa donec dapibus duis at velit eu est', '97.99', 502, 4, 8, 'images\\flowers\\polemoniacaephlox.jpe', 0, '3.83', 1, 1, '2019-12-02 15:06:41', 10, NULL, 9),
(96, 'Pseudopanax', 'eget orci vehicula condimentum curabitur in libero ut massa volutpat convallis morbi odio odio elementum eu interdum eu tincidunt in', '55.89', 627, 4, 5, 'images\\flowers\\priklypopy.jpe', 1, '1.78', 6, 0, '2019-12-02 15:06:41', 6, NULL, 7),
(97, 'Flannelbush', 'eros viverra eget congue eget semper rutrum nulla nunc purus phasellus', '97.84', 226, 1, 6, 'images\\flowers\\purpleFlowers.jpe', 0, '1.53', 9, 1, '2019-12-02 15:06:41', 4, NULL, 4),
(98, 'New Mexico Orange Lichen', 'ultrices posuere cubilia curae nulla dapibus dolor vel est donec odio justo sollicitudin ut', '21.95', 990, 2, 8, 'images\\flowers\\rareFlowers.jpe', 1, '3.38', 8, 0, '2019-12-02 15:06:41', 8, NULL, 6),
(99, 'Texas Snakeweed', 'mauris vulputate elementum nullam varius nulla facilisi cras non velit nec nisi', '63.35', 379, 3, 10, 'images\\flowers\\redGingerFlowers.jpe', 0, '4.33', 7, 0, '2019-12-02 15:06:41', 8, NULL, 10),
(100, 'Grimmia Dry Rock Moss', 'nam nulla integer pede justo lacinia eget tincidunt eget tempus vel pede morbi porttitor lorem', '2.29', 468, 1, 5, 'images\\flowers\\rubyThroatedHummingbird.jpe', 1, '1.85', 2, 0, '2019-12-02 15:06:41', 7, NULL, 10),
(101, 'Italian Catchfly', 'nunc donec quis orci eget orci vehicula condimentum curabitur in libero ut', '86.42', 537, 1, 9, 'images\\flowers\\shutterStock.jpe', 0, '4.81', 8, 1, '2019-12-02 15:06:41', 3, NULL, 8),
(102, 'Vahl\'s Alkaligrass', 'congue etiam justo etiam pretium iaculis justo in hac habitasse platea dictumst', '80.46', 314, 5, 6, 'images\\flowers\\springflowers.jpe', 0, '1.28', 7, 0, '2019-12-02 15:06:41', 4, NULL, 10),
(103, 'Mexican Primrose-willow', 'suspendisse potenti in eleifend quam a odio in hac habitasse', '89.57', 202, 1, 5, 'images\\flowers\\Tropical.jpe', 1, '3.10', 2, 0, '2019-12-02 15:06:41', 2, NULL, 8),
(104, 'Olapalapa', 'lobortis sapien sapien non mi integer ac neque duis bibendum morbi non', '21.18', 759, 4, 5, 'images\\flowers\\tulip.jpe', 0, '2.60', 5, 1, '2019-12-02 15:06:41', 2, NULL, 5),
(105, 'Cretan Bryony', 'amet lobortis sapien sapien non mi integer ac neque duis bibendum morbi non quam nec dui luctus rutrum nulla tellus', '37.88', 941, 2, 6, 'images\\flowers\\umbels.jpg', 1, '3.71', 7, 0, '2019-12-02 15:06:41', 7, NULL, 6),
(106, 'Waxy Checkerbloom', 'aliquet ultrices erat tortor sollicitudin mi sit amet lobortis sapien sapien non mi integer ac neque', '84.40', 828, 3, 5, 'images\\flowers\\verbascumThapsus.jpg', 1, '3.05', 7, 1, '2019-12-02 15:06:41', 7, NULL, 8),
(107, 'Scentless Mock Orange', 'leo rhoncus sed vestibulum sit amet cursus id turpis integer aliquet massa id lobortis convallis tortor', '36.24', 10, 5, 10, 'images\\flowers\\verbascumThapsus1.jpg', 1, '4.30', 1, 0, '2019-12-02 15:06:42', 2, NULL, 10),
(108, 'Scarlet Spiderling', 'quis justo maecenas rhoncus aliquam lacus morbi quis tortor id nulla', '98.68', 573, 2, 5, 'images\\flowers\\wildFlowerBlanket.jpg', 0, '1.90', 8, 0, '2019-12-02 15:06:42', 2, NULL, 4),
(109, 'Milletgrass', 'in eleifend quam a odio in hac habitasse platea dictumst maecenas ut massa quis augue luctus tincidunt', '99.66', 113, 1, 8, 'images\\flowers\\wildflowers.jpg', 0, '4.55', 7, 1, '2019-12-02 15:06:42', 5, NULL, 7),
(110, 'Antilles Calophyllum', 'nisi volutpat eleifend donec ut dolor morbi vel lectus in quam fringilla rhoncus mauris enim', '57.68', 181, 4, 6, 'images\\flowers\\wildIris.jpe', 0, '1.17', 6, 0, '2019-12-02 15:06:42', 4, NULL, 2),
(111, 'Gall Of The Earth', 'nulla facilisi cras non velit nec nisi vulputate nonummy maecenas tincidunt lacus', '18.68', 188, 5, 10, 'images\\flowers\\yellowTriliums.jpg', 1, '3.29', 10, 0, '2019-12-02 15:06:42', 3, NULL, 1),
(112, 'Littlehip Hawthorn', 'sapien in sapien iaculis congue vivamus metus arcu adipiscing molestie hendrerit at vulputate vitae nisl', '35.42', 903, 5, 8, 'images\\flowers\\zenobiaPulverulenta.jpe', 1, '3.45', 9, 1, '2019-12-02 15:06:42', 2, NULL, 10);
SET FOREIGN_KEY_CHECKS = 1;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `fk_product_discount` FOREIGN KEY (`discount_id`) REFERENCES `discounts` (`discount_id`) ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
