-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Dec 02, 2019 at 08:33 PM
-- Server version: 5.7.26
-- PHP Version: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `blossom_fantasy`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `description` varchar(255) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`category_id`, `name`, `description`, `is_deleted`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 'Birthday', 'tortor duis mattis egestas metus aenean fermentum donec ut mauris eget massa tempor convallis', 0, '2019-12-02 14:30:47', 6, NULL, NULL),
(2, 'Anniversary', 'tempus semper est quam pharetra magna ac consequat metus sapien ut', 0, '2019-12-02 14:30:47', 4, NULL, NULL),
(3, 'Wedding', 'molestie nibh in lectus pellentesque at nulla suspendisse potenti cras in purus eu magna', 0, '2019-12-02 14:30:47', 2, NULL, NULL),
(4, 'Debut', 'sed interdum venenatis turpis enim blandit mi in porttitor pede justo eu massa donec dapibus duis at velit eu', 0, '2019-12-02 14:30:47', 2, NULL, NULL),
(5, 'Prom', 'felis eu sapien cursus vestibulum proin eu mi nulla ac enim in tempor turpis nec euismod', 0, '2019-12-02 14:30:47', 5, NULL, NULL),
(6, 'Grand Opening', 'interdum in ante vestibulum ante ipsum primis in faucibus orci', 0, '2019-12-02 14:30:47', 4, NULL, NULL),
(7, 'Funeral', 'eros vestibulum ac est lacinia nisi venenatis tristique fusce congue diam id ornare imperdiet sapien urna pretium nisl', 0, '2019-12-02 14:30:47', 6, NULL, NULL),
(8, 'Festival', 'pellentesque eget nunc donec quis orci eget orci vehicula condimentum curabitur in libero ut', 0, '2019-12-02 14:30:47', 4, NULL, NULL),
(9, 'Christmas', 'consequat varius integer ac leo pellentesque ultrices mattis odio donec vitae nisi nam ultrices libero', 0, '2019-12-02 14:30:47', 8, NULL, NULL),
(10, 'Thanksgiving', 'pharetra magna vestibulum aliquet ultrices erat tortor sollicitudin mi sit amet lobortis sapien sapien non mi integer ac neque duis', 0, '2019-12-02 14:30:52', 4, NULL, NULL);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
